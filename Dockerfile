# Builder Image
# ---------------------------------------------------
FROM maven:3.6.3-jdk-11 as java-builder

# Set Arguments On Build
ARG ARGS_MVN_PACKAGE=""

# Set Working Directory
WORKDIR /usr/src/app

# Copy Java Source Code File
COPY . ./

# Build Java Source Code File
RUN mvn clean install -DskipTests \
    && mvn clean package -DskipTests

# Final Image
# ---------------------------------------------------
FROM adoptopenjdk/openjdk11:alpine-jre

# Set Arguments On Build
#ARG ARGS_APP_PORT="8080"

# Set Working Directory
WORKDIR /opt/app

# Set Environtment Variables
ENV DATASOURCE_REDIS_HOST="redis" \
	DATASOURCE_REDIS_PORT="6379" \
	DATASOURCE_REDIS_PASSWORD="3wsbUoClENmjvMjl"

# Copy Anything The Application Needs
COPY --from=java-builder /usr/src/app/target/*.jar ./dtpcom-be.jar
COPY --from=java-builder /usr/src/app/target/classes /opt/app/target/

#EXPOSE ${ARGS_APP_PORT}

# Running Java application
CMD java \
    ${JAVA_ARGS} \
    -jar dtpcom-be.jar