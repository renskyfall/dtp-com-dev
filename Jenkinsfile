@Library('shared-library')_
def deployImage = new DeployImage()
env.nodeName = ""

pipeline {
    parameters {
        string(name: 'PRODUCTION_NAMESPACE',       description: 'Production Namespace',                 defaultValue: 'dtpcom-prod')
        string(name: 'STAGING_NAMESPACE',          description: 'Staging Namespace',                    defaultValue: 'dtpcom-stage')
        string(name: 'DEVELOPMENT_NAMESPACE',      description: 'Development Namespace',                defaultValue: 'dtpcom-dev')

        string(name: 'DOCKER_IMAGE_NAME',          description: 'Docker Image Name',                    defaultValue: 'dtpcom-be')

        string(name: 'CHAT_ID',                    description: 'chat id of telegram group',            defaultValue: '-1001215679728')
    }
    agent none
    options {
        skipDefaultCheckout()  // Skip default checkout behavior
    }
    stages {
        stage ( "Kill Old Build" ){
            steps{
                script{
                    KillOldBuild()
        }   }   }
        stage('Checkout SCM') {
            agent { label "Java" }
            steps {
                checkout scm
                script {
                    echo "get COMMIT_ID"
                    sh 'echo -n $(git rev-parse --short HEAD) > ./commit-id'
                    commitId = readFile('./commit-id')
                }
                stash(name: 'ws', includes:'**,./commit-id') // stash this current workspace
        }   }
        stage('Initialize') {
            parallel {
                stage("Agent: Java") {
                    agent { label "Java" }
                    steps {
                        cleanWs()
                           }   }
                stage("Agent: Docker") {
                    agent { label "Docker" }
                    steps {
                        cleanWs()
                        script{
                            if ( env.BRANCH_NAME == 'master' ){
                                envStage = "production"
                            } else if ( env.BRANCH_NAME == 'release' ){
                                envStage = "staging"
                            } else if ( env.BRANCH_NAME == 'develop'){
                                envStage = "development"
        }   }   }   }   }   }
        stage('Test & Build') {
            parallel {
                stage('Unit Test') {
                    agent { label "Java" }
                    steps {
                        unstash 'ws'
                        script {
                            echo "Do Unit Test Here"
            			    // withMaven(maven: 'Maven 3.6.3') {
            				//    sh "mvn dependency:resolve"
            			    // }

                            //echo "defining sonar-scanner"
                            //def scannerHome = tool 'SonarScanner' ;
                            //withSonarQubeEnv('SonarQube') {
                                //sh "${scannerHome}/bin/sonar-scanner"
                            //}
                }   }   }
                stage('Build') {
                    agent { label "Docker" }
                    steps {
                        unstash 'ws'
                        script{
                            env.nodeName = "${env.NODE_NAME}"
                            sh "docker build --rm --no-cache --pull -t ${params.DOCKER_IMAGE_NAME}:${BUILD_NUMBER}-${commitId} ."
                            sh "docker run -d -u 0 --name=${params.DOCKER_IMAGE_NAME}  ${params.DOCKER_IMAGE_NAME}:${BUILD_NUMBER}-${commitId} tail -f /dev/null"
                            sh "mkdir -p ./target/classes"
                            sh "docker cp ${params.DOCKER_IMAGE_NAME}:/opt/app/target ./target/classes"
                            sh "docker stop ${params.DOCKER_IMAGE_NAME}"
                            sh "docker rm -f ${params.DOCKER_IMAGE_NAME}"
                            echo "defining sonar-scanner"
                            def scannerHome = tool 'SonarScanner' ;
                            withSonarQubeEnv('SonarQube') {
                                sh "${scannerHome}/bin/sonar-scanner"
                            }
        }   }   }   }   }
        stage ('Deployment'){
            steps{
                node (nodeName as String) { 
                    echo "Running on ${nodeName}"
                    script{
                        if (env.BRANCH_NAME == 'master'){
                            echo "Deploying to ${envStage} "
                            deployImage.to_vsan("${commitId}")
                        } else if (env.BRANCH_NAME == 'release'){
                            echo "Deploying to ${envStage} "
                            deployImage.to_stage("${commitId}")
                        } else if (env.BRANCH_NAME == 'develop'){
                            echo "Deploying to ${envStage} "
                            deployImage.to_vsan("${commitId}")
    }   }   }   }   }   }
    post {
        always{
            node("Docker"){
                TelegramNotif(currentBuild.currentResult)
	}   }
	failure{
            node(nodeName as String){
                script{
                    sh "docker rm -f ${params.DOCKER_IMAGE_NAME}"
                    sh "docker rmi -f ${params.DOCKER_IMAGE_NAME}:${BUILD_NUMBER}-${commitId}"
}   }   }   }   }