# Website DTP DP

Digital point for digital product

[![License](http://img.shields.io/:license-apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

Minimal [Spring Boot](http://projects.spring.io/spring-boot/) sample app.

## Getting Started

## Getting Started

```
git clone http://gitlab.playcourt.id/dtp-com/dtp-com-be.git
```

### Prerequisites

* Java JDK 8 or Higher https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html
* Maven https://maven.apache.org/download.cgi
* Java Spring Boot
* Lombok

### Installing

```
mvn package
```

## Running the tests

```
mvn test
```

## Running the service

after you clone git and install maven, you can run service with command below :

```
mvn spring-boot:run
```

### Break down into end to end tests


### And coding style tests


## Deployment


## Built With

* [Java JDK](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html)
* [Maven 3](https://maven.apache.org/download.cgi)
* [Java Springboot](https://spring.io/projects/spring-boot) - The web framework used
* [Lombok](https://projectlombok.org/) - Java Library

## Contributing


## Versioning


## Authors

* **M. Reno Dimas P.** - *Initial work*

See also the list of [contributors](https://gitlab.playcourt.id/dtp-com/dtp-com-be/-/project_members) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments


