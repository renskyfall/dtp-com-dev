package com.telkom.dtpbe;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telkom.dtpbe.core.shared.EmptyJson;
import com.telkom.dtpbe.core.shared.Response;

@Component
public class BasicAuthEntryPoint extends BasicAuthenticationEntryPoint {

    private final static Logger LOGGER = LoggerFactory.getLogger(BasicAuthEntryPoint.class);

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        super.commence(request, response, authException);

        LOGGER.info("commence");
        response.addHeader("WWW-Authenticate", "Basic realm=" +getRealmName());
        response.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        OutputStream out = response.getOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(out, new Response(HttpStatus.UNAUTHORIZED.value(),
                false,
                new EmptyJson(),
                "invalid basic auth"));
        out.flush();
    }

    @Override
    public void afterPropertiesSet() {
        setRealmName("umeetme.id");
        super.afterPropertiesSet();
    }
}
