package com.telkom.dtpbe;

public final class Constant {

    private Constant() {

    }

    public static final String ROOT_PATH = "/api";

    public static final Integer PASSWORD_ITERATIONS = 1000;

    public static final Integer PASSWORD_KEY_LEN = 32;

    public static final Integer PASSWORD_SALT_LEN = 16;

    public static final long ONE_MINUTE_IN_MILLIS = 60000;

    public static final long ONE_HOUR_IN_MILLIS = ONE_MINUTE_IN_MILLIS * 60;

    public static final long ONE_DAY_IN_MILLIS = ONE_HOUR_IN_MILLIS * 24;

    public enum PRODUCT_TYPE { MONTHLY, WEEKLY, DAILY}

    public enum masterOrderStatus {PAID, WAITING_PAYMENT, CANCEL, CART}

    public static final String CRONTAB_ONE_HOURS = "0 0 0/1 ? * * *";

    public static final String CRONTAB_HALF_HOURS = "0 0/30 * ? * * *";

    public static final Integer ONE_HOURS = 1;

    public static final Integer GMT = 7;

    public enum  CLIENT_CODE { UMEETME, VIP, EDU}

}
