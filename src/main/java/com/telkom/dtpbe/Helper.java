package com.telkom.dtpbe;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class Helper {
    public static ZonedDateTime convertToAsiaJakarta(Date date) {
        if (date == null) {
            return null;
        }
        ZoneId asiaJakartaZoneId = ZoneId.of("Asia/Jakarta");
        return date.toInstant().atZone(asiaJakartaZoneId);
    }
}
