package com.telkom.dtpbe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.core.shared.EmptyJson;
import com.telkom.dtpbe.core.shared.Response;


@RestController
@RequestMapping(value = Constant.ROOT_PATH)
public class IndexController {

    @Autowired
    private MessageSetting messageSetting;

    private final static Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

    @GetMapping("")
    public Response index(@RequestParam(name = "lang", required = false) String lang) {
        LOGGER.info("index handler");

        String message = messageSetting.getMessage("response.api.running", lang);

        return new Response(HttpStatus.OK.value(),
                true,
                new EmptyJson(),
                message);
    }

}
