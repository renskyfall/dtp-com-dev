package com.telkom.dtpbe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class MessageSetting {

    private final static Logger LOGGER = LoggerFactory.getLogger(MessageSetting.class);

    @Autowired
    private MessageSource messageSourceResponse;

    public String getMessage(String code, String locale) {
        if (locale != null) {
            if (locale.equals("en") || locale.equals("in_ID")) {
                return messageSourceResponse.getMessage(code, null, new Locale(locale));
            }
            return messageSourceResponse.getMessage(code, null, new Locale("en"));
        }

        return messageSourceResponse.getMessage(code, null, new Locale("en"));
    }
}
