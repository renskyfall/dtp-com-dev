package com.telkom.dtpbe.config;

import java.io.InputStream;
import java.security.Key;
import java.util.Properties;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.telkom.dtpbe.core.email.EmailService;
import com.telkom.dtpbe.core.email.IEmailService;
import com.telkom.dtpbe.core.jwt.IJwtService;
import com.telkom.dtpbe.core.jwt.JwtService;
import com.telkom.dtpbe.core.jwt.KeyReader;
import com.telkom.dtpbe.core.mps.IMPSService;
import com.telkom.dtpbe.core.mps.MPSService;
import com.telkom.dtpbe.core.shared.Result;
import com.telkom.dtpbe.core.sms.ISMSService;
import com.telkom.dtpbe.core.sms.SMSService;
import com.telkom.dtpbe.core.umeetme.IOrderUmeetme;
import com.telkom.dtpbe.core.umeetme.OrderUmeetmePayload;
import com.telkom.dtpbe.core.umeetme.OrderUmeetmeResponse;
import com.telkom.dtpbe.core.umeetme.OrderUmeetmeService;
import com.telkom.dtpbe.core.umeetme.PayloadPaymentNotification;
import com.telkom.dtpbe.core.umeetme.ResponsePaymentNotification;
import com.telkom.dtpbe.core.umeetme.UserVerificationPayload;
import com.telkom.dtpbe.core.umeetme.UserVerificationResponse;

@Configuration
@PropertySource("classpath:application.properties")
@ComponentScan(basePackages = {"com.telkom.dtpbe.*"})
@Profile("default")
public class AppConfig {

    private final static Logger LOGGER = LoggerFactory.getLogger(AppConfig.class);
    
    @Value("${spring.mail.properties.mail.smtp.starttls.enable}")
	private String tls;

	@Value("${spring.mail.username}")
	private String username;

	@Value("${spring.mail.password}")
	private String password;

	@Value("${spring.mail.host}")
	private String host;

	@Value("${spring.mail.port}")
	private String port;
	
	@Value("$spring.sms.otp.host}")
	private String smsHost;

	@Value("${spring.sms.otp.basic.auth}")
	private String smsAuth;
	
	@Value("${spring.mps.host}")
	private String mpsHost;
	
	@Value("${spring.mps.merchant.id}")
	private String mpsMerchantId;
	
	@Value("${spring.mps.access.key}")
	private String mpsMerchantKey;
	
    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        mailSender.setHost(host);
        mailSender.setPort(Integer.parseInt(port));
        mailSender.setUsername(username);
        mailSender.setPassword(password);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.starttls.enable", tls);
        props.put("mail.debug", "true");

        return mailSender;
    }

    @Bean
    public IEmailService emailService() {
        return new EmailService(getJavaMailSender());
    }

    @Bean
    public ISMSService smsService() {
        return new SMSService(smsHost, smsAuth);
    }
    
    @Bean
    public IMPSService mpsService() {
        return new MPSService(
        		mpsHost,
        		mpsMerchantId,
        		mpsMerchantKey
        );
    }
    @Bean 
    public IOrderUmeetme orderUmmetme() {
    	return new OrderUmeetmeService(); 

    }
    
    @Bean
    public IJwtService jwtService() throws Exception {
        InputStream privateKeyIs, publicKeyIs;

        privateKeyIs = new ClassPathResource("rsapriv.der").getInputStream();
        publicKeyIs = new ClassPathResource("rsapub.der").getInputStream();

        Key privateKey = KeyReader.getPrivateKey(privateKeyIs);
        Key publicKey = KeyReader.getPublicKey(publicKeyIs);

        IJwtService jwtService = new JwtService(privateKey, publicKey);
        return jwtService;
    }
}
