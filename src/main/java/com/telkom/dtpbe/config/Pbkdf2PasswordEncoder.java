package com.telkom.dtpbe.config;

import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.telkom.dtpbe.Constant;
import com.telkom.dtpbe.password.Pbkdf2;

public class Pbkdf2PasswordEncoder implements PasswordEncoder {

    private final static Logger LOGGER = LoggerFactory.getLogger(Pbkdf2PasswordEncoder.class);

    @Override
    public String encode(CharSequence charSequence) {
        String h = null;
        try {
            h = Pbkdf2.hashPassword(charSequence.toString(), Constant.PASSWORD_SALT_LEN, Constant.PASSWORD_ITERATIONS,
                    Constant.PASSWORD_KEY_LEN);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("error hash password {}", e.getMessage());
        }

        return h;
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        return Pbkdf2.verifyPassword(charSequence.toString(), s);
    }
}
