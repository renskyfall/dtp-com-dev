package com.telkom.dtpbe.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;

@Configuration
public class RedisConfiguration {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(AppConfig.class);
	 
	@Value("${spring.datasource.redis.host}")
	private String host;

	@Value("${spring.datasource.redis.port}")
	private String port;

	@Value("${spring.datasource.redis.password}")
	private String password;

    @Bean
    JedisConnectionFactory jedisConnectionFactory() throws Exception {
        JedisConnectionFactory jedisConFactory;
        try {
            RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
            
            redisStandaloneConfiguration.setHostName(host);
            redisStandaloneConfiguration.setPort(Integer.parseInt(port));
            redisStandaloneConfiguration.setPassword(password);
            
            jedisConFactory = new JedisConnectionFactory(redisStandaloneConfiguration);
            jedisConFactory.getPoolConfig().setMaxTotal(50);
            jedisConFactory.getPoolConfig().setMaxIdle(50);
        } catch (RedisConnectionFailureException ex) {
            throw new Exception(ex.getMessage());
        }
        return jedisConFactory;
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate() throws Exception {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(jedisConnectionFactory());
        template.afterPropertiesSet();
        template.setValueSerializer(new GenericToStringSerializer<Object>(Object.class));
        template.setEnableTransactionSupport(true);
        return template;
    }

    @Bean
    public StringRedisTemplate redisTemplateString() throws Exception {
        return new StringRedisTemplate(jedisConnectionFactory());
    }
}
