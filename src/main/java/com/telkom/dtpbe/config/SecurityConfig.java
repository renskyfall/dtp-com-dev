package com.telkom.dtpbe.config;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;

import com.telkom.dtpbe.AuthEntryPoint;
import com.telkom.dtpbe.Constant;
import com.telkom.dtpbe.filter.JwtFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final static Logger LOGGER = LoggerFactory.getLogger(SecurityConfig.class);

	@Value("${spring.cors.config.origin}")
	protected String corsConfigOrigin;

    private final static String INDEX = Constant.ROOT_PATH;
    private final static String USER_ENDPOINT = Constant.ROOT_PATH + "/users/**";
    private final static String FORGOT_PASSWORD_ENDPOINT = Constant.ROOT_PATH + "/users/forgot-password";
    private final static String RESET_PASSWORD_ENDPOINT = Constant.ROOT_PATH + "/users/reset-password";
    private final static String VERIFICATION_CODE_ENDPOINT = Constant.ROOT_PATH + "/users/verification-code/**";
    private final static String GENERATE_QR_CODE_ENDPOINT = Constant.ROOT_PATH + "/users/generate-qrcode";
    private final static String AUTH_ENDPOINT = Constant.ROOT_PATH + "/auth/**";
    private final static String MONITORING_ENDPOINT = Constant.ROOT_PATH + "/monitor/**";
    private final static String WEBSOCKET_ENDPOINT = Constant.ROOT_PATH + "/stomp/**";
    private final static String PRODUCT_ENDPOINT = Constant.ROOT_PATH + "/product/**";
    private final static String PRODUCT_TYPE_ENDPOINT = Constant.ROOT_PATH + "/productType/**";
    private final static String PRODUCT_PACKAGE_ENDPOINT = Constant.ROOT_PATH + "/productPackage/**";
    private final static String GUIDE_ENDPOINT = Constant.ROOT_PATH + "/guide/**";
    private final static String HOW_TO_USE_ENDPOINT = Constant.ROOT_PATH + "/howToUse/**";
    private final static String PROBLEM_REPORT_ENDPOINT = Constant.ROOT_PATH + "/problemReport/**";
    private final static String LOCATION_ENDPOINT = Constant.ROOT_PATH + "/location/**";
    private final static String INDUSTRIES_ENDPOINT = Constant.ROOT_PATH + "/industries/**";
    private final static String ABOUT_ENDPOINT = Constant.ROOT_PATH + "/about/**";
    private final static String SHOW_ABOUT_ENDPOINT = Constant.ROOT_PATH + "/showAbout/**";
    private final static String CONTENT_DASHBOARD_MENU_ENDPOINT = Constant.ROOT_PATH + "/contentDashboardMenu/**";
    private final static String CONTENT_PRODUCT_ENDPOINT = Constant.ROOT_PATH + "/contentProduct/**";
    private final static String FOOTER_ENDPOINT = Constant.ROOT_PATH + "/footer/**";
    private final static String CONTENT_CONTACT_US_ENDPOINT = Constant.ROOT_PATH + "/contentContactUs/**";
    private final static String CONTENT_CUSTOMERs_STORIES_ENDPOINT = Constant.ROOT_PATH + "/contentCustomerStories/**";
    private final static String CONTENT_ADVANTAGE_DTP_ENDPOINT = Constant.ROOT_PATH + "/contentAdvantageDtp/**";
    private final static String LANDING_PAGE_ENDPOINT = Constant.ROOT_PATH + "/landingPage/**";
    private final static String HEADER_ENDPOINT = Constant.ROOT_PATH + "/header/**";
    private final static String FAQ_ENDPOINT = Constant.ROOT_PATH + "/faq/**";
    private final static String DOWNLOAD_FILE = Constant.ROOT_PATH + "/downloadFile/**";
    private final static String TERM_AND_CONDITION_ENDPOINT = Constant.ROOT_PATH + "/termsAndConditions/**";
    private final static String SUB_TERN_AND_CONDITION_ENDPOINT = Constant.ROOT_PATH + "/subTermsAndConditions/**";
    private final static String PRIVACY_POLICY_ENDPOINT = Constant.ROOT_PATH + "/privacyPolicy/**";
    private final static String SUB_PRIVACY_POLICY_ENDPOINT = Constant.ROOT_PATH + "/subPrivacyPolicy/**";
    private final static String DASHBOARD_HOME_DESCRIPTION_ENDPOINT = Constant.ROOT_PATH + "/dashboard/home/getDescription";
    private final static String PLAN_AND_PRICING_ENDPOINT = Constant.ROOT_PATH + "/planAndPricing/**";
    private final static String PAYMENT_NOTIFICATION_ENDPOINT = Constant.ROOT_PATH + "/payment/notification";
    
    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AuthEntryPoint authEntryPoint;

    @Autowired
    private JwtFilter jwtFilter;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new Pbkdf2PasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        LOGGER.info("configure http security");
        // disable default login by spring boot
        http.httpBasic().disable()

                // all should be authenticated, except /auth/login
                .authorizeRequests()

                .antMatchers(INDEX).permitAll()
                .antMatchers(USER_ENDPOINT).permitAll()
                .antMatchers(FORGOT_PASSWORD_ENDPOINT).permitAll()
                .antMatchers(RESET_PASSWORD_ENDPOINT).permitAll()
                .antMatchers(VERIFICATION_CODE_ENDPOINT).permitAll()
                .antMatchers(AUTH_ENDPOINT).permitAll()
                .antMatchers(GENERATE_QR_CODE_ENDPOINT).permitAll()
                .antMatchers(PRODUCT_ENDPOINT).permitAll()
                .antMatchers(PRODUCT_TYPE_ENDPOINT).permitAll()
                .antMatchers(PRODUCT_PACKAGE_ENDPOINT).permitAll()
                .antMatchers(GUIDE_ENDPOINT).permitAll()
                .antMatchers(HOW_TO_USE_ENDPOINT).permitAll()
                .antMatchers(PROBLEM_REPORT_ENDPOINT).permitAll()
                .antMatchers(LOCATION_ENDPOINT).permitAll()
                .antMatchers(INDUSTRIES_ENDPOINT).permitAll()
                .antMatchers(ABOUT_ENDPOINT).permitAll()
                .antMatchers(SHOW_ABOUT_ENDPOINT).permitAll()
                .antMatchers(CONTENT_DASHBOARD_MENU_ENDPOINT).permitAll()
                .antMatchers(CONTENT_PRODUCT_ENDPOINT).permitAll()
                
                .antMatchers(FOOTER_ENDPOINT).permitAll()
                .antMatchers(CONTENT_CONTACT_US_ENDPOINT).permitAll()
                .antMatchers(CONTENT_CUSTOMERs_STORIES_ENDPOINT).permitAll()
                .antMatchers(CONTENT_ADVANTAGE_DTP_ENDPOINT).permitAll()
                .antMatchers(LANDING_PAGE_ENDPOINT).permitAll()
                .antMatchers(HEADER_ENDPOINT).permitAll()
                
                .antMatchers(FAQ_ENDPOINT).permitAll()
                .antMatchers(DOWNLOAD_FILE).permitAll()
                .antMatchers(TERM_AND_CONDITION_ENDPOINT).permitAll()
                .antMatchers(SUB_TERN_AND_CONDITION_ENDPOINT).permitAll()
                .antMatchers(PRIVACY_POLICY_ENDPOINT).permitAll()
                .antMatchers(SUB_PRIVACY_POLICY_ENDPOINT).permitAll()
                .antMatchers(DASHBOARD_HOME_DESCRIPTION_ENDPOINT).permitAll()
                
                .antMatchers(PLAN_AND_PRICING_ENDPOINT).permitAll()
                .antMatchers(PAYMENT_NOTIFICATION_ENDPOINT).permitAll()
                // disable authentication for health and monitoring check
                // https://docs.spring.io/spring-boot/docs/2.0.x/actuator-api/html/
                .antMatchers(MONITORING_ENDPOINT).permitAll()

                .antMatchers(WEBSOCKET_ENDPOINT).permitAll()

                .anyRequest().authenticated().and()

                .exceptionHandling().authenticationEntryPoint(authEntryPoint).and()

                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // disable cors and csrf
        http.cors().and().csrf().disable();
        http.cors().configurationSource(request -> {
            String[] originsArr = corsConfigOrigin.split(",");
            List<String> allowOrigins = Arrays.stream(originsArr).collect(Collectors.toList());
            CorsConfiguration corsConfiguration = new CorsConfiguration();
            corsConfiguration.setAllowCredentials(true);
            corsConfiguration.addAllowedHeader("*");
            corsConfiguration.addAllowedMethod("*");
            corsConfiguration.setAllowedOrigins(allowOrigins);
            //corsConfiguration.applyPermitDefaultValues();
            return corsConfiguration;
        }).and().csrf().disable();

        http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);

    }


}
