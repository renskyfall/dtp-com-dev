package com.telkom.dtpbe.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.ConfigurableWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class ServerPortConfig implements WebServerFactoryCustomizer<ConfigurableWebServerFactory> {

    private final static Logger LOGGER = LoggerFactory.getLogger(ServerPortConfig.class);

    @Autowired
    Environment env;

    @Override
    public void customize(ConfigurableWebServerFactory factory) {
        LOGGER.info("customize port number");
        factory.setPort(Integer.parseInt("8080"));
    }
}
