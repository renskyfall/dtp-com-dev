package com.telkom.dtpbe.config;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

import com.telkom.dtpbe.Constant;
import com.telkom.dtpbe.core.jwt.CustomClaim;
import com.telkom.dtpbe.core.jwt.IJwtService;
import com.telkom.dtpbe.core.shared.Result;

@Configuration
@EnableWebSocketMessageBroker
@Order(Ordered.HIGHEST_PRECEDENCE + 99)
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    private final static Logger LOGGER = LoggerFactory.getLogger(WebSocketConfig.class);

    @Autowired
    private IJwtService jwtService;

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/topic", "/queue")
                .setTaskScheduler(heartBeatScheduler());

        registry.setApplicationDestinationPrefixes("/stomp");

        // using AMQP
        //        registry.setApplicationDestinationPrefixes("/app")
        //                .enableStompBrokerRelay("/topic")
        //                .setRelayHost("localhost")
        //                .setRelayPort(61613)
        //                .setClientLogin("guest")
        //                .setClientPasscode("guest");
    }

    @Bean
    public TaskScheduler heartBeatScheduler() {
        return new ThreadPoolTaskScheduler();
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint(Constant.ROOT_PATH + "/stomp")
                .setAllowedOrigins("*").addInterceptors(new HttpSessionHandshakeInterceptor());
        // with sockjs
        registry.addEndpoint(Constant.ROOT_PATH + "/stomp")
                .setAllowedOrigins("*")
                .withSockJS();
    }

    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
        //registration.interceptors(new JWTInterceptor());
        registration.interceptors(new ClientIDInterceptor());
    }

    private class JWTInterceptor implements ChannelInterceptor {

        @Override
        public Message<?> preSend(Message<?> message, MessageChannel channel) throws AuthenticationCredentialsNotFoundException {
            StompHeaderAccessor accessor =
                    MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
            if (StompCommand.CONNECT.equals(accessor.getCommand())) {
                List<String> authorization = accessor.getNativeHeader("X-Authorization");
                if (authorization.size() > 0) {
                    if (authorization.get(0) != null) {
                        String tokenFromHeader = authorization.get(0);
                        if (tokenFromHeader.startsWith("Bearer ")) {
                            LOGGER.info("X-Authorization {}", authorization.get(0));

                            Future<Result<CustomClaim, String>> resultFuture = jwtService.validate(tokenFromHeader);
                            try {
                                if (resultFuture.get().getError() != null) {
                                    throw new AuthenticationCredentialsNotFoundException(resultFuture.get().getError());
                                }

                                CustomClaim claim = resultFuture.get().getData();
                                accessor.setUser(new UsernamePasswordAuthenticationToken(claim.getSubject(), "", new ArrayList<>()));

                            } catch (InterruptedException e) {
                                LOGGER.error("interrupted {}", e.getLocalizedMessage());
                                throw new AuthenticationCredentialsNotFoundException(e.getMessage());
                            } catch (ExecutionException e) {
                                LOGGER.error("execution exception {}", e.getLocalizedMessage());
                                throw new AuthenticationCredentialsNotFoundException(e.getMessage());
                            }

                        }

                    }
                }
            }
            return message;
        }
    }

    private class ClientIDInterceptor implements ChannelInterceptor {
        @Override
        public Message<?> preSend(Message<?> message, MessageChannel channel) {
            StompHeaderAccessor accessor =
                    MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
            if (StompCommand.CONNECT.equals(accessor.getCommand())) {
                List<String> authorization = accessor.getNativeHeader("X-Authorization");
                if (authorization.size() > 0) {
                    if (authorization.get(0) != null) {
                        String clientIDFromHeader = authorization.get(0);
                        accessor.setUser(new UsernamePasswordAuthenticationToken(clientIDFromHeader, "", new ArrayList<>()));
                    }
                }
            }
            return message;
        }
    }
}