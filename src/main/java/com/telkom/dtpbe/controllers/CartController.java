package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telkom.dtpbe.dtos.CartDTO;
import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.Cart;
import com.telkom.dtpbe.repositories.CartRepository;
import com.telkom.dtpbe.repositories.ProductPackageSubcriptionRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class CartController extends Minio{

	ModelMapper modelMapper = new ModelMapper();
	ObjectMapper objectMapper = new ObjectMapper();
	
	@Autowired
	CartRepository cartRepository;

	@Autowired
	ProductPackageSubcriptionRepository productPackageSubcriptionRepository;
	
	public CartDTO convertToDTO(Cart object) {
	        return modelMapper.map(object, CartDTO.class);
	}

	public Cart convertToEntity(CartDTO object) {
	    return modelMapper.map(object, Cart.class);
	}
	
	//View All Data
	@GetMapping("/cart/readAll")
	public HashMap<String, Object> getAllData() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<CartDTO> listData = new ArrayList<CartDTO>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (Cart tempData : cartRepository.findAll()) {
				CartDTO tempDataDTO = convertToDTO(tempData);
				listData.add(tempDataDTO);
			}
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listData.size());
		showHashMap.put("data", listData);

		return showHashMap;
	}
	
	//View Data By ID
	@GetMapping("/cart/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Cart tempData = cartRepository.findById(id).orElse(null);
			CartDTO tempDataDTO = convertToDTO(tempData);
			showHashMap.put("data", tempDataDTO);
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Create a new data
	@PostMapping("/cart/add")
	public HashMap<String, Object> createData(@Valid @RequestBody CartDTO dataDto) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Cart tempData = convertToEntity(dataDto);
			tempData.setIsDeleted(false);
			tempData.setCreatedOn(dateNow);
			tempData.setLastModifiedOn(dateNow);
			dataDto = convertToDTO(cartRepository.save(tempData));
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("data", tempData);
			
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }
	
	// UPDATE BELUM
	@PutMapping("/cart/update/{id}")
	public HashMap<String, Object> updateData(@PathVariable(value="id") Long id, @Valid @RequestBody CartDTO tempDataDto){
		
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		
		try {
			Cart tempData = cartRepository.findById(id).orElseThrow(null);
			
			tempDataDto.setCartId(tempData.getCartId());
			
			if(tempDataDto.getUserId() != null) {
				tempData.setUserId(convertToEntity(tempDataDto).getUserId());
			}
			
			if(tempDataDto.getProducts() != null) {
				tempData.setProducts(convertToEntity(tempDataDto).getProducts());
			}
			
			if(tempDataDto.getDiscount() != null) {
				tempData.setDiscount(convertToEntity(tempDataDto).getDiscount());
			}
			
			if(tempDataDto.getGrandTotal() != null) {
				tempData.setGrandTotal(convertToEntity(tempDataDto).getGrandTotal());
			}
			
			if(tempDataDto.getIsDeleted() != null) {
				tempData.setIsDeleted(convertToEntity(tempDataDto).getIsDeleted());
			}
			
			if(tempDataDto.getLastModifiedBy() != null) {
				tempData.setLastModifiedBy(convertToEntity(tempDataDto).getLastModifiedBy());
			}
		
			tempData.setLastModifiedOn(dateNow);
			
			
			Cart updateData = cartRepository.save(tempData);
			
			List<CartDTO> resultList = new ArrayList<CartDTO>();
			resultList.add(convertToDTO(updateData));
	
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
			
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Delete a Data
    @DeleteMapping("/cart/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Cart tempData = cartRepository.findById(id).orElse(null);
			cartRepository.delete(tempData);
	
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
    }
    
}
