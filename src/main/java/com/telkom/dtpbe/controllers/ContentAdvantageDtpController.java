package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ContentAdvantageDtpDTO;
import com.telkom.dtpbe.models.ContentAdvantageDtp;
import com.telkom.dtpbe.repositories.ContentAdvantageDtpRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ContentAdvantageDtpController {
	
	private String timeFormat = "yyyy-MM-dd HH:mm:ss";
	private String successMessage = "success";
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ContentAdvantageDtpRepository contentAdvantageDtpRepository;
	
	public ContentAdvantageDtpDTO convertToDTO(ContentAdvantageDtp contentAdvantageDtp) {
		return modelMapper.map(contentAdvantageDtp, ContentAdvantageDtpDTO.class);
	}

	public ContentAdvantageDtp convertToEntity(ContentAdvantageDtpDTO contentAdvantageDtpDTO) {
		return modelMapper.map(contentAdvantageDtpDTO, ContentAdvantageDtp.class);
	}	

	// Get All ContentAdvantageDtp
	@GetMapping("/contentAdvantageDtp/readAll")
	public HashMap<String, Object> getAllContentAdvantageDtp() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<HashMap<String, Object>> listContentAdvantageDtps = new ArrayList<HashMap<String, Object>>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (ContentAdvantageDtp c : contentAdvantageDtpRepository.findAll()) {
				ContentAdvantageDtpDTO contentAdvantageDtpDTO = convertToDTO(c);
				
				HashMap<String, Object> mapTemp = new HashMap<String, Object>();
				mapTemp.put("advantageDtpId",contentAdvantageDtpDTO.getAdvantageDtpId());
				mapTemp.put("title",contentAdvantageDtpDTO.getTitle());
				mapTemp.put("advantage",contentAdvantageDtpDTO.getAdvantage());
				mapTemp.put("imagePath",contentAdvantageDtpDTO.getImagePath());
				mapTemp.put("description",contentAdvantageDtpDTO.getDescription());
				mapTemp.put("isDeleted",contentAdvantageDtpDTO.getIsDeleted());
				mapTemp.put("createdBy",contentAdvantageDtpDTO.getCreatedBy());
				String createdOn = new SimpleDateFormat(timeFormat).format(contentAdvantageDtpDTO.getCreatedOn());
				mapTemp.put("createdOn",createdOn);
				mapTemp.put("lastModifiedBy",contentAdvantageDtpDTO.getLastModifiedBy());
				String lastModifiedOn = new SimpleDateFormat(timeFormat).format(contentAdvantageDtpDTO.getLastModifiedOn());
				mapTemp.put("lastModifiedOn",lastModifiedOn);
				
				listContentAdvantageDtps.add(mapTemp);
			}
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listContentAdvantageDtps.size());
		showHashMap.put("data", listContentAdvantageDtps);

		return showHashMap;
	}

	// Read ContentAdvantageDtp By ID
	@GetMapping("/contentAdvantageDtp/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentAdvantageDtp contentAdvantageDtp = contentAdvantageDtpRepository.findById(id).orElse(null);
			ContentAdvantageDtpDTO contentAdvantageDtpDTO = convertToDTO(contentAdvantageDtp);
			
			HashMap<String, Object> mapTemp = new HashMap<String, Object>();
			mapTemp.put("advantageDtpId",contentAdvantageDtpDTO.getAdvantageDtpId());
			mapTemp.put("title",contentAdvantageDtpDTO.getTitle());
			mapTemp.put("advantage",contentAdvantageDtpDTO.getAdvantage());
			mapTemp.put("imagePath",contentAdvantageDtpDTO.getImagePath());
			mapTemp.put("description",contentAdvantageDtpDTO.getDescription());
			mapTemp.put("isDeleted",contentAdvantageDtpDTO.getIsDeleted());
			mapTemp.put("createdBy",contentAdvantageDtpDTO.getCreatedBy());
			String createdOn = new SimpleDateFormat(timeFormat).format(contentAdvantageDtpDTO.getCreatedOn());
			mapTemp.put("createdOn",createdOn);
			mapTemp.put("lastModifiedBy",contentAdvantageDtpDTO.getLastModifiedBy());
			String lastModifiedOn = new SimpleDateFormat(timeFormat).format(contentAdvantageDtpDTO.getLastModifiedOn());
			mapTemp.put("lastModifiedOn",lastModifiedOn);
			
			showHashMap.put("data", mapTemp);
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		
		return showHashMap;
	}

	// Create a new ContentAdvantageDtp
	@PostMapping("/contentAdvantageDtp/add")
	public HashMap<String, Object> createContentAdvantageDtp(@Valid @RequestBody ContentAdvantageDtpDTO contentAdvantageDtpDTO) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	LocalDateTime localNow = LocalDateTime.now();
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
    	Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
    	Long id = new Long(1);
    	String message;
		boolean isSuccess = false;
		Integer code = null;
		try {
			ContentAdvantageDtp contentAdvantageDtp = convertToEntity(contentAdvantageDtpDTO);
			if(contentAdvantageDtpDTO.getIsDeleted() == null) {
				contentAdvantageDtp.setIsDeleted(false);
			}
			if(contentAdvantageDtpDTO.getCreatedBy() == null) {
				contentAdvantageDtp.setCreatedBy(id);
			}
			if(contentAdvantageDtpDTO.getLastModifiedBy() == null) {
				contentAdvantageDtp.setLastModifiedBy(id);
			}
			contentAdvantageDtp.setCreatedOn(dateNow);
			contentAdvantageDtp.setLastModifiedOn(dateNow);
			contentAdvantageDtpRepository.save(contentAdvantageDtp);
			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("data", contentAdvantageDtp);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }

	// Update a ContentAdvantageDtp
	@PutMapping("/contentAdvantageDtp/update/{id}")
	public HashMap<String, Object> updateContentAdvantageDtp(@PathVariable(value = "id") Long id,
			@Valid @RequestBody ContentAdvantageDtpDTO contentAdvantageDtpDetails) {

		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		LocalDateTime localNow = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
		Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		try {
			ContentAdvantageDtp contentAdvantageDtp = contentAdvantageDtpRepository.findById(id).orElse(null);
			
			contentAdvantageDtpDetails.setAdvantageDtpId(contentAdvantageDtp.getAdvantageDtpId());
	
			if (contentAdvantageDtpDetails.getTitle() != null) {
				contentAdvantageDtp.setTitle(convertToEntity(contentAdvantageDtpDetails).getTitle());
			}
			if (contentAdvantageDtpDetails.getAdvantage() != null) {
				contentAdvantageDtp.setAdvantage(convertToEntity(contentAdvantageDtpDetails).getAdvantage());
			}
			if (contentAdvantageDtpDetails.getImagePath() != null) {
				contentAdvantageDtp.setImagePath(convertToEntity(contentAdvantageDtpDetails).getImagePath());
			}
			if (contentAdvantageDtpDetails.getDescription() != null) {
				contentAdvantageDtp.setDescription(convertToEntity(contentAdvantageDtpDetails).getDescription());
			}
			if (contentAdvantageDtpDetails.getIsDeleted() != null) {
				contentAdvantageDtp.setIsDeleted(convertToEntity(contentAdvantageDtpDetails).getIsDeleted());
			}
			if (contentAdvantageDtpDetails.getLastModifiedBy() != null) {
				contentAdvantageDtp.setLastModifiedBy(convertToEntity(contentAdvantageDtpDetails).getLastModifiedBy());
			}
			contentAdvantageDtp.setLastModifiedOn(dateNow);
			
			ContentAdvantageDtp updateContentAdvantageDtp = contentAdvantageDtpRepository.save(contentAdvantageDtp);
	
			List<ContentAdvantageDtp> resultList = new ArrayList<ContentAdvantageDtp>();
			resultList.add(updateContentAdvantageDtp);
			
			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		
		return showHashMap;
	}

	// Delete a ContentAdvantageDtp
	@DeleteMapping("/contentAdvantageDtp/delete/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentAdvantageDtp contentAdvantageDtp = contentAdvantageDtpRepository.findById(id).orElse(null);
			contentAdvantageDtpRepository.delete(contentAdvantageDtp);

			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
}
