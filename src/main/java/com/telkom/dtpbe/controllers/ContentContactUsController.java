package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ContentContactUsDTO;
import com.telkom.dtpbe.models.ContentContactUs;
import com.telkom.dtpbe.repositories.ContentContactUsRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ContentContactUsController {
	
	private String timeFormat = "yyyy-MM-dd HH:mm:ss";
	private String successMessage = "success";
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ContentContactUsRepository contentContactUsRepository;
	
	public ContentContactUsDTO convertToDTO(ContentContactUs contentContactUs) {
		return modelMapper.map(contentContactUs, ContentContactUsDTO.class);
	}

	public ContentContactUs convertToEntity(ContentContactUsDTO contentContactUsDTO) {
		return modelMapper.map(contentContactUsDTO, ContentContactUs.class);
	}

	// Get All ContentContactUs
	@GetMapping("/contentContactUs/readAll")
	public HashMap<String, Object> getAllContentContactUs() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<HashMap<String, Object>> listContentContactUss = new ArrayList<HashMap<String, Object>>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (ContentContactUs c : contentContactUsRepository.findAll()) {
				ContentContactUsDTO contentContactUsDTO = convertToDTO(c);
				HashMap<String, Object> mapTemp = new HashMap<String, Object>();
				mapTemp.put("contactUsId",contentContactUsDTO.getContactUsId());
				mapTemp.put("title",contentContactUsDTO.getTitle());
				mapTemp.put("imagePath",contentContactUsDTO.getImagePath());
				mapTemp.put("description",contentContactUsDTO.getDescription());
				mapTemp.put("isDeleted",contentContactUsDTO.getIsDeleted());
				mapTemp.put("createdBy",contentContactUsDTO.getCreatedBy());
				String createdOn = new SimpleDateFormat(timeFormat).format(contentContactUsDTO.getCreatedOn());
				mapTemp.put("createdOn",createdOn);
				mapTemp.put("lastModifiedBy",contentContactUsDTO.getLastModifiedBy());
				String lastModifiedOn = new SimpleDateFormat(timeFormat).format(contentContactUsDTO.getLastModifiedOn());
				mapTemp.put("lastModifiedOn",lastModifiedOn);
				listContentContactUss.add(mapTemp);
			}
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listContentContactUss.size());
		showHashMap.put("data", listContentContactUss);

		return showHashMap;
	}

	// Read ContentContactUs By ID
	@GetMapping("/contentContactUs/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentContactUs contentContactUs = contentContactUsRepository.findById(id).orElse(null);
			ContentContactUsDTO contentContactUsDTO = convertToDTO(contentContactUs);
			HashMap<String, Object> mapTemp = new HashMap<String, Object>();
			mapTemp.put("contactUsId",contentContactUsDTO.getContactUsId());
			mapTemp.put("title",contentContactUsDTO.getTitle());
			mapTemp.put("imagePath",contentContactUsDTO.getImagePath());
			mapTemp.put("description",contentContactUsDTO.getDescription());
			mapTemp.put("isDeleted",contentContactUsDTO.getIsDeleted());
			mapTemp.put("createdBy",contentContactUsDTO.getCreatedBy());
			String createdOn = new SimpleDateFormat(timeFormat).format(contentContactUsDTO.getCreatedOn());
			mapTemp.put("createdOn",createdOn);
			mapTemp.put("lastModifiedBy",contentContactUsDTO.getLastModifiedBy());
			String lastModifiedOn = new SimpleDateFormat(timeFormat).format(contentContactUsDTO.getLastModifiedOn());
			mapTemp.put("lastModifiedOn",lastModifiedOn);
			showHashMap.put("data", mapTemp);
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		
		return showHashMap;
	}

	// Create a new ContentContactUs
	@PostMapping("/contentContactUs/add")
	public HashMap<String, Object> createContentContactUs(@Valid @RequestBody ContentContactUsDTO contentContactUsDTO) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	LocalDateTime localNow = LocalDateTime.now();
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
    	Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
    	Long id = new Long(1);
    	String message;
		boolean isSuccess = false;
		Integer code = null;
		try {
			ContentContactUs contentContactUs = convertToEntity(contentContactUsDTO);
			if(contentContactUsDTO.getIsDeleted() == null) {
				contentContactUs.setIsDeleted(false);
			}
			if(contentContactUsDTO.getCreatedBy() == null) {
				contentContactUs.setCreatedBy(id);
			}
			if(contentContactUsDTO.getLastModifiedBy() == null) {
				contentContactUs.setLastModifiedBy(id);
			}
			contentContactUs.setCreatedOn(dateNow);
			contentContactUs.setLastModifiedOn(dateNow);
			contentContactUsRepository.save(contentContactUs);
			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("data", contentContactUs);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }

	// Update a ContentContactUs
	@PutMapping("/contentContactUs/update/{id}")
	public HashMap<String, Object> updateContentContactUs(@PathVariable(value = "id") Long id,
			@Valid @RequestBody ContentContactUsDTO contentContactUsDTO) {

		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		LocalDateTime localNow = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
		Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		try {
			ContentContactUs contentContactUs = contentContactUsRepository.findById(id).orElse(null);
			
			contentContactUsDTO.setContactUsId(contentContactUs.getContactUsId());
	
			if (contentContactUsDTO.getTitle() != null) {
				contentContactUs.setTitle(convertToEntity(contentContactUsDTO).getTitle());
			}
			if (contentContactUsDTO.getImagePath() != null) {
				contentContactUs.setImagePath(convertToEntity(contentContactUsDTO).getImagePath());
			}
			if (contentContactUsDTO.getDescription() != null) {
				contentContactUs.setDescription(convertToEntity(contentContactUsDTO).getDescription());
			}
			if (contentContactUsDTO.getIsDeleted() != null) {
				contentContactUs.setIsDeleted(convertToEntity(contentContactUsDTO).getIsDeleted());
			}
			if (contentContactUsDTO.getLastModifiedBy() != null) {
				contentContactUs.setLastModifiedBy(convertToEntity(contentContactUsDTO).getLastModifiedBy());
			}
			contentContactUs.setLastModifiedOn(dateNow);
			ContentContactUs updateContentContactUs = contentContactUsRepository.save(contentContactUs);
	
			List<ContentContactUs> resultList = new ArrayList<ContentContactUs>();
			resultList.add(updateContentContactUs);
			
			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		
		return showHashMap;
	}

	// Delete a ContentContactUs
	@DeleteMapping("/contentContactUs/delete/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentContactUs contentContactUs = contentContactUsRepository.findById(id).orElse(null);
			contentContactUsRepository.delete(contentContactUs);

			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
}
