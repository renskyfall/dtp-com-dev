package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ContentCustomerStoriesDTO;
import com.telkom.dtpbe.models.ContentCustomerStories;
import com.telkom.dtpbe.repositories.ContentCustomerStoriesRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ContentCustomerStoriesController {
	
	private String timeFormat = "yyyy-MM-dd HH:mm:ss";
	private String successMessage = "success";
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ContentCustomerStoriesRepository contentCustomerStoriesRepository;
	
	public ContentCustomerStoriesDTO convertToDTO(ContentCustomerStories contentCustomerStories) {
		return modelMapper.map(contentCustomerStories, ContentCustomerStoriesDTO.class);
	}

	public ContentCustomerStories convertToEntity(ContentCustomerStoriesDTO contentCustomerStoriesDTO) {
		return modelMapper.map(contentCustomerStoriesDTO, ContentCustomerStories.class);
	}
		
	// Get All ContentCustomerStories
	@GetMapping("/contentCustomerStories/readAll")
	public HashMap<String, Object> getAllContentCustomerStories() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<HashMap<String, Object>> listContentCustomerStoriess = new ArrayList<HashMap<String, Object>>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (ContentCustomerStories c : contentCustomerStoriesRepository.findAll()) {
				ContentCustomerStoriesDTO contentCustomerStoriesDTO = convertToDTO(c);
				HashMap<String, Object> mapTemp = new HashMap<String, Object>();
				mapTemp.put("customerStoriesId",contentCustomerStoriesDTO.getCustomerStoriesId());
				mapTemp.put("industries",contentCustomerStoriesDTO.getIndustries());
				mapTemp.put("users",contentCustomerStoriesDTO.getUsers());
				mapTemp.put("title",contentCustomerStoriesDTO.getTitle());
				mapTemp.put("stories",contentCustomerStoriesDTO.getStories());
				mapTemp.put("imagePath",contentCustomerStoriesDTO.getImagePath());
				mapTemp.put("logoPath",contentCustomerStoriesDTO.getLogoPath());
				mapTemp.put("description",contentCustomerStoriesDTO.getDescription());
				mapTemp.put("isDeleted",contentCustomerStoriesDTO.getIsDeleted());
				mapTemp.put("createdBy",contentCustomerStoriesDTO.getCreatedBy());
				String createdOn = new SimpleDateFormat(timeFormat).format(contentCustomerStoriesDTO.getCreatedOn());
				mapTemp.put("createdOn",createdOn);
				mapTemp.put("lastModifiedBy",contentCustomerStoriesDTO.getLastModifiedBy());
				String lastModifiedOn = new SimpleDateFormat(timeFormat).format(contentCustomerStoriesDTO.getLastModifiedOn());
				mapTemp.put("lastModifiedOn",lastModifiedOn);
				listContentCustomerStoriess.add(mapTemp);
			}
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listContentCustomerStoriess.size());
		showHashMap.put("data", listContentCustomerStoriess);

		return showHashMap;
	}

	// Read ContentCustomerStories By ID
	@GetMapping("/contentCustomerStories/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentCustomerStories contentCustomerStories = contentCustomerStoriesRepository.findById(id).orElse(null);
			ContentCustomerStoriesDTO contentCustomerStoriesDTO = convertToDTO(contentCustomerStories);
			HashMap<String, Object> mapTemp = new HashMap<String, Object>();
			mapTemp.put("customerStoriesId",contentCustomerStoriesDTO.getCustomerStoriesId());
			mapTemp.put("industries",contentCustomerStoriesDTO.getIndustries());
			mapTemp.put("users",contentCustomerStoriesDTO.getUsers());
			mapTemp.put("title",contentCustomerStoriesDTO.getTitle());
			mapTemp.put("stories",contentCustomerStoriesDTO.getStories());
			mapTemp.put("imagePath",contentCustomerStoriesDTO.getImagePath());
			mapTemp.put("logoPath",contentCustomerStoriesDTO.getLogoPath());
			mapTemp.put("description",contentCustomerStoriesDTO.getDescription());
			mapTemp.put("isDeleted",contentCustomerStoriesDTO.getIsDeleted());
			mapTemp.put("createdBy",contentCustomerStoriesDTO.getCreatedBy());
			String createdOn = new SimpleDateFormat(timeFormat).format(contentCustomerStoriesDTO.getCreatedOn());
			mapTemp.put("createdOn",createdOn);
			mapTemp.put("lastModifiedBy",contentCustomerStoriesDTO.getLastModifiedBy());
			String lastModifiedOn = new SimpleDateFormat(timeFormat).format(contentCustomerStoriesDTO.getLastModifiedOn());
			mapTemp.put("lastModifiedOn",lastModifiedOn);
			showHashMap.put("data", mapTemp);
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}

	// Create a new ContentCustomerStories
	@PostMapping("/contentCustomerStories/add")
	public HashMap<String, Object> createContentCustomerStories(@Valid @RequestBody ContentCustomerStoriesDTO contentCustomerStoriesDTO) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	LocalDateTime localNow = LocalDateTime.now();
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
    	Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
    	Long id = new Long(1);
		String message;
		boolean isSuccess = false;
		Integer code = null;
		try {
			ContentCustomerStories contentCustomerStories = convertToEntity(contentCustomerStoriesDTO);
			if(contentCustomerStoriesDTO.getIsDeleted() == null) {
				contentCustomerStories.setIsDeleted(false);
			}
			if(contentCustomerStoriesDTO.getCreatedBy() == null) {
				contentCustomerStories.setCreatedBy(id);
			}
			if(contentCustomerStoriesDTO.getLastModifiedBy() == null) {
				contentCustomerStories.setLastModifiedBy(id);
			}
			contentCustomerStories.setCreatedOn(dateNow);
			contentCustomerStories.setLastModifiedOn(dateNow);
			contentCustomerStoriesRepository.save(contentCustomerStories);
			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("data", contentCustomerStories);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }

	// Update a ContentCustomerStories
	@PutMapping("/contentCustomerStories/update/{id}")
	public HashMap<String, Object> updateContentCustomerStories(@PathVariable(value = "id") Long id,
			@Valid @RequestBody ContentCustomerStoriesDTO contentCustomerStoriesDTO) {

		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		LocalDateTime localNow = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
		Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentCustomerStories contentCustomerStories = contentCustomerStoriesRepository.findById(id).orElse(null);
			contentCustomerStoriesDTO.setCustomerStoriesId(contentCustomerStories.getCustomerStoriesId());
	
			if (contentCustomerStoriesDTO.getTitle() != null) {
				contentCustomerStories.setTitle(convertToEntity(contentCustomerStoriesDTO).getTitle());
			}
			if (contentCustomerStoriesDTO.getStories() != null) {
				contentCustomerStories.setStories(convertToEntity(contentCustomerStoriesDTO).getStories());
			}
			if (contentCustomerStoriesDTO.getImagePath() != null) {
				contentCustomerStories.setImagePath(convertToEntity(contentCustomerStoriesDTO).getImagePath());
			}
			if (contentCustomerStoriesDTO.getDescription() != null) {
				contentCustomerStories.setDescription(convertToEntity(contentCustomerStoriesDTO).getDescription());
			}
			if (contentCustomerStoriesDTO.getIsDeleted() != null) {
				contentCustomerStories.setIsDeleted(convertToEntity(contentCustomerStoriesDTO).getIsDeleted());
			}
			if (contentCustomerStoriesDTO.getLastModifiedBy() != null) {
				contentCustomerStories.setLastModifiedBy(convertToEntity(contentCustomerStoriesDTO).getLastModifiedBy());
			}
			contentCustomerStories.setLastModifiedOn(dateNow);
			ContentCustomerStories updateContentCustomerStories = contentCustomerStoriesRepository.save(contentCustomerStories);
	
			List<ContentCustomerStories> resultList = new ArrayList<ContentCustomerStories>();
			resultList.add(updateContentCustomerStories);

			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);

		return showHashMap;
	}

	// Delete a ContentCustomerStories
	@DeleteMapping("/contentCustomerStories/delete/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentCustomerStories contentCustomerStories = contentCustomerStoriesRepository.findById(id).orElse(null);
			contentCustomerStoriesRepository.delete(contentCustomerStories);

			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
}
