package com.telkom.dtpbe.controllers;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ContentDashboardMenuDTO;
import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.ContentDashboardMenu;
import com.telkom.dtpbe.repositories.ContentDashboardMenuRepository;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ContentDashboardMenuController extends Minio{
	
	private String timeFormat = "yyyy-MM-dd HH:mm:ss";
	private String successMessage = "success";
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ContentDashboardMenuRepository contentDashboardMenuRepository;
	
	public ContentDashboardMenuDTO convertToDTO(ContentDashboardMenu contentDashboardMenu) {
		return modelMapper.map(contentDashboardMenu, ContentDashboardMenuDTO.class);
	}

	public ContentDashboardMenu convertToEntity(ContentDashboardMenuDTO contentDashboardMenuDTO) {
		return modelMapper.map(contentDashboardMenuDTO, ContentDashboardMenu.class);
	}

	// Get All ContentDashboardMenu
	@GetMapping("/contentDashboardMenu/readAll")
	public HashMap<String, Object> getAllContentContactUsDetails() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<HashMap<String, Object>> listContentDashboardMenu = new ArrayList<HashMap<String, Object>>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (ContentDashboardMenu c : contentDashboardMenuRepository.findAll()) {
				ContentDashboardMenuDTO contentDashboardMenuDTO = convertToDTO(c);
				HashMap<String, Object> mapTemp = new HashMap<String, Object>();
				mapTemp.put("ContentDashboardMenuId",contentDashboardMenuDTO.getContentDashboardMenuId());
				mapTemp.put("title",contentDashboardMenuDTO.getTitle());
				mapTemp.put("imagePath",contentDashboardMenuDTO.getImagePath());
				mapTemp.put("description",contentDashboardMenuDTO.getDescription());
				mapTemp.put("isDeleted",contentDashboardMenuDTO.getIsDeleted());
				mapTemp.put("createdBy",contentDashboardMenuDTO.getCreatedBy());
				String createdOn = new SimpleDateFormat(timeFormat).format(contentDashboardMenuDTO.getCreatedOn());
				mapTemp.put("createdOn",createdOn);
				mapTemp.put("lastModifiedBy",contentDashboardMenuDTO.getLastModifiedBy());
				String lastModifiedOn = new SimpleDateFormat(timeFormat).format(contentDashboardMenuDTO.getLastModifiedOn());
				mapTemp.put("lastModifiedOn",lastModifiedOn);
				listContentDashboardMenu.add(mapTemp);
			}
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listContentDashboardMenu.size());
		showHashMap.put("data", listContentDashboardMenu);

		return showHashMap;
	}
	
	// Get All Content Product
	@SuppressWarnings("deprecation")
	@GetMapping("/contentDashboardMenu/getAll")
	public HashMap<String, Object> getAllContentContactUs() throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException {
		HashMap<String, Object> mapResult = new HashMap<String, Object>();
		List<HashMap<String, Object>> listContentContactUs = new ArrayList<HashMap<String, Object>>();
		String imageUrl = null;
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		try {
			for (ContentDashboardMenu c : contentDashboardMenuRepository.findAll(Sort.by(Sort.Direction.ASC, "ContentDashboardMenuId"))) {
				ContentDashboardMenuDTO contentDashboardMenuDTO = convertToDTO(c);
				
				HashMap<String, Object> mapTemp = new HashMap<String, Object>();
				mapTemp.put("ContentDashboardMenuId",contentDashboardMenuDTO.getContentDashboardMenuId());
				mapTemp.put("title",contentDashboardMenuDTO.getTitle());
				mapTemp.put("imagePath",contentDashboardMenuDTO.getImagePath());
				mapTemp.put("description",contentDashboardMenuDTO.getDescription());
				mapTemp.put("url",contentDashboardMenuDTO.getUrl());
				if(c.getImagePath() == null) {
					imageUrl = null;
				} else if (!c.getImagePath().isEmpty()) {
					imageUrl = minio().presignedGetObject(bucketName, c.getImagePath());
				} else {
					imageUrl = null;
				}
				mapTemp.put("imageUrl",imageUrl);
				
				listContentContactUs.add(mapTemp);
			}
			
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		} 
		
		mapResult.put("code", code);
		mapResult.put("success", isSuccess);
		mapResult.put("message", message);
		mapResult.put("total", listContentContactUs.size());
		mapResult.put("data", listContentContactUs);

		return mapResult;
	}
	
	// Read ContentDashboardMenu By ID
	@GetMapping("/contentDashboardMenu/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentDashboardMenu contentDashboardMenu = contentDashboardMenuRepository.findById(id).orElse(null);
			ContentDashboardMenuDTO contentDashboardMenuDTO = convertToDTO(contentDashboardMenu);
			HashMap<String, Object> mapTemp = new HashMap<String, Object>();
			mapTemp.put("ContentDashboardMenuId",contentDashboardMenuDTO.getContentDashboardMenuId());
			mapTemp.put("title",contentDashboardMenuDTO.getTitle());
			mapTemp.put("imagePath",contentDashboardMenuDTO.getImagePath());
			mapTemp.put("description",contentDashboardMenuDTO.getDescription());
			mapTemp.put("isDeleted",contentDashboardMenuDTO.getIsDeleted());
			mapTemp.put("createdBy",contentDashboardMenuDTO.getCreatedBy());
			String createdOn = new SimpleDateFormat(timeFormat).format(contentDashboardMenuDTO.getCreatedOn());
			mapTemp.put("createdOn",createdOn);
			mapTemp.put("lastModifiedBy",contentDashboardMenuDTO.getLastModifiedBy());
			String lastModifiedOn = new SimpleDateFormat(timeFormat).format(contentDashboardMenuDTO.getLastModifiedOn());
			mapTemp.put("lastModifiedOn",lastModifiedOn);
			showHashMap.put("data", mapTemp);
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		
		return showHashMap;
	}

	// Create a new ContentDashboardMenu
	@PostMapping("/contentDashboardMenu/add")
	public HashMap<String, Object> createContentContactUsDetails(@Valid @RequestBody ContentDashboardMenuDTO contentDashboardMenuDTO) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	LocalDateTime localNow = LocalDateTime.now();
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
    	Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
    	Long id = new Long(1);
    	String message;
		boolean isSuccess = false;
		Integer code = null;
		try {
			ContentDashboardMenu contentDashboardMenu = convertToEntity(contentDashboardMenuDTO);
			if(contentDashboardMenuDTO.getIsDeleted() == null) {
				contentDashboardMenu.setIsDeleted(false);
			}
			if(contentDashboardMenuDTO.getCreatedBy() == null) {
				contentDashboardMenu.setCreatedBy(id);
			}
			if(contentDashboardMenuDTO.getLastModifiedBy() == null) {
				contentDashboardMenu.setLastModifiedBy(id);
			}
			contentDashboardMenu.setCreatedOn(dateNow);
			contentDashboardMenu.setLastModifiedOn(dateNow);
			contentDashboardMenuRepository.save(contentDashboardMenu);
			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("data", contentDashboardMenu);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }

	// Update a ContentDashboardMenu
	@PutMapping("/contentDashboardMenu/update/{id}")
	public HashMap<String, Object> updateContentContactUsDetails(@PathVariable(value = "id") Long id,
			@Valid @RequestBody ContentDashboardMenuDTO contentDashboardMenuDTO) {

		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		LocalDateTime localNow = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
		Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentDashboardMenu contentDashboardMenu = contentDashboardMenuRepository.findById(id).orElse(null);
			
			contentDashboardMenuDTO.setContentDashboardMenuId(contentDashboardMenu.getContentDashboardMenuId());
	
			if (contentDashboardMenuDTO.getTitle() != null) {
				contentDashboardMenu.setTitle(convertToEntity(contentDashboardMenuDTO).getTitle());
			}
			if (contentDashboardMenuDTO.getImagePath() != null) {
				contentDashboardMenu.setImagePath(convertToEntity(contentDashboardMenuDTO).getImagePath());
			}
			if (contentDashboardMenuDTO.getDescription() != null) {
				contentDashboardMenu.setDescription(convertToEntity(contentDashboardMenuDTO).getDescription());
			}
			if (contentDashboardMenuDTO.getIsDeleted() != null) {
				contentDashboardMenu.setIsDeleted(convertToEntity(contentDashboardMenuDTO).getIsDeleted());
			}
			if (contentDashboardMenuDTO.getLastModifiedBy() != null) {
				contentDashboardMenu.setLastModifiedBy(convertToEntity(contentDashboardMenuDTO).getLastModifiedBy());
			}
			contentDashboardMenu.setLastModifiedOn(dateNow);
			ContentDashboardMenu updateContentContactUsDetails = contentDashboardMenuRepository.save(contentDashboardMenu);
	
			List<ContentDashboardMenu> resultList = new ArrayList<ContentDashboardMenu>();
			resultList.add(updateContentContactUsDetails);
			
			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		
		return showHashMap;
	}

	// Delete a ContentDashboardMenu
	@DeleteMapping("/contentDashboardMenu/delete/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentDashboardMenu contentDashboardMenu = contentDashboardMenuRepository.findById(id).orElse(null);
			contentDashboardMenuRepository.delete(contentDashboardMenu);

			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
}
