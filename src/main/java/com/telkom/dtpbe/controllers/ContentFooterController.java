package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ContentFooterDTO;
import com.telkom.dtpbe.models.ContentFooter;
import com.telkom.dtpbe.repositories.ContentFooterRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ContentFooterController{

	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ContentFooterRepository contentFooterRepository;

	public ContentFooterDTO convertToDTO(ContentFooter object) {
	        return modelMapper.map(object, ContentFooterDTO.class);
	}

	public ContentFooter convertToEntity(ContentFooterDTO object) {
	    return modelMapper.map(object, ContentFooter.class);
	}
	
	//View All Data
	@GetMapping("/footer/readAll")
	public HashMap<String, Object> getAllData() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<ContentFooterDTO> listData = new ArrayList<ContentFooterDTO>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (ContentFooter tempData : contentFooterRepository.findAll()) {
				ContentFooterDTO tempDataDTO = convertToDTO(tempData);
				listData.add(tempDataDTO);
			}
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listData.size());
		showHashMap.put("data", listData);

		return showHashMap;
	}
	
	//View Data By ID
	@GetMapping("/footer/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentFooter tempData = contentFooterRepository.findById(id).orElse(null);
			ContentFooterDTO tempDataDTO = convertToDTO(tempData);
			showHashMap.put("data", tempDataDTO);
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Create a new data
	@PostMapping("/footer/add")
	public HashMap<String, Object> createData(@Valid @RequestBody ContentFooterDTO dataDto) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentFooter tempData = convertToEntity(dataDto);
			tempData.setIsDeleted(false);
			tempData.setCreatedOn(dateNow);
			tempData.setLastModifiedOn(dateNow);
			dataDto = convertToDTO(contentFooterRepository.save(tempData));
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("data", tempData);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }
	
	// UPDATE BELUM
	@PutMapping("/footer/update/{id}")
	public HashMap<String, Object> updateData(@PathVariable(value="id") Long id, @Valid @RequestBody ContentFooterDTO tempDataDto){
		
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		
		try {
			ContentFooter tempData = contentFooterRepository.findById(id).orElseThrow(null);
			
			tempDataDto.setContentFooterId(tempData.getContentFooterId());
			
			if(tempDataDto.getTitle() != null) {
				tempData.setTitle(convertToEntity(tempDataDto).getTitle());
			}
			
			if(tempDataDto.getDescription() != null) {
				tempData.setDescription(convertToEntity(tempDataDto).getDescription());
			}
			
			if(tempDataDto.getIsDeleted() != null) {
				tempData.setIsDeleted(convertToEntity(tempDataDto).getIsDeleted());
			}
			
			if(tempDataDto.getLastModifiedBy() != null) {
				tempData.setLastModifiedBy(convertToEntity(tempDataDto).getLastModifiedBy());
			}
		
			tempData.setLastModifiedOn(dateNow);
			
			
			ContentFooter updateData = contentFooterRepository.save(tempData);
			
			List<ContentFooterDTO> resultList = new ArrayList<ContentFooterDTO>();
			resultList.add(convertToDTO(updateData));
	
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
			
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Delete a Data
    @DeleteMapping("/footer/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentFooter tempData = contentFooterRepository.findById(id).orElse(null);
			contentFooterRepository.delete(tempData);
	
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
    }
}
