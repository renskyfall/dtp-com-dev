package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ContentHeaderDTO;
import com.telkom.dtpbe.models.ContentHeader;
import com.telkom.dtpbe.repositories.ContentHeaderRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ContentHeaderController{

	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ContentHeaderRepository contentHeaderRepository;

	public ContentHeaderDTO convertToDTO(ContentHeader object) {
	        return modelMapper.map(object, ContentHeaderDTO.class);
	}

	public ContentHeader convertToEntity(ContentHeaderDTO object) {
	    return modelMapper.map(object, ContentHeader.class);
	}
	
	//View All Data
	@GetMapping("/header/readAll")
	public HashMap<String, Object> getAllData() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<ContentHeaderDTO> listData = new ArrayList<ContentHeaderDTO>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (ContentHeader tempData : contentHeaderRepository.findAll()) {
				ContentHeaderDTO tempDataDTO = convertToDTO(tempData);
				listData.add(tempDataDTO);
			}
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listData.size());
		showHashMap.put("data", listData);

		return showHashMap;
	}
	
	//View Data By ID
	@GetMapping("/header/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentHeader tempData = contentHeaderRepository.findById(id).orElse(null);
			ContentHeaderDTO tempDataDTO = convertToDTO(tempData);
			showHashMap.put("data", tempDataDTO);
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Create a new data
	@PostMapping("/header/add")
	public HashMap<String, Object> createData(@Valid @RequestBody ContentHeaderDTO dataDto) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentHeader tempData = convertToEntity(dataDto);
			tempData.setIsDeleted(false);
			tempData.setCreatedOn(dateNow);
			tempData.setLastModifiedOn(dateNow);
			contentHeaderRepository.save(tempData);
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("data", tempData);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }
	
	// UPDATE BELUM
	@PutMapping("/header/update/{id}")
	public HashMap<String, Object> updateData(@PathVariable(value="id") Long id, @Valid @RequestBody ContentHeaderDTO dataDto){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		
		try {
			ContentHeader tempData = contentHeaderRepository.findById(id).orElseThrow(null);
			
			dataDto.setContentHeaderId(tempData.getContentHeaderId());
			
			if(dataDto.getTitle() != null) {
				tempData.setTitle(convertToEntity(dataDto).getTitle());
			}
			
			if(dataDto.getUrl() != null) {
				tempData.setUrl(convertToEntity(dataDto).getUrl());
			}
			
			if(dataDto.getIsDeleted() != null) {
				tempData.setIsDeleted(convertToEntity(dataDto).getIsDeleted());
			}
			
			if(dataDto.getLastModifiedBy() != null) {
				tempData.setLastModifiedBy(convertToEntity(dataDto).getLastModifiedBy());
			}
			
			tempData.setLastModifiedOn(dateNow);
			
			
			ContentHeader updateData = contentHeaderRepository.save(tempData);
			
			List<ContentHeader> resultList = new ArrayList<ContentHeader>();
			resultList.add(updateData);
	
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
			
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Delete a Data
    @DeleteMapping("/header/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentHeader tempData = contentHeaderRepository.findById(id).orElse(null);
			contentHeaderRepository.delete(tempData);
	
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
    }
}
