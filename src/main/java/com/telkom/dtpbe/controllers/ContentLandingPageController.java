package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ContentLandingPageDTO;
import com.telkom.dtpbe.models.ContentLandingPage;
import com.telkom.dtpbe.repositories.ContentLandingPageRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ContentLandingPageController{

	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ContentLandingPageRepository contentLandingPageRepository;

	public ContentLandingPageDTO convertToDTO(ContentLandingPage object) {
	        return modelMapper.map(object, ContentLandingPageDTO.class);
	}

	public ContentLandingPage convertToEntity(ContentLandingPageDTO object) {
	    return modelMapper.map(object, ContentLandingPage.class);
	}
	
	//View All Data
	@GetMapping("/landingPage/readAll")
	public HashMap<String, Object> getAllData() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<ContentLandingPageDTO> listData = new ArrayList<ContentLandingPageDTO>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (ContentLandingPage tempData : contentLandingPageRepository.findAll()) {
				ContentLandingPageDTO tempDataDTO = convertToDTO(tempData);
				listData.add(tempDataDTO);
			}
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listData.size());
		showHashMap.put("data", listData);

		return showHashMap;
	}
	
	//View Data By ID
	@GetMapping("/landingPage/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentLandingPage tempData = contentLandingPageRepository.findById(id).orElse(null);
			ContentLandingPageDTO tempDataDTO = convertToDTO(tempData);
			showHashMap.put("data", tempDataDTO);
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Create a new data
	@PostMapping("/landingPage/add")
	public HashMap<String, Object> createData(@Valid @RequestBody ContentLandingPageDTO dataDto) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentLandingPage tempData = convertToEntity(dataDto);
			tempData.setIsDeleted(false);
			tempData.setCreatedOn(dateNow);
			tempData.setLastModifiedOn(dateNow);
			contentLandingPageRepository.save(tempData);
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("data", tempData);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }
	
	// UPDATE BELUM
	@PutMapping("/landingPage/update/{id}")
	public HashMap<String, Object> updateData(@PathVariable(value="id") Long id, @Valid @RequestBody ContentLandingPageDTO dataDto){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		
		try {
			ContentLandingPage tempData = contentLandingPageRepository.findById(id).orElseThrow(null);
		
			dataDto.setContentLandingPageId(tempData.getContentLandingPageId());
			
			if(dataDto.getImagePath() != null) {
				tempData.setImagePath(convertToEntity(dataDto).getImagePath());
			}
			
			if(dataDto.getDescription() != null) {
				tempData.setDescription(convertToEntity(dataDto).getDescription());
			}
			
			if(dataDto.getIsDeleted() != null) {
				tempData.setIsDeleted(convertToEntity(dataDto).getIsDeleted());
			}
			
			if(dataDto.getCreatedBy() != null){
				tempData.setCreatedBy(convertToEntity(dataDto).getCreatedBy());
			}
			
			if(dataDto.getCreatedOn() != null) {
				tempData.setCreatedOn(convertToEntity(dataDto).getCreatedOn());
			}
			
			if(dataDto.getLastModifiedBy() != null) {
				tempData.setLastModifiedBy(convertToEntity(dataDto).getLastModifiedBy());
			}
			
			tempData.setLastModifiedOn(dateNow);
			
			
			ContentLandingPage updateData = contentLandingPageRepository.save(tempData);
			
			List<ContentLandingPage> resultList = new ArrayList<ContentLandingPage>();
			resultList.add(updateData);
	
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
			
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Delete a Data
    @DeleteMapping("/landingPage/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentLandingPage tempData = contentLandingPageRepository.findById(id).orElse(null);
			contentLandingPageRepository.delete(tempData);
	
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
    }
}
