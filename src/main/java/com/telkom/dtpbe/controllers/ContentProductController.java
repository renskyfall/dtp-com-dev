package com.telkom.dtpbe.controllers;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ContentProductDTO;
import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.ContentProduct;
import com.telkom.dtpbe.repositories.ContentProductRepository;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ContentProductController extends Minio{
	
	private String timeFormat = "yyyy-MM-dd HH:mm:ss";
	private String successMessage = "success";
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ContentProductRepository contentProductRepository;
	
	public ContentProductDTO convertToDTO(ContentProduct contentProduct) {
		return modelMapper.map(contentProduct, ContentProductDTO.class);
	}

	public ContentProduct convertToEntity(ContentProductDTO contentProductDTO) {
		return modelMapper.map(contentProductDTO, ContentProduct.class);
	}

	// Get All contentProduct
	@GetMapping("/contentProduct/readAll")
	public HashMap<String, Object> getAllContentContactUsDetails() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<HashMap<String, Object>> listContentProduct = new ArrayList<HashMap<String, Object>>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (ContentProduct c : contentProductRepository.findAll()) {
				ContentProductDTO contentProductDTO = convertToDTO(c);
				HashMap<String, Object> mapTemp = new HashMap<String, Object>();
				mapTemp.put("contentProductId",contentProductDTO.getContentProductId());
				mapTemp.put("title",contentProductDTO.getTitle());
				mapTemp.put("imagePath",contentProductDTO.getImagePath());
				mapTemp.put("description",contentProductDTO.getDescription());
				mapTemp.put("isDeleted",contentProductDTO.getIsDeleted());
				mapTemp.put("createdBy",contentProductDTO.getCreatedBy());
				String createdOn = new SimpleDateFormat(timeFormat).format(contentProductDTO.getCreatedOn());
				mapTemp.put("createdOn",createdOn);
				mapTemp.put("lastModifiedBy",contentProductDTO.getLastModifiedBy());
				String lastModifiedOn = new SimpleDateFormat(timeFormat).format(contentProductDTO.getLastModifiedOn());
				mapTemp.put("lastModifiedOn",lastModifiedOn);
				listContentProduct.add(mapTemp);
			}
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listContentProduct.size());
		showHashMap.put("data", listContentProduct);

		return showHashMap;
	}
	
	// Get All Content Product
	@SuppressWarnings("deprecation")
	@GetMapping("/contentProduct/getAll")
	public HashMap<String, Object> getAllContentContactUs() throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException {
		HashMap<String, Object> mapResult = new HashMap<String, Object>();
		List<HashMap<String, Object>> listContentContactUs = new ArrayList<HashMap<String, Object>>();
		String imageUrl = null;
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		try {
			for (ContentProduct c : contentProductRepository.findAll()) {
				ContentProductDTO contentProductDTO = convertToDTO(c);
				
				HashMap<String, Object> mapTemp = new HashMap<String, Object>();
				mapTemp.put("contentProductId",contentProductDTO.getContentProductId());
				mapTemp.put("title",contentProductDTO.getTitle());
				mapTemp.put("imagePath",contentProductDTO.getImagePath());
				mapTemp.put("description",contentProductDTO.getDescription());
				if(c.getImagePath() == null) {
					imageUrl = null;
				} else if (!c.getImagePath().isEmpty()) {
					imageUrl = minio().presignedGetObject(bucketName, c.getImagePath());
				} else {
					imageUrl = null;
				}
				mapTemp.put("imageUrl",imageUrl);
				
				listContentContactUs.add(mapTemp);
			}
			
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		} 
		
		mapResult.put("code", code);
		mapResult.put("success", isSuccess);
		mapResult.put("message", message);
		mapResult.put("total", listContentContactUs.size());
		mapResult.put("data", listContentContactUs);

		return mapResult;
	}
	
	// Read contentProduct By ID
	@GetMapping("/contentProduct/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentProduct contentProduct = contentProductRepository.findById(id).orElse(null);
			ContentProductDTO contentProductDTO = convertToDTO(contentProduct);
			HashMap<String, Object> mapTemp = new HashMap<String, Object>();
			mapTemp.put("contentProductId",contentProductDTO.getContentProductId());
			mapTemp.put("title",contentProductDTO.getTitle());
			mapTemp.put("imagePath",contentProductDTO.getImagePath());
			mapTemp.put("description",contentProductDTO.getDescription());
			mapTemp.put("isDeleted",contentProductDTO.getIsDeleted());
			mapTemp.put("createdBy",contentProductDTO.getCreatedBy());
			String createdOn = new SimpleDateFormat(timeFormat).format(contentProductDTO.getCreatedOn());
			mapTemp.put("createdOn",createdOn);
			mapTemp.put("lastModifiedBy",contentProductDTO.getLastModifiedBy());
			String lastModifiedOn = new SimpleDateFormat(timeFormat).format(contentProductDTO.getLastModifiedOn());
			mapTemp.put("lastModifiedOn",lastModifiedOn);
			showHashMap.put("data", mapTemp);
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		
		return showHashMap;
	}

	// Create a new contentProduct
	@PostMapping("/contentProduct/add")
	public HashMap<String, Object> createContentContactUsDetails(@Valid @RequestBody ContentProductDTO contentProductDTO) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	LocalDateTime localNow = LocalDateTime.now();
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
    	Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
    	Long id = new Long(1);
    	String message;
		boolean isSuccess = false;
		Integer code = null;
		try {
			ContentProduct contentProduct = convertToEntity(contentProductDTO);
			if(contentProductDTO.getIsDeleted() == null) {
				contentProduct.setIsDeleted(false);
			}
			if(contentProductDTO.getCreatedBy() == null) {
				contentProduct.setCreatedBy(id);
			}
			if(contentProductDTO.getLastModifiedBy() == null) {
				contentProduct.setLastModifiedBy(id);
			}
			contentProduct.setCreatedOn(dateNow);
			contentProduct.setLastModifiedOn(dateNow);
			contentProductRepository.save(contentProduct);
			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("data", contentProduct);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }

	// Update a contentProduct
	@PutMapping("/contentProduct/update/{id}")
	public HashMap<String, Object> updateContentContactUsDetails(@PathVariable(value = "id") Long id,
			@Valid @RequestBody ContentProductDTO contentProductDTO) {

		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		LocalDateTime localNow = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
		Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentProduct contentProduct = contentProductRepository.findById(id).orElse(null);
			
			contentProductDTO.setContentProductId(contentProduct.getContentProductId());
	
			if (contentProductDTO.getTitle() != null) {
				contentProduct.setTitle(convertToEntity(contentProductDTO).getTitle());
			}
			if (contentProductDTO.getImagePath() != null) {
				contentProduct.setImagePath(convertToEntity(contentProductDTO).getImagePath());
			}
			if (contentProductDTO.getDescription() != null) {
				contentProduct.setDescription(convertToEntity(contentProductDTO).getDescription());
			}
			if (contentProductDTO.getIsDeleted() != null) {
				contentProduct.setIsDeleted(convertToEntity(contentProductDTO).getIsDeleted());
			}
			if (contentProductDTO.getLastModifiedBy() != null) {
				contentProduct.setLastModifiedBy(convertToEntity(contentProductDTO).getLastModifiedBy());
			}
			contentProduct.setLastModifiedOn(dateNow);
			ContentProduct updateContentContactUsDetails = contentProductRepository.save(contentProduct);
	
			List<ContentProduct> resultList = new ArrayList<ContentProduct>();
			resultList.add(updateContentContactUsDetails);
			
			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		
		return showHashMap;
	}

	// Delete a contentProduct
	@DeleteMapping("/contentProduct/delete/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ContentProduct contentProduct = contentProductRepository.findById(id).orElse(null);
			contentProductRepository.delete(contentProduct);

			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
}
