package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ContentHeaderDTO;
import com.telkom.dtpbe.dtos.GuideDTO;
import com.telkom.dtpbe.models.ContentHeader;
import com.telkom.dtpbe.models.Guide;
import com.telkom.dtpbe.repositories.ContentHeaderRepository;
import com.telkom.dtpbe.repositories.GuideRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class GuideController{

	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	GuideRepository guideRepository;

	public GuideDTO convertToDTO(Guide object) {
	        return modelMapper.map(object, GuideDTO.class);
	}

	public Guide convertToEntity(GuideDTO object) {
	    return modelMapper.map(object, Guide.class);
	}
	
	//View All Data
	@GetMapping("/guide/readAll")
	public HashMap<String, Object> getAllData() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<GuideDTO> listData = new ArrayList<GuideDTO>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (Guide tempData : guideRepository.findAll()) {
				GuideDTO tempDataDTO = convertToDTO(tempData);
				listData.add(tempDataDTO);
			}
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listData.size());
		showHashMap.put("data", listData);

		return showHashMap;
	}
	
	//View Data By ID
	@GetMapping("/guide/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Guide tempData = guideRepository.findById(id).orElse(null);
			GuideDTO tempDataDTO = convertToDTO(tempData);
			showHashMap.put("data", tempDataDTO);
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Create a new data
	@PostMapping("/guide/add")
	public HashMap<String, Object> createData(@Valid @RequestBody GuideDTO dataDto) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Guide tempData = convertToEntity(dataDto);
			tempData.setIsDeleted(false);
			tempData.setCreatedOn(dateNow);
			tempData.setLastModifiedOn(dateNow);
			guideRepository.save(tempData);
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("data", tempData);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }
	
	// UPDATE 
	@PutMapping("/guide/update/{id}")
	public HashMap<String, Object> updateData(@PathVariable(value="id") Long id, @Valid @RequestBody GuideDTO dataDto){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		
		try {
			Guide tempData = guideRepository.findById(id).orElseThrow(null);
			
			dataDto.setGuideId(tempData.getGuideId());
			
			if(dataDto.getProductId() != null) {
				tempData.setProductId(convertToEntity(dataDto).getProductId());
			}
			
			if(dataDto.getGuideContent() != null) {
				tempData.setGuideContent(convertToEntity(dataDto).getGuideContent());
			}
			
			if(dataDto.getDescription() != null) {
				tempData.setDescription(convertToEntity(dataDto).getDescription());
			}
			
			if(dataDto.getIsDeleted() != null) {
				tempData.setIsDeleted(convertToEntity(dataDto).getIsDeleted());
			}
			
			if(dataDto.getLastModifiedBy() != null) {
				tempData.setLastModifiedBy(convertToEntity(dataDto).getLastModifiedBy());
			}
			
			tempData.setLastModifiedOn(dateNow);
			
			
			Guide updateData = guideRepository.save(tempData);
			
			List<Guide> resultList = new ArrayList<Guide>();
			resultList.add(updateData);
	
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
			
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Delete a Data
    @DeleteMapping("/guide/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Guide tempData = guideRepository.findById(id).orElse(null);
			guideRepository.delete(tempData);
	
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
    }
}
