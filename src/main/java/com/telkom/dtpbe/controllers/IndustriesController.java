package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.IndustriesDTO;
import com.telkom.dtpbe.models.Industries;
import com.telkom.dtpbe.repositories.IndustriesRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class IndustriesController {
	
	private String timeFormat = "yyyy-MM-dd HH:mm:ss";
	private String successMessage = "success";
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	IndustriesRepository industriesRepository;
	
	public IndustriesDTO convertToDTO(Industries industries) {
		return modelMapper.map(industries, IndustriesDTO.class);
	}

	public Industries convertToEntity(IndustriesDTO industriesDTO) {
		return modelMapper.map(industriesDTO, Industries.class);
	}
	
	// Get All Industries
	@GetMapping("/industries/readAll")
	public HashMap<String, Object> getAllIndustries() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<HashMap<String, Object>> listIndustriess = new ArrayList<HashMap<String, Object>>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (Industries c : industriesRepository.findAll()) {
				IndustriesDTO industriesDTO = convertToDTO(c);
				HashMap<String, Object> mapTemp = new HashMap<String, Object>();
				mapTemp.put("industriesId",industriesDTO.getIndustriesId());
				mapTemp.put("industriesName",industriesDTO.getIndustriesName());
				mapTemp.put("description",industriesDTO.getDescription());
				mapTemp.put("isDeleted",industriesDTO.getIsDeleted());
				mapTemp.put("createdBy",industriesDTO.getCreatedBy());
				String createdOn = new SimpleDateFormat(timeFormat).format(industriesDTO.getCreatedOn());
				mapTemp.put("createdOn",createdOn);
				mapTemp.put("lastModifiedBy",industriesDTO.getLastModifiedBy());
				String lastModifiedOn = new SimpleDateFormat(timeFormat).format(industriesDTO.getLastModifiedOn());
				mapTemp.put("lastModifiedOn",lastModifiedOn);
				listIndustriess.add(mapTemp);
			}
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listIndustriess.size());
		showHashMap.put("data", listIndustriess);

		return showHashMap;
	}

	// Read Industries By ID
	@GetMapping("/industries/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Industries industries = industriesRepository.findById(id).orElse(null);
			IndustriesDTO industriesDTO = convertToDTO(industries);
			HashMap<String, Object> mapTemp = new HashMap<String, Object>();
			mapTemp.put("industriesId",industriesDTO.getIndustriesId());
			mapTemp.put("industriesName",industriesDTO.getIndustriesName());
			mapTemp.put("description",industriesDTO.getDescription());
			mapTemp.put("isDeleted",industriesDTO.getIsDeleted());
			mapTemp.put("createdBy",industriesDTO.getCreatedBy());
			String createdOn = new SimpleDateFormat(timeFormat).format(industriesDTO.getCreatedOn());
			mapTemp.put("createdOn",createdOn);
			mapTemp.put("lastModifiedBy",industriesDTO.getLastModifiedBy());
			String lastModifiedOn = new SimpleDateFormat(timeFormat).format(industriesDTO.getLastModifiedOn());
			mapTemp.put("lastModifiedOn",lastModifiedOn);
			showHashMap.put("data", mapTemp);
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}

	// Create a new Industries
	@PostMapping("/industries/add")
	public HashMap<String, Object> createIndustries(@Valid @RequestBody IndustriesDTO industriesDTO) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	LocalDateTime localNow = LocalDateTime.now();
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
    	Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
    	Long id = new Long(1);
		String message;
		boolean isSuccess = false;
		Integer code = null;
		try {
			Industries industries = convertToEntity(industriesDTO);
			if(industriesDTO.getIsDeleted() == null) {
				industries.setIsDeleted(false);
			}
			if(industriesDTO.getCreatedBy() == null) {
				industries.setCreatedBy(id);
			}
			if(industriesDTO.getLastModifiedBy() == null) {
				industries.setLastModifiedBy(id);
			}
			industries.setCreatedOn(dateNow);
			industries.setLastModifiedOn(dateNow);
			industriesRepository.save(industries);
			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("data", industries);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }

	// Update a Industries
	@PutMapping("/industries/update/{id}")
	public HashMap<String, Object> updateIndustries(@PathVariable(value = "id") Long id,
			@Valid @RequestBody IndustriesDTO industriesDTO) {

		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		LocalDateTime localNow = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
		Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Industries industries = industriesRepository.findById(id).orElse(null);
			industriesDTO.setIndustriesId(industries.getIndustriesId());
	
			if (industriesDTO.getIndustriesName() != null) {
				industries.setIndustriesName(convertToEntity(industriesDTO).getIndustriesName());
			}
			if (industriesDTO.getDescription() != null) {
				industries.setDescription(convertToEntity(industriesDTO).getDescription());
			}
			if (industriesDTO.getIsDeleted() != null) {
				industries.setIsDeleted(convertToEntity(industriesDTO).getIsDeleted());
			}
			if (industriesDTO.getLastModifiedBy() != null) {
				industries.setLastModifiedBy(convertToEntity(industriesDTO).getLastModifiedBy());
			}
			industries.setLastModifiedOn(dateNow);
			Industries updateIndustries = industriesRepository.save(industries);
	
			List<Industries> resultList = new ArrayList<Industries>();
			resultList.add(updateIndustries);

			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);

		return showHashMap;
	}

	// Delete a Industries
	@DeleteMapping("/industries/delete/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Industries industries = industriesRepository.findById(id).orElse(null);
			industriesRepository.delete(industries);

			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
}
