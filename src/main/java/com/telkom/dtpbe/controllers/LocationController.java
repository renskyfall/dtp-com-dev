package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.LocationDTO;
import com.telkom.dtpbe.models.Location;
import com.telkom.dtpbe.repositories.LocationRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class LocationController {
	
	private String timeFormat = "yyyy-MM-dd HH:mm:ss";
	private String successMessage = "success";
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	LocationRepository locationRepository;
	
	public LocationDTO convertToDTO(Location location) {
		return modelMapper.map(location, LocationDTO.class);
	}

	public Location convertToEntity(LocationDTO locationDTO) {
		return modelMapper.map(locationDTO, Location.class);
	}
	
	// Get All Location
	@GetMapping("/location/readAll")
	public HashMap<String, Object> getAllLocation() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<HashMap<String, Object>> listLocations = new ArrayList<HashMap<String, Object>>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (Location l : locationRepository.findAll()) {
				LocationDTO locationDTO = convertToDTO(l);
				HashMap<String, Object> mapTemp = new HashMap<String, Object>();
				mapTemp.put("locationId",locationDTO.getLocationId());
				mapTemp.put("latitude",locationDTO.getLatitude());
				mapTemp.put("longitude",locationDTO.getLongitude());
				mapTemp.put("description",locationDTO.getDescription());
				mapTemp.put("isDeleted",locationDTO.getIsDeleted());
				mapTemp.put("createdBy",locationDTO.getCreatedBy());
				String createdOn = new SimpleDateFormat(timeFormat).format(locationDTO.getCreatedOn());
				mapTemp.put("createdOn",createdOn);
				mapTemp.put("lastModifiedBy",locationDTO.getLastModifiedBy());
				String lastModifiedOn = new SimpleDateFormat(timeFormat).format(locationDTO.getLastModifiedOn());
				mapTemp.put("lastModifiedOn",lastModifiedOn);
				listLocations.add(mapTemp);
			}
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("data", listLocations);

		return showHashMap;
	}

	// Read Location By ID
	@GetMapping("/location/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Location location = locationRepository.findById(id).orElse(null);
			LocationDTO locationDTO = convertToDTO(location);
			HashMap<String, Object> mapTemp = new HashMap<String, Object>();
			mapTemp.put("locationId",locationDTO.getLocationId());
			mapTemp.put("latitude",locationDTO.getLatitude());
			mapTemp.put("longitude",locationDTO.getLongitude());
			mapTemp.put("description",locationDTO.getDescription());
			mapTemp.put("isDeleted",locationDTO.getIsDeleted());
			mapTemp.put("createdBy",locationDTO.getCreatedBy());
			String createdOn = new SimpleDateFormat(timeFormat).format(locationDTO.getCreatedOn());
			mapTemp.put("createdOn",createdOn);
			mapTemp.put("lastModifiedBy",locationDTO.getLastModifiedBy());
			String lastModifiedOn = new SimpleDateFormat(timeFormat).format(locationDTO.getLastModifiedOn());
			mapTemp.put("lastModifiedOn",lastModifiedOn);
			showHashMap.put("data", mapTemp);
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		
		return showHashMap;
	}

	// Create a new Location
	@PostMapping("/location/add")
	public HashMap<String, Object> createLocation(@Valid @RequestBody LocationDTO locationDTO) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	LocalDateTime localNow = LocalDateTime.now();
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
    	Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
    	Long id = new Long(1);
    	String message;
		boolean isSuccess = false;
		Integer code = null;
		try {
			Location location = convertToEntity(locationDTO);
			if(locationDTO.getIsDeleted() == null) {
				location.setIsDeleted(false);
			}
			if(locationDTO.getCreatedBy() == null) {
				location.setCreatedBy(id);
			}
			if(locationDTO.getLastModifiedBy() == null) {
				location.setLastModifiedBy(id);
			}
			location.setCreatedOn(dateNow);
			location.setLastModifiedOn(dateNow);
			locationRepository.save(location);
			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("data", location);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }

	// Update a Location
	@PutMapping("/location/update/{id}")
	public HashMap<String, Object> updateLocation(@PathVariable(value = "id") Long id,
			@Valid @RequestBody LocationDTO locationDTO) {

		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		LocalDateTime localNow = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
		Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		try {
			Location location = locationRepository.findById(id).orElse(null);
			
			locationDTO.setLocationId(location.getLocationId());
	
			if (locationDTO.getLatitude() != null) {
				location.setLatitude(convertToEntity(locationDTO).getLatitude());
			}
			if (locationDTO.getLongitude() != null) {
				location.setLongitude(convertToEntity(locationDTO).getLongitude());
			}
			if (locationDTO.getDescription() != null) {
				location.setDescription(convertToEntity(locationDTO).getDescription());
			}
			if (locationDTO.getIsDeleted() != null) {
				location.setIsDeleted(convertToEntity(locationDTO).getIsDeleted());
			}
			if (locationDTO.getLastModifiedBy() != null) {
				location.setLastModifiedBy(convertToEntity(locationDTO).getLastModifiedBy());
			}
			location.setLastModifiedOn(dateNow);
			Location updateLocation = locationRepository.save(location);
	
			List<Location> resultList = new ArrayList<Location>();
			resultList.add(updateLocation);
			
			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		
		return showHashMap;
	}

	// Delete a Location
	@DeleteMapping("/location/delete/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Location location = locationRepository.findById(id).orElse(null);
			locationRepository.delete(location);

			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
}
