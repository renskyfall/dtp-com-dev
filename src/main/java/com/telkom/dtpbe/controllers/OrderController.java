package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.OrderDTO;
import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.Order;
import com.telkom.dtpbe.repositories.OrderRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class OrderController extends Minio{

	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	OrderRepository orderRepository;
	
	public OrderDTO convertToDTO(Order object) {
	        return modelMapper.map(object, OrderDTO.class);
	}

	public Order convertToEntity(OrderDTO object) {
	    return modelMapper.map(object, Order.class);
	}
	
	//View All Data
	@GetMapping("/order/readAll")
	public HashMap<String, Object> getAllData() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<OrderDTO> listData = new ArrayList<OrderDTO>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (Order tempData : orderRepository.findAll()) {
				OrderDTO tempDataDTO = convertToDTO(tempData);
				listData.add(tempDataDTO);
			}
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listData.size());
		showHashMap.put("data", listData);

		return showHashMap;
	}
	
	//View Data By ID
	@GetMapping("/order/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") String id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Order tempData = orderRepository.findById(id).orElse(null);
			OrderDTO tempDataDTO = convertToDTO(tempData);
			showHashMap.put("data", tempDataDTO);
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Create a new data
	@PostMapping("/order/add")
	public HashMap<String, Object> createData(@Valid @RequestBody OrderDTO dataDto) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Order tempData = convertToEntity(dataDto);
			tempData.setIsDeleted(false);
			tempData.setCreatedOn(dateNow);
			tempData.setLastModifiedOn(dateNow);
			dataDto = convertToDTO(orderRepository.save(tempData));
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("data", tempData);
			
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }
	
	// UPDATE BELUM
	@PutMapping("/order/update/{id}")
	public HashMap<String, Object> updateData(@PathVariable(value="id") String id, @Valid @RequestBody OrderDTO tempDataDto){
		
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		
		try {
			Order tempData = orderRepository.findById(id).orElseThrow(null);
			
			tempDataDto.setOrdersId(tempData.getOrdersId());
			
			if(tempDataDto.getInvoiceNumber() != null) {
				tempData.setInvoiceNumber(convertToEntity(tempDataDto).getInvoiceNumber());
			}
			
			if(tempDataDto.getUserId() != null) {
				tempData.setUserId(convertToEntity(tempDataDto).getUserId());
			}
			
			if(tempDataDto.getPaymentMethod() != null) {
				tempData.setPaymentMethod(convertToEntity(tempDataDto).getPaymentMethod());
			}
			if(tempDataDto.getPaymentMethodName() != null) {
				tempData.setPaymentMethodName(convertToEntity(tempDataDto).getPaymentMethodName());
			}
			
			if(tempDataDto.getVaNumber() != null) {
				tempData.setVaNumber(convertToEntity(tempDataDto).getVaNumber());
			}
			
			if(tempDataDto.getTotalPrice() != null) {
				tempData.setTotalPrice(convertToEntity(tempDataDto).getTotalPrice());
			}
			
			if(tempDataDto.getDiscount() != null) {
				tempData.setDiscount(convertToEntity(tempDataDto).getDiscount());
			}
			
			if(tempDataDto.getAdminFee() != null) {
				tempData.setAdminFee(convertToEntity(tempDataDto).getAdminFee());
			}
			
			if(tempDataDto.getGrandTotal() != null) {
				tempData.setGrandTotal(convertToEntity(tempDataDto).getGrandTotal());
			}
			
			if(tempDataDto.getOrderDate() != null) {
				tempData.setOrderDate(convertToEntity(tempDataDto).getOrderDate());
			}
			
			if(tempDataDto.getExpiredDate() != null) {
				tempData.setExpiredDate(convertToEntity(tempDataDto).getExpiredDate());
			}
			
			if(tempDataDto.getActiveDate() != null) {
				tempData.setActiveDate(convertToEntity(tempDataDto).getActiveDate());
			}
			
			if(tempDataDto.getDueDate() != null) {
				tempData.setDueDate(convertToEntity(tempDataDto).getDueDate());
			}
			
			if(tempDataDto.getCancelReason() != null) {
				tempData.setCancelReason(convertToEntity(tempDataDto).getCancelReason());
			}
			
			if(tempDataDto.getProducts() != null) {
				tempData.setProducts(convertToEntity(tempDataDto).getProducts());
			}
			
			if(tempDataDto.getIsDeleted() != null) {
				tempData.setIsDeleted(convertToEntity(tempDataDto).getIsDeleted());
			}
			
			if(tempDataDto.getLastModifiedBy() != null) {
				tempData.setLastModifiedBy(convertToEntity(tempDataDto).getLastModifiedBy());
			}
		
			tempData.setLastModifiedOn(dateNow);
			
			
			Order updateData = orderRepository.save(tempData);
			OrderDTO updateDataDTO = convertToDTO(updateData);
			
			List<OrderDTO> resultList = new ArrayList<OrderDTO>();
			resultList.add(updateDataDTO);
	
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
			
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Delete a Data
    @DeleteMapping("/order/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") String id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Order tempData = orderRepository.findById(id).orElse(null);
			orderRepository.delete(tempData);
	
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
    }
    
}
