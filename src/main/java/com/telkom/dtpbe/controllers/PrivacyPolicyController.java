package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.PrivacyPolicyDTO;
import com.telkom.dtpbe.models.PrivacyPolicy;
import com.telkom.dtpbe.repositories.PrivacyPolicyRepository;

@CrossOrigin(allowCredentials="true", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class PrivacyPolicyController{

	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	PrivacyPolicyRepository privacyPolicyRepository;

	public PrivacyPolicyDTO convertToDTO(PrivacyPolicy object) {
	        return modelMapper.map(object, PrivacyPolicyDTO.class);
	}

	public PrivacyPolicy convertToEntity(PrivacyPolicyDTO object) {
	    return modelMapper.map(object, PrivacyPolicy.class);
	}
	
	//View All Data
	@GetMapping("/privacyPolicy/readAll")
	public HashMap<String, Object> getAllData() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<PrivacyPolicyDTO> listData = new ArrayList<PrivacyPolicyDTO>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (PrivacyPolicy tempData : privacyPolicyRepository.findAll()) {
				PrivacyPolicyDTO tempDataDTO = convertToDTO(tempData);
				listData.add(tempDataDTO);
			}
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listData.size());
		showHashMap.put("data", listData);

		return showHashMap;
	}
	
	//View Data By ID
	@GetMapping("/privacyPolicy/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			PrivacyPolicy tempData = privacyPolicyRepository.findById(id).orElse(null);
			PrivacyPolicyDTO tempDataDTO = convertToDTO(tempData);
			showHashMap.put("data", tempDataDTO);
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Create a new data
	@PostMapping("/privacyPolicy/add")
	public HashMap<String, Object> createData(@Valid @RequestBody PrivacyPolicyDTO dataDto) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			PrivacyPolicy tempData = convertToEntity(dataDto);
			tempData.setIsDeleted(false);
			tempData.setCreatedOn(dateNow);
			tempData.setLastModifiedOn(dateNow);
			dataDto = convertToDTO(privacyPolicyRepository.save(tempData));
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("data", tempData);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }
	
	// UPDATE 
	@PutMapping("/privacyPolicy/update/{id}")
	public HashMap<String, Object> updateData(@PathVariable(value="id") Long id, @Valid @RequestBody PrivacyPolicyDTO tempDataDto){
		
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		
		try {
			PrivacyPolicy tempData = privacyPolicyRepository.findById(id).orElseThrow(null);
			
			tempDataDto.setPrivacyPolicyId(tempData.getPrivacyPolicyId());
			
			if(tempDataDto.getTitle() != null) {
				tempData.setTitle(convertToEntity(tempDataDto).getTitle());
			}
			
			if(tempDataDto.getFilePath() != null) {
				tempData.setFilePath(convertToEntity(tempDataDto).getFilePath());
			}
			
			if(tempDataDto.getDescription() != null) {
				tempData.setDescription(convertToEntity(tempDataDto).getDescription());
			}
			
			if(tempDataDto.getIsDeleted() != null) {
				tempData.setIsDeleted(convertToEntity(tempDataDto).getIsDeleted());
			}
			
			if(tempDataDto.getLastModifiedBy() != null) {
				tempData.setLastModifiedBy(convertToEntity(tempDataDto).getLastModifiedBy());
			}
		
			tempData.setLastModifiedOn(dateNow);
			
			
			PrivacyPolicy updateData = privacyPolicyRepository.save(tempData);
			
			List<PrivacyPolicyDTO> resultList = new ArrayList<PrivacyPolicyDTO>();
			resultList.add(convertToDTO(updateData));
	
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
			
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Delete a Data
    @DeleteMapping("/privacyPolicy/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			PrivacyPolicy tempData = privacyPolicyRepository.findById(id).orElse(null);
			privacyPolicyRepository.delete(tempData);
	
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
    }
}
