package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.core.email.EmailMessage;
import com.telkom.dtpbe.core.email.IEmailService;
import com.telkom.dtpbe.core.shared.Result;
import com.telkom.dtpbe.dtos.ForgotPassword;
import com.telkom.dtpbe.dtos.ProblemReportDTO;
import com.telkom.dtpbe.models.ProblemReport;
import com.telkom.dtpbe.repositories.ProblemReportRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ProblemReportController {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(ForgotPassword.class);
	
	private String timeFormat = "yyyy-MM-dd HH:mm:ss";
	private String successMessage = "success";
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Value("${spring.mail.username}")
	private String username;
	
	@Autowired
	ProblemReportRepository problemReportRepository;
	
	@Autowired
    private IEmailService emailSender;
	
	public ProblemReportDTO convertToDTO(ProblemReport problemReport) {
		return modelMapper.map(problemReport, ProblemReportDTO.class);
	}

	public ProblemReport convertToEntity(ProblemReportDTO problemReportDTO) {
		return modelMapper.map(problemReportDTO, ProblemReport.class);
	}
	
	// Get All ProblemReport
	@GetMapping("/problemReport/readAll")
	public HashMap<String, Object> getAllProblemReport() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<HashMap<String, Object>> listProblemReports = new ArrayList<HashMap<String, Object>>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (ProblemReport p : problemReportRepository.findAll()) {
				ProblemReportDTO problemReportDTO = convertToDTO(p);
				HashMap<String, Object> mapTemp = new HashMap<String, Object>();
				mapTemp.put("problemReportId",problemReportDTO.getProblemReportId());
				mapTemp.put("email",problemReportDTO.getEmail());
				mapTemp.put("subject",problemReportDTO.getSubject());
				mapTemp.put("description",problemReportDTO.getDescription());
				mapTemp.put("isDeleted",problemReportDTO.getIsDeleted());
				mapTemp.put("createdBy",problemReportDTO.getCreatedBy());
				String createdOn = new SimpleDateFormat(timeFormat).format(problemReportDTO.getCreatedOn());
				mapTemp.put("createdOn",createdOn);
				mapTemp.put("lastModifiedBy",problemReportDTO.getLastModifiedBy());
				String lastModifiedOn = new SimpleDateFormat(timeFormat).format(problemReportDTO.getLastModifiedOn());
				mapTemp.put("lastModifiedOn",lastModifiedOn);
				listProblemReports.add(mapTemp);
			}
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listProblemReports.size());
		showHashMap.put("data", listProblemReports);

		return showHashMap;
	}

	// Read ProblemReport By ID
	@GetMapping("/problemReport/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ProblemReport problemReport = problemReportRepository.findById(id).orElse(null);
			ProblemReportDTO problemReportDTO = convertToDTO(problemReport);
			HashMap<String, Object> mapTemp = new HashMap<String, Object>();
			mapTemp.put("problemReportId",problemReportDTO.getProblemReportId());
			mapTemp.put("email",problemReportDTO.getEmail());
			mapTemp.put("subject",problemReportDTO.getSubject());
			mapTemp.put("description",problemReportDTO.getDescription());
			mapTemp.put("isDeleted",problemReportDTO.getIsDeleted());
			mapTemp.put("createdBy",problemReportDTO.getCreatedBy());
			String createdOn = new SimpleDateFormat(timeFormat).format(problemReportDTO.getCreatedOn());
			mapTemp.put("createdOn",createdOn);
			mapTemp.put("lastModifiedBy",problemReportDTO.getLastModifiedBy());
			String lastModifiedOn = new SimpleDateFormat(timeFormat).format(problemReportDTO.getLastModifiedOn());
			mapTemp.put("lastModifiedOn",lastModifiedOn);
			showHashMap.put("data", mapTemp);
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		
		return showHashMap;
	}

	// Create a new ProblemReport
	@PostMapping("/problemReport/add")
	public HashMap<String, Object> createProblemReport(@Valid @RequestBody ProblemReportDTO problemReportDTO) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	LocalDateTime localNow = LocalDateTime.now();
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
    	Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
    	Long id = new Long(1);
    	String message;
		boolean isSuccess = false;
		Integer code = null;
		try {
			ProblemReport problemReport = convertToEntity(problemReportDTO);
			if(problemReportDTO.getIsDeleted() == null) {
				problemReport.setIsDeleted(false);
			}
			if(problemReportDTO.getCreatedBy() == null) {
				problemReport.setCreatedBy(id);
			}
			if(problemReportDTO.getLastModifiedBy() == null) {
				problemReport.setLastModifiedBy(id);
			}
			problemReport.setCreatedOn(dateNow);
			problemReport.setLastModifiedOn(dateNow);
			problemReportRepository.save(problemReport);
			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("data", problemReport);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }
	
	// Create a new ProblemReport With Send Email
	@PostMapping("/problemReport/addWithEmail")
	public HashMap<String, Object> createProblemReportSendEmail(@Valid @RequestBody ProblemReportDTO problemReportDTO) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	LocalDateTime localNow = LocalDateTime.now();
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
    	Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
    	Long id = new Long(1);
    	String message;
		boolean isSuccess = false;
		Integer code = null;
		try {
			ProblemReport problemReport = convertToEntity(problemReportDTO);
			if(problemReportDTO.getIsDeleted() == null) {
				problemReport.setIsDeleted(false);
			}
			if(problemReportDTO.getCreatedBy() == null) {
				problemReport.setCreatedBy(id);
			}
			if(problemReportDTO.getLastModifiedBy() == null) {
				problemReport.setLastModifiedBy(id);
			}
			problemReport.setCreatedOn(dateNow);
			problemReport.setLastModifiedOn(dateNow);
			problemReportRepository.save(problemReport);
			
			Map<String, String> result = new HashMap<>();
			String messageBody = "Thanks for filling out our form!\r\n" + 
			 		"We have received your message. Our team will process it as soon as possible.\r\n" + 
			 		"\r\n" + 
			 		"Best Regards\r\n" + 
			 		"DTP's team";
            LOGGER.info(problemReportDTO.getEmail());
            List<String> to = new ArrayList<>(Arrays.asList(problemReportDTO.getEmail()));
            EmailMessage emailMessage = new EmailMessage.Builder()
                     .setSubject("Confirming Request")
                     .setFrom(username)
                     .setTo(to)
                     .setMessage(messageBody)
                     .build();
            CompletableFuture.runAsync(() -> {
                 Future<Result<String, String>> futureEmail = emailSender.send(emailMessage);
                 LOGGER.info("response: -- {} --");
                 try {
                     LOGGER.info("response: -- {} --", futureEmail.get().getData());
                     result.put("data", futureEmail.get().getData());
                 } catch (InterruptedException e) {
                     LOGGER.error("sending email error : {}", e.getLocalizedMessage());
                 } catch (ExecutionException e) {
                     LOGGER.error("sending email error : {}", e.getLocalizedMessage());
                 } catch (Exception e) {
                 	LOGGER.error("sending sms error : {}", e.getLocalizedMessage());
                 }
            });
			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("data", problemReport);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }
		
	// Update a ProblemReport
	@PutMapping("/problemReport/update/{id}")
	public HashMap<String, Object> updateProblemReport(@PathVariable(value = "id") Long id,
			@Valid @RequestBody ProblemReportDTO problemReportDTO) {

		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		LocalDateTime localNow = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
		Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		try {
			ProblemReport problemReport = problemReportRepository.findById(id).orElse(null);
			
			problemReportDTO.setProblemReportId(problemReport.getProblemReportId());
	
			if (problemReportDTO.getEmail() != null) {
				problemReport.setEmail(convertToEntity(problemReportDTO).getEmail());
			}
			if (problemReportDTO.getSubject() != null) {
				problemReport.setSubject(convertToEntity(problemReportDTO).getSubject());
			}
			if (problemReportDTO.getDescription() != null) {
				problemReport.setDescription(convertToEntity(problemReportDTO).getDescription());
			}
			if (problemReportDTO.getIsDeleted() != null) {
				problemReport.setIsDeleted(convertToEntity(problemReportDTO).getIsDeleted());
			}
			if (problemReportDTO.getLastModifiedBy() != null) {
				problemReport.setLastModifiedBy(convertToEntity(problemReportDTO).getLastModifiedBy());
			}
			problemReport.setLastModifiedOn(dateNow);
			ProblemReport updateProblemReport = problemReportRepository.save(problemReport);
	
			List<ProblemReport> resultList = new ArrayList<ProblemReport>();
			resultList.add(updateProblemReport);
			
			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		
		return showHashMap;
	}

	// Delete a ProblemReport
	@DeleteMapping("/problemReport/delete/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			ProblemReport problemReport = problemReportRepository.findById(id).orElse(null);
			problemReportRepository.delete(problemReport);

			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
}
