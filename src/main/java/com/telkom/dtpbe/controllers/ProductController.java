package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ProductDTO;
import com.telkom.dtpbe.models.Product;
import com.telkom.dtpbe.repositories.ProductRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ProductController{

	private String timeFormat = "yyyy-MM-dd HH:mm:ss";
	private String successMessage = "success";
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ProductRepository productRepository;

	public ProductDTO convertToDTO(Product object) {
	        return modelMapper.map(object, ProductDTO.class);
	}

	public Product convertToEntity(ProductDTO object) {
	    return modelMapper.map(object, Product.class);
	}
	
	//View All Data
	@GetMapping("/product/readAll")
	public HashMap<String, Object> getAllData() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<ProductDTO> listData = new ArrayList<ProductDTO>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (Product tempData : productRepository.findAll()) {
				ProductDTO tempDataDTO = convertToDTO(tempData);
				listData.add(tempDataDTO);
			}
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listData.size());
		showHashMap.put("data", listData);

		return showHashMap;
	}
	
	//View Data By ID
	@GetMapping("/product/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Product tempData = productRepository.findById(id).orElse(null);
			ProductDTO tempDataDTO = convertToDTO(tempData);
			showHashMap.put("data", tempDataDTO);
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Create a new data
	@PostMapping("/product/add")
	public HashMap<String, Object> createData(@Valid @RequestBody ProductDTO dataDto) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		LocalDateTime localNow = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
		Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Product tempData = convertToEntity(dataDto);
			tempData.setIsDeleted(false);
			tempData.setCreatedOn(dateNow);
			tempData.setLastModifiedOn(dateNow);
			productRepository.save(tempData);
			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("data", tempData);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }
	
	// UPDATE BELUM
	@PutMapping("/product/update/{id}")
	public HashMap<String, Object> updateData(@PathVariable(value="id") Long id, @Valid @RequestBody ProductDTO tempDataDto){
		
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		LocalDateTime localNow = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat);
		Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Product tempData = productRepository.findById(id).orElseThrow(null);
			
			tempDataDto.setProductId(tempData.getProductId());
			
			if(tempDataDto.getBrand() != null) {
				tempData.setBrand(convertToEntity(tempDataDto).getBrand());
			}
			if(tempDataDto.getProductType() != null) {
				tempData.setProductType(convertToEntity(tempDataDto).getProductType());
			}
			if(tempDataDto.getProductName() != null) {
				tempData.setProductName(convertToEntity(tempDataDto).getProductName());
			}
			if(tempDataDto.getTagline() != null) {
				tempData.setTagline(convertToEntity(tempDataDto).getTagline());
			}
			if(tempDataDto.getImagePath() != null) {
				tempData.setImagePath(convertToEntity(tempDataDto).getImagePath());
			}
			if(tempDataDto.getLinkUrl() != null) {
				tempData.setLinkUrl(convertToEntity(tempDataDto).getLinkUrl());
			}
			if(tempDataDto.getDescription() != null) {
				tempData.setDescription(convertToEntity(tempDataDto).getDescription());
			}
			if(tempDataDto.getIsDeleted() != null) {
				tempData.setIsDeleted(convertToEntity(tempDataDto).getIsDeleted());
			}
			if(tempDataDto.getLastModifiedBy() != null) {
				tempData.setLastModifiedBy(convertToEntity(tempDataDto).getLastModifiedBy());
			}
			tempData.setLastModifiedOn(dateNow);
			Product updateData = productRepository.save(tempData);
			ProductDTO updateDataDTO = convertToDTO(updateData);
			
			List<ProductDTO> resultList = new ArrayList<ProductDTO>();
			resultList.add(updateDataDTO);
	
			message = successMessage;
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
			
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Delete a Data
    @DeleteMapping("/product/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Product tempData = productRepository.findById(id).orElse(null);
			productRepository.delete(tempData);
	
			message = successMessage;
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
    }
}
