package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.SubPrivacyPolicyDTO;
import com.telkom.dtpbe.models.SubPrivacyPolicy;
import com.telkom.dtpbe.repositories.SubPrivacyPolicyRepository;

@CrossOrigin(allowCredentials="true", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class SubPrivacyPolicyController{

	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	SubPrivacyPolicyRepository subPrivacyPolicyDTORepository;

	public SubPrivacyPolicyDTO convertToDTO(SubPrivacyPolicy object) {
	        return modelMapper.map(object, SubPrivacyPolicyDTO.class);
	}

	public SubPrivacyPolicy convertToEntity(SubPrivacyPolicyDTO object) {
	    return modelMapper.map(object, SubPrivacyPolicy.class);
	}
	
	//View All Data
	@GetMapping("/privacyPolicy/sub/readAll")
	public HashMap<String, Object> getAllData() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<SubPrivacyPolicyDTO> listData = new ArrayList<SubPrivacyPolicyDTO>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (SubPrivacyPolicy tempData : subPrivacyPolicyDTORepository.findAll()) {
				SubPrivacyPolicyDTO tempDataDTO = convertToDTO(tempData);
				listData.add(tempDataDTO);
			}
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listData.size());
		showHashMap.put("data", listData);

		return showHashMap;
	}
	
	//View Data By ID
	@GetMapping("/privacyPolicy/sub/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			SubPrivacyPolicy tempData = subPrivacyPolicyDTORepository.findById(id).orElse(null);
			SubPrivacyPolicyDTO tempDataDTO = convertToDTO(tempData);
			showHashMap.put("data", tempDataDTO);
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Create a new data
	@PostMapping("/privacyPolicy/sub/add")
	public HashMap<String, Object> createData(@Valid @RequestBody SubPrivacyPolicyDTO dataDto) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			SubPrivacyPolicy tempData = convertToEntity(dataDto);
			tempData.setIsDeleted(false);
			tempData.setCreatedOn(dateNow);
			tempData.setLastModifiedOn(dateNow);
			dataDto = convertToDTO(subPrivacyPolicyDTORepository.save(tempData));
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("data", tempData);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }
	
	// UPDATE 
	@PutMapping("/privacyPolicy/sub/update/{id}")
	public HashMap<String, Object> updateData(@PathVariable(value="id") Long id, @Valid @RequestBody SubPrivacyPolicyDTO tempDataDto){
		
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		
		try {
			SubPrivacyPolicy tempData = subPrivacyPolicyDTORepository.findById(id).orElseThrow(null);
			
			tempDataDto.setSubPrivacyPolicyId(tempData.getSubPrivacyPolicyId());
			
			if(tempDataDto.getPrivacyPolicyId() != null) {
				tempData.setPrivacyPolicyId(convertToEntity(tempDataDto).getPrivacyPolicyId());
			}
			
			if(tempDataDto.getTitle() != null) {
				tempData.setTitle(convertToEntity(tempDataDto).getTitle());
			}
			
			if(tempDataDto.getDescription() != null) {
				tempData.setDescription(convertToEntity(tempDataDto).getDescription());
			}
			
			if(tempDataDto.getIsDeleted() != null) {
				tempData.setIsDeleted(convertToEntity(tempDataDto).getIsDeleted());
			}
			
			if(tempDataDto.getLastModifiedBy() != null) {
				tempData.setLastModifiedBy(convertToEntity(tempDataDto).getLastModifiedBy());
			}
		
			tempData.setLastModifiedOn(dateNow);
			
			
			SubPrivacyPolicy updateData = subPrivacyPolicyDTORepository.save(tempData);
			
			List<SubPrivacyPolicyDTO> resultList = new ArrayList<SubPrivacyPolicyDTO>();
			resultList.add(convertToDTO(updateData));
	
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
			
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Delete a Data
    @DeleteMapping("/privacyPolicy/sub/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			SubPrivacyPolicy tempData = subPrivacyPolicyDTORepository.findById(id).orElse(null);
			subPrivacyPolicyDTORepository.delete(tempData);
	
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
    }
}
