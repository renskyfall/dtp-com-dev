package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.SubTermsAndConditionsDTO;
import com.telkom.dtpbe.models.SubTermsAndConditions;
import com.telkom.dtpbe.repositories.SubTermsAndConditionsRepository;

@CrossOrigin(allowCredentials="true", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class SubTermsAndConditionsController{

	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	SubTermsAndConditionsRepository subTermsAndConditionsRepository;

	public SubTermsAndConditionsDTO convertToDTO(SubTermsAndConditions object) {
	        return modelMapper.map(object, SubTermsAndConditionsDTO.class);
	}

	public SubTermsAndConditions convertToEntity(SubTermsAndConditionsDTO object) {
	    return modelMapper.map(object, SubTermsAndConditions.class);
	}
	
	//View All Data
	@GetMapping("/termsAndConditions/sub/readAll")
	public HashMap<String, Object> getAllData() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<SubTermsAndConditionsDTO> listData = new ArrayList<SubTermsAndConditionsDTO>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (SubTermsAndConditions tempData : subTermsAndConditionsRepository.findAll()) {
				SubTermsAndConditionsDTO tempDataDTO = convertToDTO(tempData);
				listData.add(tempDataDTO);
			}
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listData.size());
		showHashMap.put("data", listData);

		return showHashMap;
	}
	
	//View Data By ID
	@GetMapping("/termsAndConditions/sub/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			SubTermsAndConditions tempData = subTermsAndConditionsRepository.findById(id).orElse(null);
			SubTermsAndConditionsDTO tempDataDTO = convertToDTO(tempData);
			showHashMap.put("data", tempDataDTO);
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Create a new data
	@PostMapping("/termsAndConditions/sub/add")
	public HashMap<String, Object> createData(@Valid @RequestBody SubTermsAndConditionsDTO dataDto) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			SubTermsAndConditions tempData = convertToEntity(dataDto);
			tempData.setIsDeleted(false);
			tempData.setCreatedOn(dateNow);
			tempData.setLastModifiedOn(dateNow);
			dataDto = convertToDTO(subTermsAndConditionsRepository.save(tempData));
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("data", tempData);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }
	
	// UPDATE 
	@PutMapping("/termsAndConditions/sub/update/{id}")
	public HashMap<String, Object> updateData(@PathVariable(value="id") Long id, @Valid @RequestBody SubTermsAndConditionsDTO tempDataDto){
		
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		
		try {
			SubTermsAndConditions tempData = subTermsAndConditionsRepository.findById(id).orElseThrow(null);
			
			tempDataDto.setSubTermsAndConditionsId(tempData.getSubTermsAndConditionsId());
			
			if(tempDataDto.getTermsAndConditionsId() != null) {
				tempData.setTermsAndConditionsId(convertToEntity(tempDataDto).getTermsAndConditionsId());
			}
			
			if(tempDataDto.getTitle() != null) {
				tempData.setTitle(convertToEntity(tempDataDto).getTitle());
			}
			
			if(tempDataDto.getDescription() != null) {
				tempData.setDescription(convertToEntity(tempDataDto).getDescription());
			}
			
			if(tempDataDto.getIsDeleted() != null) {
				tempData.setIsDeleted(convertToEntity(tempDataDto).getIsDeleted());
			}
			
			if(tempDataDto.getLastModifiedBy() != null) {
				tempData.setLastModifiedBy(convertToEntity(tempDataDto).getLastModifiedBy());
			}
		
			tempData.setLastModifiedOn(dateNow);
			
			
			SubTermsAndConditions updateData = subTermsAndConditionsRepository.save(tempData);
			
			List<SubTermsAndConditionsDTO> resultList = new ArrayList<SubTermsAndConditionsDTO>();
			resultList.add(convertToDTO(updateData));
	
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
			
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Delete a Data
    @DeleteMapping("/termsAndConditions/sub/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			SubTermsAndConditions tempData = subTermsAndConditionsRepository.findById(id).orElse(null);
			subTermsAndConditionsRepository.delete(tempData);
	
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
    }
}
