package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.UsersDTO;
import com.telkom.dtpbe.models.Users;
import com.telkom.dtpbe.repositories.UsersRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class UsersController{

	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	UsersRepository usersRepository;

	public UsersDTO convertToDTO(Users object) {
	        return modelMapper.map(object, UsersDTO.class);
	}

	public Users convertToEntity(UsersDTO object) {
	    return modelMapper.map(object, Users.class);
	}
	
	//View All Data
	@GetMapping("/users/readAll")
	public HashMap<String, Object> getAllData() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<UsersDTO> listData = new ArrayList<UsersDTO>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (Users tempData : usersRepository.findAll()) {
				UsersDTO tempDataDTO = convertToDTO(tempData);
				listData.add(tempDataDTO);
			}
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		showHashMap.put("total", listData.size());
		showHashMap.put("data", listData);

		return showHashMap;
	}
	
	//View Data By ID
	@GetMapping("/users/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Users tempData = usersRepository.findById(id).orElse(null);
			UsersDTO tempDataDTO = convertToDTO(tempData);
			showHashMap.put("data", tempDataDTO);
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Create a new data
	@PostMapping("/users/add")
	public HashMap<String, Object> createData(@Valid @RequestBody UsersDTO dataDto) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Users tempData = convertToEntity(dataDto);
			tempData.setIsDeleted(false);
			tempData.setCreatedOn(dateNow);
			tempData.setLastModifiedOn(dateNow);
			usersRepository.save(tempData);
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("data", tempData);
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }
	
	// UPDATE 
	@PutMapping("/users/update/{id}")
	public HashMap<String, Object> updateData(@PathVariable(value="id") Long id, @Valid @RequestBody UsersDTO dataDto){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		
		try {
			Users tempData = usersRepository.findById(id).orElseThrow(null);
			
			dataDto.setUserId(tempData.getUserId());
			
			if(dataDto.getCompany() != null) {
				tempData.setCompany(convertToEntity(dataDto).getCompany());
			}
			
			if(dataDto.getRole() != null) {
				tempData.setRole(convertToEntity(dataDto).getRole());
			}
			
			if(dataDto.getFullName() != null) {
				tempData.setFullName(convertToEntity(dataDto).getFullName());
			}
			
			if(dataDto.getFullName() != null) {
				tempData.setFullName(convertToEntity(dataDto).getFullName());
			}
			
			if(dataDto.getEmail() != null) {
				tempData.setEmail(convertToEntity(dataDto).getEmail());
			}
			
			if(dataDto.getPassword() != null) {
				tempData.setPassword(convertToEntity(dataDto).getPassword());
			}
			
			if(dataDto.getMobileNumber() != null) {
				tempData.setMobileNumber(convertToEntity(dataDto).getMobileNumber());
			}
			
			if(dataDto.getImagePath() != null) {
				tempData.setImagePath(convertToEntity(dataDto).getImagePath());
			}
			
			if(dataDto.getTokens() != null) {
				tempData.setTokens(convertToEntity(dataDto).getTokens());
			}
			
			if(dataDto.getStatus() != null) {
				tempData.setStatus(convertToEntity(dataDto).getStatus());
			}
			
			if(dataDto.getIsDeleted() != null) {
				tempData.setIsDeleted(convertToEntity(dataDto).getIsDeleted());
			}
			
			if(dataDto.getLastModifiedBy() != null) {
				tempData.setLastModifiedBy(convertToEntity(dataDto).getLastModifiedBy());
			}
			
			tempData.setLastModifiedOn(dateNow);
			
			
			Users updateData = usersRepository.save(tempData);
			
			List<Users> resultList = new ArrayList<Users>();
			resultList.add(updateData);
	
			message = "success";
			code = 200;
			isSuccess = true;
			showHashMap.put("total", resultList.size());
			showHashMap.put("data", resultList);
			
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("total", 0);
			showHashMap.put("data", "");
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
	}
	
	// Delete a Data
    @DeleteMapping("/users/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Users tempData = usersRepository.findById(id).orElse(null);
			usersRepository.delete(tempData);
	
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
		return showHashMap;
    }
}
