package com.telkom.dtpbe.core.email;

import java.io.File;

public class Attachment {

    private String name;
    private File file;
    private String contentType;

    public Attachment() {

    }

    public Attachment(String name, File file, String contentType) {
        this.name = name;
        this.file = file;
        this.contentType = contentType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
