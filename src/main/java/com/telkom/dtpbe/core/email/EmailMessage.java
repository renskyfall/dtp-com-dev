package com.telkom.dtpbe.core.email;

import java.util.List;

public class EmailMessage {

    private String subject;
    private String from;
    private List<String> to;
    private List<String> cc;
    private List<String> bcc;
    private String message;
    private List<Attachment> attachments;

    private EmailMessage(Builder builder) {
        this.subject = builder.subject;
        this.from = builder.from;
        this.to = builder.to;
        if (builder.cc != null) {
            this.cc = builder.cc;
        }

        if (builder.bcc != null) {
            this.bcc = builder.bcc;
        }
        this.message = builder.message;
        if (builder.attachments != null) {
            this.attachments = builder.attachments;
        }
    }

    public String getSubject() {
        return subject;
    }

    public String getFrom() {
        return from;
    }

    public List<String> getTo() {
        return to;
    }

    public List<String> getCc() {
        return cc;
    }

    public List<String> getBcc() {
        return bcc;
    }

    public String getMessage() {
        return message;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public static class Builder {
        private String subject;
        private String from;
        private List<String> to;
        private List<String> cc;
        private List<String> bcc;
        private String message;
        private List<Attachment> attachments;

        public Builder() {

        }

        public Builder setSubject(String subject) {
            this.subject = subject;
            return this;
        }

        public Builder setFrom(String from) {
            this.from = from;
            return this;
        }

        public Builder setTo(List<String> to) {
            this.to = to;
            return this;
        }

        public Builder setCc(List<String> cc) {
            this.cc = cc;
            return this;
        }

        public Builder setBcc(List<String> bcc) {
            this.bcc = bcc;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setAttachments(List<Attachment> attachments) {
            this.attachments = attachments;
            return this;
        }

        public EmailMessage build() {
            return new EmailMessage(this);
        }
    }
}
