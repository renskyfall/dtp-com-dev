package com.telkom.dtpbe.core.email;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;

import com.telkom.dtpbe.core.shared.Result;

public class EmailService implements IEmailService {

    private final static Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    private JavaMailSender mailSender;

    public EmailService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    /**
     * @param message
     * @return usage:
     * List<String> to = new ArrayList<>();
     * to.add(userPayload.getEmail());
     * EmailMessage emailMessage = new EmailMessage.Builder()
     * .setSubject("Umeetme Account Confirmation")
     * .setFrom("admin@umeetme.id")
     * .setTo(to)
     * .setMessage("Thank you for joinning with Umeetme")
     * .build();
     * <p>
     * Future<Result<String, String>> resultFutureEmail = emailService.send(emailMessage);
     * if (resultFutureEmail.get().getError() != null) {
     * LOGGER.error(resultFutureEmail.get().getError());
     * } else {
     * LOGGER.info(resultFutureEmail.get().getData());
     * }
     */
    @Async
    @Override
    public Future<Result<String, String>> send(EmailMessage message) {

        LOGGER.info("sending email");

        MimeMessage mimeMessage = mailSender.createMimeMessage();

        try {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            if (message.getSubject() != null) {
                messageHelper.setSubject(message.getSubject());
            }

            messageHelper.setFrom(message.getFrom());
            String to = String.join(",", message.getTo());
            messageHelper.setTo(InternetAddress.parse(to));
            if (message.getCc() != null) {
                String cc = String.join(",", message.getCc());
                messageHelper.setCc(InternetAddress.parse(cc));
            }

            if (message.getBcc() != null) {
                String bcc = String.join(",", message.getBcc());
                messageHelper.setBcc(InternetAddress.parse(bcc));
            }

            messageHelper.setText(message.getMessage());
            if (message.getAttachments() != null) {
                message.getAttachments().forEach(attachment -> {
                    try {
                        messageHelper.addAttachment(attachment.getName(), attachment.getFile());
                    } catch (MessagingException e) {
                        LOGGER.error("set attachment error {}", e.getMessage());
                    }
                });
            }

            mailSender.send(mimeMessage);

            LOGGER.info("email sent");

            return CompletableFuture.completedFuture(Result.from("success send email", null));
        } catch (MessagingException | MailException e) {
            LOGGER.error("send email error {}", e.getMessage());
            return CompletableFuture.completedFuture(Result.from(null, e.getMessage()));
        }

    }
}
