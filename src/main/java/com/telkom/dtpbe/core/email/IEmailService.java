package com.telkom.dtpbe.core.email;

import java.util.concurrent.Future;

import com.telkom.dtpbe.core.shared.Result;

public interface IEmailService {

    Future<Result<String, String>> send(EmailMessage message);
}
