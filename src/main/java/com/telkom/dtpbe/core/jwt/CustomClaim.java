package com.telkom.dtpbe.core.jwt;

import io.jsonwebtoken.Claims;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;

/**
 * @author wurianto
 */
public class CustomClaim {

    private String subject;
    private String issuer;
    private String audience;
    private Date expiration;
    private Date issuedAt;
    private Date notBefore;
    private String id;
    private String name;
    private String avatar;
    private String role;
    private String contextEmail;

    public CustomClaim() {

    }

    public CustomClaim(String subject, String issuer, String audience) {
        this.subject = subject;
        this.issuer = issuer;
        this.audience = audience;
    }

    public CustomClaim(String id, String subject, String issuer, String audience, Integer expirationInMinutes) {
        this.id = id;
        this.subject = subject;
        this.issuer = issuer;
        this.audience = audience;
        this.contextEmail = subject;

        this.expiration = Date.from(Instant.now().plus(expirationInMinutes.longValue(), ChronoUnit.MINUTES));
        this.issuedAt = new Date();
    }

    public CustomClaim(String subject, String issuer, String audience, Integer expirationInMinutes) {
        this.subject = subject;
        this.issuer = issuer;
        this.audience = audience;
        this.contextEmail = subject;

        this.expiration = Date.from(Instant.now().plus(expirationInMinutes.longValue(), ChronoUnit.MINUTES));
        this.issuedAt = new Date();
    }

    // construct from JWT default claim
    public CustomClaim(Claims claims) {
        this.id = claims.getId();
        this.subject = claims.getSubject();
        this.issuedAt = claims.getIssuedAt();
        this.expiration = claims.getExpiration();
        this.notBefore = claims.getNotBefore();
        this.issuer = claims.getIssuer();
        this.audience = claims.getAudience();;
        this.role = (String) claims.get("role");

        if (claims.get("context") instanceof HashMap) {
            HashMap<String, Object> context = (HashMap<String, Object>) claims.get("context");
            if (context.get("userPayload") instanceof HashMap) {
                HashMap<String, Object> user = (HashMap<String, Object>) context.get("userPayload");
                this.name = (String) user.get("name");
                this.avatar = (String) user.get("avatar");
            }
        }
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getAudience() {
        return audience;
    }

    public void setAudience(String audience) {
        this.audience = audience;
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }

    public Date getIssuedAt() {
        return issuedAt;
    }

    public void setIssuedAt(Date issuedAt) {
        this.issuedAt = issuedAt;
    }

    public Date getNotBefore() {
        return notBefore;
    }

    public void setNotBefore(Date notBefore) {
        this.notBefore = notBefore;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "CustomClaim{" +
                "subject='" + subject + '\'' +
                ", issuer='" + issuer + '\'' +
                ", audience='" + audience + '\'' +
                ", expiration=" + expiration +
                ", issuedAt=" + issuedAt +
                ", notBefore=" + notBefore +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", avatar='" + avatar + '\'' +
                ", role='" + role + '\'' +
                '}';
    }

    public String getContextEmail() {
        return contextEmail;
    }

    public void setContextEmail(String contextEmail) {
        this.contextEmail = contextEmail;
    }
}
