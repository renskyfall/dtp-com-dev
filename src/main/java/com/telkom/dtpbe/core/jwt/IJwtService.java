package com.telkom.dtpbe.core.jwt;

import java.util.concurrent.Future;

import com.telkom.dtpbe.core.shared.Result;

/**
 *
 * @author wurianto
 */
public interface IJwtService {

    Future<Result<String, String>> generate(CustomClaim claim);

    Future<Result<CustomClaim, String>> validate(String jwt);

    Future<Result<CustomClaim, String>> decode(String jwt);
}