package com.telkom.dtpbe.core.jwt;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import com.telkom.dtpbe.core.shared.Result;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

public class JwtService implements IJwtService {

    private final static Logger LOGGER = LoggerFactory.getLogger(JwtService.class);

    private final Key privateKey;
    private final Key publicKey;

    public JwtService(Key privateKey, Key publicKey) {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
    }

    @Async
    @Override
    public Future<Result<String, String>> generate(CustomClaim claim) {
        LOGGER.info("generate access token");

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> userClaims = new HashMap<>();
        Map<String, Object> user = new HashMap<>();

        userClaims.put("id", claim.getId());
        userClaims.put("name", claim.getName());
        userClaims.put("avatar", claim.getAvatar());
        userClaims.put("email", claim.getContextEmail());

        user.put("userPayload", userClaims);

        context.put("context", user);
        context.put("role", claim.getRole());

        String jwt = Jwts.builder().setSubject(claim.getSubject())
                .setHeaderParam(Header.TYPE, Header.JWT_TYPE)
                .setHeaderParam("kid", "cert")
                .addClaims(context)
                .setId(claim.getId())
                .setIssuer(claim.getIssuer())
                .setAudience(claim.getAudience())
                .setExpiration(claim.getExpiration())
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.RS256, this.privateKey)
                .compact();
        return CompletableFuture.completedFuture(Result.from(jwt, null));
    }

    @Async
    @Override
    public Future<Result<CustomClaim, String>> validate(String jwt) {
        String[] jwtSplit = jwt.split(" ");
        if (jwtSplit.length < 2 || jwtSplit.length > 2) {
            return CompletableFuture.completedFuture(Result.from(null, "invalid token format"));
        }
        String token = jwtSplit[1];

        try {
            Claims c = Jwts.parser().setSigningKey(this.publicKey).parseClaimsJws(token).getBody();
            CustomClaim cc = new CustomClaim(c);
            return CompletableFuture.completedFuture(Result.from(cc, null));

        } catch (ExpiredJwtException | MalformedJwtException | SignatureException | UnsupportedJwtException | IllegalArgumentException e) {
            LOGGER.error("jwt exception {}", e.getLocalizedMessage());
            if (e instanceof ExpiredJwtException) {
                return CompletableFuture.completedFuture(Result.from(null, "token expired"));
            } else {
                return CompletableFuture.completedFuture(Result.from(null, "token in invalid format or malformed"));
            }
        }
    }

    @Async
    @Override
    public Future<Result<CustomClaim, String>> decode(String jwt) {
        CustomClaim cc = null;
        String token = jwt;

        try {
            Claims c = Jwts.parser().setSigningKey(this.publicKey).parseClaimsJws(token).getBody();
            cc = new CustomClaim(c);
            return CompletableFuture.completedFuture(Result.from(cc, null));

        } catch (ExpiredJwtException | MalformedJwtException | SignatureException | UnsupportedJwtException | IllegalArgumentException e) {
            if (e instanceof ExpiredJwtException) {
                LOGGER.error("jwt expired");
                Claims c = ((ExpiredJwtException) e).getClaims();
                cc = new CustomClaim(c);
                return CompletableFuture.completedFuture(Result.from(cc, null));
            } else {
                LOGGER.error("decode jwt error", e);
                return CompletableFuture.completedFuture(Result.from(null, "token in invalid format or malformed"));
            }
        }
    }
}
