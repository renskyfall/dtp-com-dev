package com.telkom.dtpbe.core.jwt;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * @author wurianto
 */
public class KeyReader {

    // openssl rsa -inform PEM -in app.rsa -outform DER -pubout -out rsapub.der
    public static PrivateKey getPrivateKey(InputStream inputStream) throws Exception {

        //byte[] keyBytes = Files.readAllBytes(Paths.get(filename));
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[1024];
        while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }

        buffer.flush();
        byte[] keyBytes = buffer.toByteArray();

        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }

    //openssl pkcs8 -topk8 -inform PEM -in app.rsa -outform DER -nocrypt -out rsapriv.der
    public static PublicKey getPublicKey(InputStream inputStream) throws Exception {

        //byte[] keyBytes = Files.readAllBytes(Paths.get(filename));
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[1024];
        while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }

        buffer.flush();
        byte[] keyBytes = buffer.toByteArray();

        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(spec);
    }

}
