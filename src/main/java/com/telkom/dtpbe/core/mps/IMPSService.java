package com.telkom.dtpbe.core.mps;

import java.util.List;
import java.util.concurrent.Future;

import com.telkom.dtpbe.core.shared.Result;

public interface IMPSService {

    Future<Result<List<PayloadProduct>, String>> requestProduct();
    Future<Result<String, String>> generateInvoice(PayloadRequestInvoice payloadRequestInvoice);
    Future<Result<PayloadResponseVA, String>> requestVA(PayloadRequestVA payloadRequestVA);
}
