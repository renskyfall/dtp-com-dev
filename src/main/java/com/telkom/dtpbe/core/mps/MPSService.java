package com.telkom.dtpbe.core.mps;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telkom.dtpbe.core.shared.Result;
import com.telkom.dtpbe.hunk.Request;

public class MPSService implements IMPSService {
    private final static Logger LOGGER = LoggerFactory.getLogger(MPSService.class);

    private String host;
    private String merchantId;
    private String merchantKey;

    public MPSService(String host, String merchantId, String merchantKey) {
        this.host = host;
        this.merchantId = merchantId;
        this.merchantKey = merchantKey;
    }

    @Async
    @Override
    public Future<Result<List<PayloadProduct>, String>> requestProduct() {
        try {
            LOGGER.info("Sending request list payment method...");

            String hostProduct = String.format("%s/merchant/requestProduct?merchantId=%s", this.host, this.merchantId);

            Map<String, String> headers = new HashMap<>();
            headers.put(Request.CONTENT_TYPE, "application/json");
            Future<HttpResponse<byte[]>> futureResult = Request.doAsync(Request.HttpMethod.GET,
                    hostProduct, headers, null, 10);

            LOGGER.info("response: -- {} --", new String(futureResult.get().body()));

            ObjectMapper mapper = new ObjectMapper();
            List<PayloadProduct> payloadProducts = mapper.readValue(futureResult.get().body(), new TypeReference<ArrayList<PayloadProduct>>() {});

            return CompletableFuture.completedFuture(Result.from(payloadProducts, null));

        } catch (Exception e) {
            e.printStackTrace();
            return CompletableFuture.completedFuture(Result.from(null, e.getMessage()));
        }
    }

    @Override
    public Future<Result<String, String>> generateInvoice(PayloadRequestInvoice payloadRequestInvoice) {

        try {
            LOGGER.info("Sending request generate invoice...");
            payloadRequestInvoice.setMerchantId(this.merchantId);
            payloadRequestInvoice.setMerchantKey(this.merchantKey);

            String hostReq = String.format("%s/merchant/requestInvoice", this.host);

            Map<String, String> headers = new HashMap<>();
            headers.put(Request.CONTENT_TYPE, "application/json");
            Future<HttpResponse<byte[]>> futureResult = Request.doAsync(Request.HttpMethod.POST,
                    hostReq, headers, Request.ofJsonObject(payloadRequestInvoice), 10);

            LOGGER.info("response: -- {} --", new String(futureResult.get().body()));

            ObjectMapper mapper = new ObjectMapper();
            Map<String, String> payloadProducts = mapper.readValue(futureResult.get().body(), new TypeReference<Map<String, String>>() {});

            return CompletableFuture.completedFuture(Result.from(payloadProducts.get("invoiceId"), null));

        } catch (Exception e) {
            e.printStackTrace();
            return CompletableFuture.completedFuture(Result.from(null, e.getMessage()));
        }
    }

    @Override
    public Future<Result<PayloadResponseVA, String>> requestVA(PayloadRequestVA payloadRequestVA) {
        try {
            LOGGER.info("Sending request generate VA...");
            payloadRequestVA.setMerchantId(this.merchantId);
            payloadRequestVA.setMerchantKey(this.merchantKey);

            String hostReq = String.format("%s/merchant/requestVa", this.host);

            Map<String, String> headers = new HashMap<>();
            headers.put(Request.CONTENT_TYPE, "application/json");
            Future<HttpResponse<byte[]>> futureResult = Request.doAsync(Request.HttpMethod.POST,
                    hostReq, headers, Request.ofJsonObject(payloadRequestVA), 60);

            LOGGER.info("response: -- {} --", new String(futureResult.get().body()));

            ObjectMapper mapper = new ObjectMapper();
            PayloadResponseVA payloadResponseVA = mapper.readValue(futureResult.get().body(), PayloadResponseVA.class);
            return CompletableFuture.completedFuture(Result.from(payloadResponseVA, null));

        } catch (Exception e) {
            e.printStackTrace();
            return CompletableFuture.completedFuture(Result.from(null, e.getMessage()));
        }
    }
}
