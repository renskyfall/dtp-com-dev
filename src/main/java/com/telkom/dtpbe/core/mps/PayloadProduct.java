package com.telkom.dtpbe.core.mps;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PayloadProduct {

    private String gatewayRegistery;

    private String gatewayCode;

    private List<Product> productLists;

    public PayloadProduct() {
    }

    public PayloadProduct(String gatewayRegistery, String gatewayCode, List<Product> productLists) {
        this.gatewayRegistery = gatewayRegistery;
        this.gatewayCode = gatewayCode;
        this.productLists = productLists;
    }

    public String getGatewayRegistery() {
        return gatewayRegistery;
    }

    public void setGatewayRegistery(String gatewayRegistery) {
        this.gatewayRegistery = gatewayRegistery;
    }

    public String getGatewayCode() {
        return gatewayCode;
    }

    public void setGatewayCode(String gatewayCode) {
        this.gatewayCode = gatewayCode;
    }

    public List<Product> getProductLists() {
        return productLists;
    }

    public void setProductLists(List<Product> productLists) {
        this.productLists = productLists;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Product {
        private String bankCode;
        private String productCode;
        private String productName;
        private String productImage;
        private String productImageUrl;
        private String productType;
        private PayloadProductGroup productGroup;

        public Product(){}

        public Product(String bankCode, String productCode, String productName, String productImage, String productImageUrl, String productType, PayloadProductGroup productGroup) {
            this.bankCode = bankCode;
            this.productCode = productCode;
            this.productName = productName;
            this.productImage = productImage;
            this.productImageUrl = productImageUrl;
            this.productType = productType;
            this.productGroup = productGroup;
        }

        public String getBankCode() {
            return bankCode;
        }

        public void setBankCode(String bankCode) {
            this.bankCode = bankCode;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductImage() {
            return productImage;
        }

        public void setProductImage(String productImage) {
            this.productImage = productImage;
        }

        public String getProductImageUrl() {
            return productImageUrl;
        }

        public void setProductImageUrl(String productImageUrl) {
            this.productImageUrl = productImageUrl;
        }

        public String getProductType() {
            return productType;
        }

        public void setProductType(String productType) {
            this.productType = productType;
        }

        public PayloadProductGroup getProductGroup() {
            return productGroup;
        }

        public void setProductGroup(PayloadProductGroup productGroup) {
            this.productGroup = productGroup;
        }
    }
}