package com.telkom.dtpbe.core.mps;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PayloadProductGroup {
    private String id;
    private String name;
    private String description;
    private String photo;
    private String sequence;
    @JsonIgnore
    private String paymentImageUrl;

    public PayloadProductGroup(){}

    public PayloadProductGroup(String id, String name, String description, String photo, String sequence, String paymentImageUrl) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.photo = photo;
        this.sequence = sequence;
        this.paymentImageUrl = paymentImageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getPaymentImageUrl() {
        return paymentImageUrl;
    }

    public void setPaymentImageUrl(String paymentImageUrl) {
        this.paymentImageUrl = paymentImageUrl;
    }
}
