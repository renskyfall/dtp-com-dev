package com.telkom.dtpbe.core.mps;

public class PayloadRequestInvoice {
    private String merchantId;
    private String merchantKey;
    private String orderId;
    private String amount;
    private OrderInformation orderInformation;
    private AdditionalInformation additionalInformation;

    public PayloadRequestInvoice(){}

    public PayloadRequestInvoice(String merchantId, String merchantKey, String orderId, String amount, OrderInformation orderInformation, AdditionalInformation additionalInformation) {
        this.merchantId = merchantId;
        this.merchantKey = merchantKey;
        this.orderId = orderId;
        this.amount = amount;
        this.orderInformation = orderInformation;
        this.additionalInformation = additionalInformation;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantKey() {
        return merchantKey;
    }

    public void setMerchantKey(String merchantKey) {
        this.merchantKey = merchantKey;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public OrderInformation getOrderInformation() {
        return orderInformation;
    }

    public void setOrderInformation(OrderInformation orderInformation) {
        this.orderInformation = orderInformation;
    }

    public AdditionalInformation getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(AdditionalInformation additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public static class OrderInformation {
        private String productName;
        private String quantity;
        private String amount;
        private String nama;
        private String nomorHp;
        private String email;

        public OrderInformation(){}

        public OrderInformation(String productName, String quantity, String amount, String nama, String nomorHp, String email) {
            this.productName = productName;
            this.quantity = quantity;
            this.amount = amount;
            this.nama = nama;
            this.nomorHp = nomorHp;
            this.email = email;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getNomorHp() {
            return nomorHp;
        }

        public void setNomorHp(String nomorHp) {
            this.nomorHp = nomorHp;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

    public static class AdditionalInformation {
        private String emailNotif;

        public AdditionalInformation(){}

        public AdditionalInformation(String emailNotif) {
            this.emailNotif = emailNotif;
        }

        public String getEmailNotif() {
            return emailNotif;
        }

        public void setEmailNotif(String emailNotif) {
            this.emailNotif = emailNotif;
        }
    }
}
