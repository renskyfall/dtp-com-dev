package com.telkom.dtpbe.core.mps;

public class PayloadRequestVA {
    private String merchantId;
    private String merchantKey;
    private String invoiceId;
    private String productCode;
    private String gateway;

    public PayloadRequestVA(){}

    public PayloadRequestVA(String merchantId, String merchantKey, String invoiceId, String productCode, String gateway) {
        this.merchantId = merchantId;
        this.merchantKey = merchantKey;
        this.invoiceId = invoiceId;
        this.productCode = productCode;
        this.gateway = gateway;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantKey() {
        return merchantKey;
    }

    public void setMerchantKey(String merchantKey) {
        this.merchantKey = merchantKey;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }
}
