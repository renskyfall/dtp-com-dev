package com.telkom.dtpbe.core.mps;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PayloadResponseVA {
    private String resultCode;
    private String resultDescription;
    private String vaNumber;
    private String expired;
    private String description;
    private Double totalAmount;
    private Double amount;
    private Double fee;
}
