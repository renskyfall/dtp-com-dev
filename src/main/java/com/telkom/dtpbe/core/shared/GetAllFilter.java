package com.telkom.dtpbe.core.shared;

public class GetAllFilter {
    private Integer page;

    private Integer limit;

    private Integer offset;

    private String orderBy;

    private String sort;

    private String search;

    public GetAllFilter() {
    }

    public GetAllFilter(Integer page, Integer limit, String orderBy, String sort, String search) {
        if (page <= 0) page = 1;
        if (limit <= 0) limit = 10;
        this.page = page;
        this.limit = limit;
        this.offset = (page - 1) * limit;
        this.orderBy = orderBy;
        this.sort = sort;
        this.search = search;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }
}
