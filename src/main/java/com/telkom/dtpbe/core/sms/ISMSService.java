package com.telkom.dtpbe.core.sms;

import java.util.concurrent.Future;

import com.telkom.dtpbe.core.shared.Result;

public interface ISMSService {

    Future<Result<String, String>> send(SMSPayload message);

}
