package com.telkom.dtpbe.core.sms;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SMSPayload {

    @JsonProperty("sender_id")
    private String senderId;

    @JsonProperty("receipent_number")
    private String receipentNumber;

    @JsonProperty("message_body")
    private String messageBody;

    public SMSPayload() {

    }

    public SMSPayload(String senderId, String receipentNumber, String messageBody) {
        this.senderId = senderId;
        this.receipentNumber = receipentNumber.replaceFirst("^0", "62");
        this.messageBody = messageBody;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceipentNumber() {
        return receipentNumber;
    }

    public void setReceipentNumber(String receipentNumber) {
        this.receipentNumber = receipentNumber;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }
}
