package com.telkom.dtpbe.core.sms;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SMSResult {

    private Boolean success;

    @JsonProperty("msg_id")
    private String messageId;

    public SMSResult() {

    }

    public SMSResult(Boolean success, String messageId) {
        this.success = success;
        this.messageId = messageId;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }
}
