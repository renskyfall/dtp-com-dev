package com.telkom.dtpbe.core.sms;

import java.net.URISyntaxException;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.telkom.dtpbe.core.shared.Result;
import com.telkom.dtpbe.hunk.HunkMethodNotSupportedException;
import com.telkom.dtpbe.hunk.Request;

public class SMSService implements ISMSService {
    private final static Logger LOGGER = LoggerFactory.getLogger(SMSService.class);

    private String host;

    private String basicAuth;

    public SMSService() {
    }

    public SMSService(String host, String basicAuth) {
        this.host = host;
        this.basicAuth = basicAuth;
    }

    @Async
    @Override
    public Future<Result<String, String>> send(SMSPayload message) {
        try {
            LOGGER.info("Sending request...");

            Map<String, String> headers = new HashMap<>();
            headers.put(Request.AUTHORIZATION, "Basic " + this.basicAuth);
            headers.put(Request.CONTENT_TYPE, "application/json");
            Future<HttpResponse<byte[]>> futureSms = Request.doAsync(Request.HttpMethod.POST,
                    this.host, headers, Request.ofJsonObject(message), 2);

            LOGGER.info("response: -- {} --", new String(futureSms.get().body()));

            return CompletableFuture.completedFuture(Result.from("sms has been to " + message.getReceipentNumber(), null));

        } catch (InterruptedException | ExecutionException e) {
            LOGGER.info("ERROR {}", e.toString());
            return CompletableFuture.completedFuture(Result.from(null, e.getMessage()));
        } catch (HunkMethodNotSupportedException e) {
            LOGGER.info("ERROR {}", e.toString());
            return CompletableFuture.completedFuture(Result.from(null, e.getMessage()));
        } catch (URISyntaxException e) {
            LOGGER.info("ERROR {}", e.toString());
            return CompletableFuture.completedFuture(Result.from(null, e.getMessage()));
        }
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getBasicAuth() {
        return basicAuth;
    }

    public void setBasicAuth(String basicAuth) {
        this.basicAuth = basicAuth;
    }
}
