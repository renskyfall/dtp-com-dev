package com.telkom.dtpbe.core.umeetme;

import java.util.concurrent.Future;

import com.telkom.dtpbe.core.shared.Result;

public interface IOrderUmeetme {
	 Future<Result<OrderUmeetmeResponse, String>> order(OrderUmeetmePayload payload);
	 Future<Result<UserVerificationResponse, String>> userVerification(UserVerificationPayload payload);
	 Future<Result<ResponsePaymentNotification, String>> paymentNotification(PayloadPaymentNotification payload);
	 Future<Result<ResponseLogin, String>> login(PayloadLogin payload);
}
