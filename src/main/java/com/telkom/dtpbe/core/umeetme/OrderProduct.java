package com.telkom.dtpbe.core.umeetme;

import java.sql.Timestamp;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrderProduct {
	private String invoiceNumber;
	private String status;
	private String paymentMethod;
	private String vaNumber;
	private Double total;
	private Double discount;
	private Double adminFee;
	private Double grandTotal;
	private Timestamp orderDate;
	private Timestamp expiredDate;
	private Timestamp activeDate;
	private Timestamp dueDate;
	private List<ProductPayload> products;
}
