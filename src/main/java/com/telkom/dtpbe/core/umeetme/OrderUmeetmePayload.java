package com.telkom.dtpbe.core.umeetme;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrderUmeetmePayload {
	String accessToken;
	OrderProduct order;
	UserPayload user;
}
