package com.telkom.dtpbe.core.umeetme;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrderUmeetmeResponse {

	private String code;
	private String success;
	private String message;
	private Data data;
	
	public static class Data {
		private String orderId;
		private String invoiceNumber;
		private String userId;
		private String status;
		private String paymentMethod;
		private String paymentMethodName;
		private String vaNumber;
		private Double total;
		private Double discount;
		private Double adminFee;
		private Double grandTotal;
		private String orderDate;
		private String expiredDate;
		private String activeDate;
		private String dueDate;
		private String cancelReason;
		private List<Product> products;
		private String verifyUserToken;
		private String mobileNumber;
		
		public String getVerifyUserToken() {
			return verifyUserToken;
		}
		public void setVerifyUserToken(String verifyUserToken) {
			this.verifyUserToken = verifyUserToken;
		}
		public String getMobileNumber() {
			return mobileNumber;
		}
		public void setMobileNumber(String mobileNumber) {
			this.mobileNumber = mobileNumber;
		}
		public String getOrderId() {
			return orderId;
		}
		public void setOrderId(String orderId) {
			this.orderId = orderId;
		}
		public String getInvoiceNumber() {
			return invoiceNumber;
		}
		public void setInvoiceNumber(String invoiceNumber) {
			this.invoiceNumber = invoiceNumber;
		}
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getPaymentMethod() {
			return paymentMethod;
		}
		public void setPaymentMethod(String paymentMethod) {
			this.paymentMethod = paymentMethod;
		}
		public String getPaymentMethodName() {
			return paymentMethodName;
		}
		public void setPaymentMethodName(String paymentMethodName) {
			this.paymentMethodName = paymentMethodName;
		}
		public String getVaNumber() {
			return vaNumber;
		}
		public void setVaNumber(String vaNumber) {
			this.vaNumber = vaNumber;
		}
		public Double getTotal() {
			return total;
		}
		public void setTotal(Double total) {
			this.total = total;
		}
		public Double getDiscount() {
			return discount;
		}
		public void setDiscount(Double discount) {
			this.discount = discount;
		}
		public Double getAdminFee() {
			return adminFee;
		}
		public void setAdminFee(Double adminFee) {
			this.adminFee = adminFee;
		}
		public Double getGrandTotal() {
			return grandTotal;
		}
		public void setGrandTotal(Double grandTotal) {
			this.grandTotal = grandTotal;
		}
		public String getOrderDate() {
			return orderDate;
		}
		public void setOrderDate(String orderDate) {
			this.orderDate = orderDate;
		}
		public String getExpiredDate() {
			return expiredDate;
		}
		public void setExpiredDate(String expiredDate) {
			this.expiredDate = expiredDate;
		}
		public String getActiveDate() {
			return activeDate;
		}
		public void setActiveDate(String activeDate) {
			this.activeDate = activeDate;
		}
		public String getDueDate() {
			return dueDate;
		}
		public void setDueDate(String dueDate) {
			this.dueDate = dueDate;
		}
		public String getCancelReason() {
			return cancelReason;
		}
		public void setCancelReason(String cancelReason) {
			this.cancelReason = cancelReason;
		}
		
		public List<Product> getProducts() {
			return products;
		}
		public void setProducts(List<Product> products) {
			this.products = products;
		}
	}
	public static class Product {
		
		private String code;
		private String type;
		private String name;
		private Integer qty;
		private Double price;
		private Double totalPrice;
		private String endDate;
		
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Integer getQty() {
			return qty;
		}
		public void setQty(Integer qty) {
			this.qty = qty;
		}
		public Double getPrice() {
			return price;
		}
		public void setPrice(Double price) {
			this.price = price;
		}
		public Double getTotalPrice() {
			return totalPrice;
		}
		public void setTotalPrice(Double totalPrice) {
			this.totalPrice = totalPrice;
		}
		public String getEndDate() {
			return endDate;
		}
		public void setEndDate(String endDate) {
			this.endDate = endDate;
		}
	}
}