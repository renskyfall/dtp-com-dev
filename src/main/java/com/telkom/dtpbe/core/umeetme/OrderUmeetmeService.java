package com.telkom.dtpbe.core.umeetme;

import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telkom.dtpbe.core.shared.Result;
import com.telkom.dtpbe.hunk.Request;

public class OrderUmeetmeService implements IOrderUmeetme{
	private final static Logger LOGGER = LoggerFactory.getLogger(OrderUmeetmeService.class);
    
	@Async
    @Override
	public Future<Result<ResponseLogin, String>> login(PayloadLogin payload) {
		try {
            LOGGER.info("Sending request Login...");

            String hostReq = String.format("https://api-dev.umeetme.id/setkab/auth/login");

            Map<String, String> headers = new HashMap<>();
            headers.put(Request.CONTENT_TYPE, "application/json");
            Future<HttpResponse<byte[]>> futureResult = Request.doAsync(Request.HttpMethod.POST,
                    hostReq, headers, Request.ofJsonObject(payload), 10);

            LOGGER.info("response: -- {} --", new String(futureResult.get().body()));

            ObjectMapper mapper = new ObjectMapper();
            ResponseLogin response = mapper.readValue(futureResult.get().body(), ResponseLogin.class);
            return CompletableFuture.completedFuture(Result.from(response, null));

        } catch (Exception e) {
            e.printStackTrace();
            return CompletableFuture.completedFuture(Result.from(null, e.getMessage()));
        }
	}
	
	@Async
    @Override
	public Future<Result<OrderUmeetmeResponse, String>> order(OrderUmeetmePayload payload) {
		try {
            LOGGER.info("Sending request order product umeetme...");

            String hostReq = String.format("https://api-dev.umeetme.id/setkab/dtp/create-order");

            Map<String, String> headers = new HashMap<>();
            headers.put(Request.CONTENT_TYPE, "application/json");
            headers.put(Request.AUTHORIZATION, "Bearer " + payload.getAccessToken());
            headers.put(Request.X_API_KEY, "PWedk0A5HBZhZZO46zBDnewPSQSpHI51");
            Future<HttpResponse<byte[]>> futureResult = Request.doAsync(Request.HttpMethod.POST,
                    hostReq, headers, Request.ofJsonObject(payload), 10);

            LOGGER.info("response: -- {} --", new String(futureResult.get().body()));

            ObjectMapper mapper = new ObjectMapper();
            OrderUmeetmeResponse orderUmeetmeResponse = mapper.readValue(futureResult.get().body(), OrderUmeetmeResponse.class);
            return CompletableFuture.completedFuture(Result.from(orderUmeetmeResponse, null));

        } catch (Exception e) {
            e.printStackTrace();
            return CompletableFuture.completedFuture(Result.from(null, e.getMessage()));
        }
	}
	
	@Async
    @Override
	public Future<Result<UserVerificationResponse, String>> userVerification(UserVerificationPayload payload) {
		try {
            LOGGER.info("Sending request verification userPayload...");

            String hostReq = String.format("https://api-dev.umeetme.id/setkab/dtp/verify-user");

            Map<String, String> headers = new HashMap<>();
            headers.put(Request.CONTENT_TYPE, "application/json");
            headers.put(Request.AUTHORIZATION, "Bearer " + payload.getAccessToken());
            headers.put(Request.X_API_KEY, "PWedk0A5HBZhZZO46zBDnewPSQSpHI51");
            Future<HttpResponse<byte[]>> futureResult = Request.doAsync(Request.HttpMethod.POST,
                    hostReq, headers, Request.ofJsonObject(payload), 10);

            LOGGER.info("response: -- {} --", new String(futureResult.get().body()));

            ObjectMapper mapper = new ObjectMapper();
            UserVerificationResponse userVerificationResponse = mapper.readValue(futureResult.get().body(), UserVerificationResponse.class);
            return CompletableFuture.completedFuture(Result.from(userVerificationResponse, null));

        } catch (Exception e) {
            e.printStackTrace();
            return CompletableFuture.completedFuture(Result.from(null, e.getMessage()));
        }
	}
	
	@Async
    @Override
	public Future<Result<ResponsePaymentNotification, String>> paymentNotification(PayloadPaymentNotification payload) {
		try {
            LOGGER.info("Sending request payment notification...");

            String hostReq = String.format("https://api-dev.umeetme.id/setkab/dtp/payment-notification");

            Map<String, String> headers = new HashMap<>();
            headers.put(Request.CONTENT_TYPE, "application/json");
            headers.put(Request.AUTHORIZATION, "Bearer " + payload.getAccessToken());
            headers.put(Request.X_API_KEY, "PWedk0A5HBZhZZO46zBDnewPSQSpHI51");
            Future<HttpResponse<byte[]>> futureResult = Request.doAsync(Request.HttpMethod.POST,
                    hostReq, headers, Request.ofJsonObject(payload), 10);

            LOGGER.info("response: -- {} --", new String(futureResult.get().body()));

            ObjectMapper mapper = new ObjectMapper();
            ResponsePaymentNotification responsePaymentNotification = mapper.readValue(futureResult.get().body(), ResponsePaymentNotification.class);
            return CompletableFuture.completedFuture(Result.from(responsePaymentNotification, null));

        } catch (Exception e) {
            e.printStackTrace();
            return CompletableFuture.completedFuture(Result.from(null, e.getMessage()));
        }
	}
}
