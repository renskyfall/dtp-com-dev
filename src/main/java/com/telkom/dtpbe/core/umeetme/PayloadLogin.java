package com.telkom.dtpbe.core.umeetme;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PayloadLogin {
	String username;
	String password;
	String room;
	Integer accessTokenExpired;
	String domainChannel;
	String appType;
}
