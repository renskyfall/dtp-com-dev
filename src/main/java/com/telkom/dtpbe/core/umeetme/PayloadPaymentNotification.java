package com.telkom.dtpbe.core.umeetme;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PayloadPaymentNotification {
	String accessToken;
	String expiredDate;
	String invoiceId;
	String orderId;
	String merchantId;
	String amount;
	String status;
	String isVaStatic;
}
