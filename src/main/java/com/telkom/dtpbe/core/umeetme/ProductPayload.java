package com.telkom.dtpbe.core.umeetme;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductPayload {
	String code;
	String type;
	Integer qty;
	Double price;
	Double totalPrice;
}
