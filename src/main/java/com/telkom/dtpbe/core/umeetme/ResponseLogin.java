package com.telkom.dtpbe.core.umeetme;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ResponseLogin {
    
	private String code;
	private String success;
	private String message;
	private Data data;
	
	@NoArgsConstructor
    public class Data {
		private String method;
        private String accessToken;
        private Long expiredIn;
        private Long issuedAt;
        private UserResponse user;
        
        public String getMethod() {
			return method;
		}
		public void setMethod(String method) {
			this.method = method;
		}
		public String getAccessToken() {
			return accessToken;
		}
		public void setAccessToken(String accessToken) {
			this.accessToken = accessToken;
		}
		public Long getExpiredIn() {
			return expiredIn;
		}
		public void setExpiredIn(Long expiredIn) {
			this.expiredIn = expiredIn;
		}
		public Long getIssuedAt() {
			return issuedAt;
		}
		public void setIssuedAt(Long issuedAt) {
			this.issuedAt = issuedAt;
		}
		public UserResponse getUser() {
			return user;
		}
		public void setUser(UserResponse user) {
			this.user = user;
		}
    }
}
