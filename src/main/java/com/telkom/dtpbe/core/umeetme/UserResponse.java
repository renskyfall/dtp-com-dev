package com.telkom.dtpbe.core.umeetme;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserResponse {

    private String id;

    private String fullName;

    private String email;

    private String username;

    @JsonIgnore
    private String password;

    private String mobileNumber;

    private String role;

    private String jobTitle;

    private String location;

    @JsonInclude(JsonInclude.Include.USE_DEFAULTS)
    private String userGroup;

    @JsonInclude(JsonInclude.Include.USE_DEFAULTS)
    private String street;

    private String avatar;

    @JsonProperty("isAdmin")
    private Boolean admin;

    private String group;

    private List<Object> domains;

    private Date createdAt;

    private Date updatedAt;

    private String userPMR;


}
