package com.telkom.dtpbe.core.umeetme;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserVerificationPayload {
	String accessToken;
	String fullname;
	String password;
	String verificationUserToken;
}
