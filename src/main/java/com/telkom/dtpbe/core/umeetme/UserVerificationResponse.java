package com.telkom.dtpbe.core.umeetme;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor	
public class UserVerificationResponse {
	
	private String code;
	private String success;
	private String message;
	private Data data;
	
	@NoArgsConstructor
    public class Data {
		private String userId;
		private String email;
		private String mobileNumber;
		private String fullName;
		
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getMobileNumber() {
			return mobileNumber;
		}
		public void setMobileNumber(String mobileNumber) {
			this.mobileNumber = mobileNumber;
		}
		public String getFullName() {
			return fullName;
		}
		public void setFullName(String fullName) {
			this.fullName = fullName;
		}
	}	
}	