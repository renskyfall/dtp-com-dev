package com.telkom.dtpbe.dtos;

import java.sql.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BankDTO {
	private Long id;
	private Long paymentMethodId;
	private String bankCode;
	private String bankName;
	private String bankImage;
	private String paymentCode;
	private String gateway;
	private String description;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;
}
