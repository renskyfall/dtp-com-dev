package com.telkom.dtpbe.dtos;

import java.sql.Timestamp;

import com.telkom.dtpbe.models.CategoryCode;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CodeDTO {
	private long codeId;
	private CategoryCode categoryCode;
	private String codeName;
	private String description;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;
}
