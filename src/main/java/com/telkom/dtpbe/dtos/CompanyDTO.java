package com.telkom.dtpbe.dtos;

import java.math.BigDecimal;
import java.sql.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CompanyDTO {
	private long companyId;
	private String companyName;
	private String companyField;
	private String position;
	private String companyAddress;
	private String country;
	private String province;
	private BigDecimal postalCode;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;
}
