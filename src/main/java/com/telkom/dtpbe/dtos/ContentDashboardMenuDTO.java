package com.telkom.dtpbe.dtos;
import java.sql.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ContentDashboardMenuDTO{

	private Long contentDashboardMenuId;
	private String title;
	private String url;
	private String imagePath;
	private String description;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;

	
}
