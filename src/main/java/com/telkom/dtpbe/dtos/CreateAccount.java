package com.telkom.dtpbe.dtos;

import com.telkom.dtpbe.models.Users;

public class CreateAccount {

    private String activationCode;

    private String email;

    private String mobileNumber;

    private Users user;

    public CreateAccount() {

    }

    public CreateAccount(String activationCode, String email, String mobileNumber, Users user) {
        this.activationCode = activationCode;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.user = user;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
