package com.telkom.dtpbe.dtos;
import java.sql.Timestamp;
import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FAQDTO implements java.io.Serializable {

	private Long faqId;
	private FAQCategoryDTO faqCategoryId;
	private String title;
	private String description;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;

	
}
