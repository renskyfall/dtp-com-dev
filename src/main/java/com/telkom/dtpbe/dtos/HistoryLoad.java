package com.telkom.dtpbe.dtos;

import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HistoryLoad {
	private String sort;
	private String status;
	private String product;
	private Date date;
}
