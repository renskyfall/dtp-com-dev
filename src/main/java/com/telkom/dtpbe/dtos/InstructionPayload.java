package com.telkom.dtpbe.dtos;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class InstructionPayload {

    private String bankCode;
    private String paymentMethod;
    private String bankName;
    private String bankImageUrl;
    private List<Method> methods;

    @Data
    @NoArgsConstructor
    public static class Method {
        private String name;
        private List<String> steps;
    }
}
