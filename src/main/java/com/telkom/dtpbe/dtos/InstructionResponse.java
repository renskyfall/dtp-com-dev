package com.telkom.dtpbe.dtos;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class InstructionResponse {
    private String bankCode;
    private String paymentMethod;
    private String bankName;
    private String bankImageUrl;
    private List<InstructionPayload.Method> methods;
    
    public InstructionResponse(InstructionPayload payload) {
        this.bankCode = payload.getBankCode();
        this.paymentMethod = payload.getPaymentMethod();
        this.bankName = payload.getBankName();
        this.bankImageUrl = payload.getBankImageUrl();
        this.methods = payload.getMethods();
    }
}
