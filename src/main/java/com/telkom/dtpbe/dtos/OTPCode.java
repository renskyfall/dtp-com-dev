package com.telkom.dtpbe.dtos;

public class OTPCode {

    private Integer otp;

    private String code;

    private String mobileNumber;

    private String domainChannel;

    public OTPCode() {}

    public OTPCode(Integer otp, String code, String mobileNumber, String domainChannel) {
        this.otp = otp;
        this.code = code;
        this.mobileNumber = mobileNumber;
        this.domainChannel = domainChannel;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Integer getOtp() {
        return otp;
    }

    public void setOtp(Integer otp) {
        this.otp = otp;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDomainChannel() {
        return domainChannel;
    }

    public void setDomainChannel(String domainChannel) {
        this.domainChannel = domainChannel;
    }
}
