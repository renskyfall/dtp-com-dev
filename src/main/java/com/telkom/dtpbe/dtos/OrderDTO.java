package com.telkom.dtpbe.dtos;

import java.sql.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrderDTO {

	private String ordersId;
	private String ordersProductId;
	private String invoiceNumber;
	private UsersDTO userId;
	private String products;
	private String status;
	private String paymentMethod;
	private String paymentMethodName;
	private String vaNumber;
	private Double totalPrice;
	private Double discount;
	private Double adminFee;
	private Double grandTotal;
	private Timestamp orderDate;
	private Timestamp expiredDate;
	private Timestamp activeDate;
	private Timestamp dueDate;
	private String cancelReason;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;
}
