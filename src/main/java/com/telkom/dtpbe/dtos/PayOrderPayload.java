package com.telkom.dtpbe.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PayOrderPayload {
    private String ordersId;
    private String userId;
    private String paymentMethodCode;
    private String paymentMethodName;
    private String paymentGatewayId;
}
