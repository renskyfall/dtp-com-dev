package com.telkom.dtpbe.dtos;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PayOrderResponse {

    private String ordersId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
    private ZonedDateTime paymentExpired;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
    private ZonedDateTime orderDate;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String vaNumber;

    private String description;
    private Double totalAmount;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double amount;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double fee;
    private Boolean isRedirect;
    private String redirectUrl;
    private String verifyUserToken;
    private String accessToken;
}
