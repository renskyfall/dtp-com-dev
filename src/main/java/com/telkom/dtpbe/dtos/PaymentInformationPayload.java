package com.telkom.dtpbe.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PaymentInformationPayload {
    private String userId;
    private String ordersId;
}
