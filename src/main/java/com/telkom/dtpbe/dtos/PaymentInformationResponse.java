package com.telkom.dtpbe.dtos;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PaymentInformationResponse {
	private String orderId;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
    private Timestamp paymentExpired;
    private String vaNumber;
    private Double totalAmount;
    private InstructionResponse instruction;
}
