package com.telkom.dtpbe.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PaymentMethod {

    private String methodType;
    private String methodName;
    private String description;
    @JsonProperty("banks")
    private List<BankDetail> bankDetails;

    @JsonIgnore
    private String bankCode;
    @JsonIgnore
    private String bankName;
    @JsonIgnore
    private String paymentImageUrl;
    @JsonIgnore
    private List<PaymentMethodDetail> details;

    public PaymentMethod(){}

    public PaymentMethod(String methodType, String methodName, List<BankDetail> bankDetails, String bankCode, String bankName, String paymentImageUrl, String description, List<PaymentMethodDetail> details) {
        this.methodType = methodType;
        this.methodName = methodName;
        this.bankDetails = bankDetails;
        this.bankCode = bankCode;
        this.bankName = bankName;
        this.paymentImageUrl = paymentImageUrl;
        this.description = description;
        this.details = details;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getPaymentImageUrl() {
        return paymentImageUrl;
    }

    public void setPaymentImageUrl(String paymentImageUrl) {
        this.paymentImageUrl = paymentImageUrl;
    }

    public List<PaymentMethodDetail> getDetails() {
        return details;
    }

    public void setDetails(List<PaymentMethodDetail> details) {
        this.details = details;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMethodType() {
        return methodType;
    }

    public void setMethodType(String methodType) {
        this.methodType = methodType;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public List<BankDetail> getBankDetails() {
        return bankDetails;
    }

    public void setBankDetails(List<BankDetail> bankDetails) {
        this.bankDetails = bankDetails;
    }

    public static class PaymentMethodDetail {
        private String paymentCode;
        private String paymentName;
        private String paymentType;
        private String gateway;
        @JsonIgnore
        private String paymentImageUrl;

        public PaymentMethodDetail(){}

        public PaymentMethodDetail(String paymentCode, String paymentName, String paymentType, String gateway, String paymentImageUrl) {
            this.paymentCode = paymentCode;
            this.paymentName = paymentName;
            this.paymentType = paymentType;
            this.gateway = gateway;
            this.paymentImageUrl = paymentImageUrl;
        }

        public String getPaymentCode() {
            return paymentCode;
        }

        public void setPaymentCode(String paymentCode) {
            this.paymentCode = paymentCode;
        }

        public String getPaymentName() {
            return paymentName;
        }

        public void setPaymentName(String paymentName) {
            this.paymentName = paymentName;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getGateway() {
            return gateway;
        }

        public void setGateway(String gateway) {
            this.gateway = gateway;
        }

        public String getPaymentImageUrl() {
            return paymentImageUrl;
        }

        public void setPaymentImageUrl(String paymentImageUrl) {
            this.paymentImageUrl = paymentImageUrl;
        }
    }

    public static class BankDetail implements Comparable<BankDetail> {
        private String bankCode;
        private String bankName;
        private String bankImage;
        private String paymentCode;
        private String gateway;

        public BankDetail() {}

        public BankDetail(String bankCode, String bankName, String bankImage, String paymentCode, String gateway) {
            this.bankCode = bankCode;
            this.bankName = bankName;
            this.bankImage = bankImage;
            this.paymentCode = paymentCode;
            this.gateway = gateway;
        }

        public String getBankCode() {
            return bankCode;
        }

        public void setBankCode(String bankCode) {
            this.bankCode = bankCode;
        }

        public String getBankName() {
            return bankName;
        }

        public void setBankName(String bankName) {
            this.bankName = bankName;
        }

        public String getPaymentCode() {
            return paymentCode;
        }

        public void setPaymentCode(String paymentCode) {
            this.paymentCode = paymentCode;
        }

        public String getGateway() {
            return gateway;
        }

        public void setGateway(String gateway) {
            this.gateway = gateway;
        }

        public String getBankImage() {
            return bankImage;
        }

        public void setBankImage(String bankImage) {
            this.bankImage = bankImage;
        }

        @Override
        public int compareTo(BankDetail bankDetail) {
            return getBankCode().compareTo(bankDetail.getBankCode());
        }
    }
}
