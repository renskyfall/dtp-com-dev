package com.telkom.dtpbe.dtos;

public class PaymentNotificationPayload {

    private String expiredDate;
    private String invoiceId;
    private String orderId;
    private String merchantId;
    private String amount;
    private String status;
    private String message;

    public PaymentNotificationPayload(){

    }

    public PaymentNotificationPayload(String expiredDate, String invoiceId, String orderId, String merchantId, String amount, String status, String message) {
        this.expiredDate = expiredDate;
        this.invoiceId = invoiceId;
        this.orderId = orderId;
        this.merchantId = merchantId;
        this.amount = amount;
        this.status = status;
        this.message = message;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
