package com.telkom.dtpbe.dtos;

public class PaymentNotificationResponse {
    private String status;
    private String invoiceId;
    private String message;

    public PaymentNotificationResponse() {

    }

    public PaymentNotificationResponse(String status, String invoiceId, String message) {
        this.status = status;
        this.invoiceId = invoiceId;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
