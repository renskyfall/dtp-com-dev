package com.telkom.dtpbe.dtos;

import java.sql.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProblemReportDTO {
	private long problemReportId;
	private String email;
	private String subject;
	private String description;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;
}
