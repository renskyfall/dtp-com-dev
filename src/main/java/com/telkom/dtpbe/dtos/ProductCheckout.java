package com.telkom.dtpbe.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductCheckout {
	private Long productPackageSubcriptionId;
	private Integer qty;
	private Double price;
	private String productCode;
	private String productName;
	private String packageName;
	private String productType;
	private String termOfUse;
	private Double subTotal;
}
