package com.telkom.dtpbe.dtos;

import java.sql.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductDTO {
	private long productId;
	private BrandDTO brand;
	private ProductTypeDTO productType;
	private String productName;
	private String tagline;
	private String description;
	private String imagePath;
	private String linkUrl;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;
}
