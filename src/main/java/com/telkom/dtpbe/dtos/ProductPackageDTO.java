package com.telkom.dtpbe.dtos;

import java.sql.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductPackageDTO {
	private long productPackageId;
	private ProductDTO product;
	private String productCode;
	private String packageName;
	private String productPriceDetail;
	private String description;
	private String additionalInfo;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;
}
