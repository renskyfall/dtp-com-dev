package com.telkom.dtpbe.dtos;

import java.sql.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductPackageSubcriptionDTO {

	private Long productPackageSubcriptionId;
	private ProductPackageDTO productPackage;
	private ProductSubcriptionTypeDTO productSubcriptionType;
	private Double productPrice;
	private Integer quantity;
	private String description;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;
	private String properties;
	private Boolean quantityCustomizable;
	private Boolean priceInclude;
}
