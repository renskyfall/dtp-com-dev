package com.telkom.dtpbe.dtos;

import javax.persistence.Basic;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import com.telkom.dtpbe.validator.EmailValidator;
import com.telkom.dtpbe.validator.MobileNumberValidator;

public class Register {

    @Basic
    @EmailValidator
    private String email;

    @NotBlank
    private String redirectURL;

    @MobileNumberValidator
    @NotEmpty(message = "{userRegister.mobileNumber.required}")
    private String mobileNumber;

    public Register() {

    }

    public Register(String email, String redirectURL, String mobileNumber) {
        this.email = email;
        this.redirectURL = redirectURL;
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRedirectURL() {
        return redirectURL;
    }

    public void setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
