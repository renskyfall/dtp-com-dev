package com.telkom.dtpbe.dtos;

public class ResetPassword {

    private String activationCode;

    private String email;

    private String mobileNumber;

    private String newPassword;

    public ResetPassword() {

    }

    public ResetPassword(String activationCode, String email, String mobileNumber, String newPassword) {
        this.activationCode = activationCode;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.newPassword = newPassword;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
