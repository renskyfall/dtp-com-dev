package com.telkom.dtpbe.dtos;
import java.sql.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SubPrivacyPolicyDTO implements java.io.Serializable {
	
	private Long subPrivacyPolicyId;
	private TermsAndConditionsDTO privacyPolicyId;
	private String title;
	private String description;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;

	
}
