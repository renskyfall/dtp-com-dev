package com.telkom.dtpbe.dtos;
import java.sql.Timestamp;
import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SubTermsAndConditionsDTO implements java.io.Serializable {
	
	private Long subTermsAndConditionsId;
	private TermsAndConditionsDTO termsAndConditionsId;
	private String title;
	private String description;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;

	
}
