package com.telkom.dtpbe.exception;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.telkom.dtpbe.core.shared.EmptyJson;
import com.telkom.dtpbe.core.shared.Response;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (ex instanceof BindException) {

            Map<String, String> errors = new HashMap<>();
            ((BindException) ex).getBindingResult().getAllErrors().forEach((error) -> {
                String fieldName = ((FieldError) error).getField();
                String errorMessage = error.getDefaultMessage();
                errors.put(fieldName, errorMessage);
            });

            Response response = new Response(HttpStatus.BAD_REQUEST.value(), false, new EmptyJson(), errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        Response response = new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), false, new EmptyJson(), ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Response response = new Response(HttpStatus.BAD_REQUEST.value(), false, new EmptyJson(), ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Response response = new Response(HttpStatus.NOT_FOUND.value(), false, new EmptyJson(), ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ServletException.class)
    protected ResponseEntity<Object> handleInvalidAuthorizationException(ServletException ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Response response = new Response(HttpStatus.UNAUTHORIZED.value(), false, new EmptyJson(), ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<Response> handleMaxSizeException(MaxUploadSizeExceededException ex) {
        Response response = new Response(HttpStatus.BAD_REQUEST.value(), false, new EmptyJson(), "file too large, 1MB maximum");
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Response> handleResourceNotFoundException(ResourceNotFoundException ex) {
        Response response = new Response(HttpStatus.NOT_FOUND.value(), false, new EmptyJson(), "resource not found");
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

}


