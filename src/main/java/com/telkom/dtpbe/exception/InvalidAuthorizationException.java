package com.telkom.dtpbe.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class InvalidAuthorizationException extends Exception {

    public InvalidAuthorizationException(String message) {
        super(message);
    }
}
