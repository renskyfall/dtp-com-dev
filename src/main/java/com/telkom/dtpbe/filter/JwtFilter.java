package com.telkom.dtpbe.filter;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telkom.dtpbe.core.jwt.CustomClaim;
import com.telkom.dtpbe.core.jwt.IJwtService;
import com.telkom.dtpbe.core.shared.EmptyJson;
import com.telkom.dtpbe.core.shared.Response;
import com.telkom.dtpbe.core.shared.Result;

@Component
public class JwtFilter extends OncePerRequestFilter {

    private final static Logger LOGGER = LoggerFactory.getLogger(JwtFilter.class);

    @Autowired
    private Environment env;

    @Autowired
    private IJwtService jwtService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        final String tokenFromHeader = request.getHeader("Authorization");

        if (tokenFromHeader != null) {
            if (tokenFromHeader.startsWith("Bearer ")) {

            if (SecurityContextHolder.getContext().getAuthentication() == null) {
                Future<Result<CustomClaim, String>> resultFuture = jwtService.validate(tokenFromHeader);
                try {
                    if (resultFuture.get().getError() != null) {
                        sendResponse(response, resultFuture.get().getError());
                        return;
                    }	

                    CustomClaim claim = resultFuture.get().getData();
                    UserDetails userDetails = userDetailsService.loadUserByUsername(claim.getSubject());
                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                            new UsernamePasswordAuthenticationToken(
                                    userDetails, null, userDetails.getAuthorities());
                    usernamePasswordAuthenticationToken
                            .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    // After setting the Authentication in the context, we specify
                    // that the current userPayload is authenticated. So it passes the
                    // Spring Security Configurations successfully.
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                } catch (InterruptedException e) {
                    LOGGER.error("interrupted {}", e.getLocalizedMessage());
                    // instead of throwing exception, just immediately send a http response
                    sendResponse(response, e);
                    return;
                } catch (ExecutionException e) {
                    LOGGER.error("execution exception {}", e.getLocalizedMessage());
                    // instead of throwing exception, just immediately send a http response
                    sendResponse(response, e);
                    return;
                }
            }
        } else if (tokenFromHeader.startsWith("Basic ")) {
            try {
                String[] authParts = tokenFromHeader.split("\\s+");
                String authInfo = authParts[1];
                byte[] bytes = Base64.getDecoder().decode(authInfo);

                String basicAuthUsername = env.getRequiredProperty("BASIC_AUTH_USERNAME");
                String basicAuthPass = env.getRequiredProperty("BASIC_AUTH_PASSWORD");
                String decodedAuth = new String(bytes);
                String[] decodedParts = decodedAuth.split(":");
                if ((!decodedParts[0].equals(basicAuthUsername)) || (!decodedParts[1].equals(basicAuthPass))) {
                    sendResponse(response, "Invalid authorization");
                    return;
                }

                UserDetails userDetails = new org.springframework.security.core.userdetails.User(
                        decodedParts[0], decodedParts[1], new ArrayList<>());
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                        new UsernamePasswordAuthenticationToken(
                                userDetails, null, userDetails.getAuthorities());
                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

        chain.doFilter(request, response);
    }

    private void sendResponse(HttpServletResponse response, Exception e) throws IOException {
        response.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        OutputStream out = response.getOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(out, new Response(HttpStatus.UNAUTHORIZED.value(),
                false,
                new EmptyJson(),
                e.getMessage()));
        out.flush();
    }

    private void sendResponse(HttpServletResponse response, String e) throws IOException {
        response.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        OutputStream out = response.getOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(out, new Response(HttpStatus.UNAUTHORIZED.value(),
                false,
                new EmptyJson(),
                e));
        out.flush();
    }
}
