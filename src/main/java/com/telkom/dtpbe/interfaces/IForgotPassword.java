package com.telkom.dtpbe.interfaces;

import java.util.Map;
import java.util.concurrent.Future;

import com.telkom.dtpbe.core.shared.Result;
import com.telkom.dtpbe.dtos.ForgotPassword;
import com.telkom.dtpbe.dtos.OTPCode;
import com.telkom.dtpbe.dtos.ResetPassword;
import com.telkom.dtpbe.dtos.UsersDTO;

public interface IForgotPassword {
	Future<Result<Map<String, String>, String>> forgotPassword(ForgotPassword forgotPassword);
	
	Future<Result<UsersDTO, String>> resetPassword(ResetPassword resetPassword);

    Future<Result<Map<String, String>, String>> verificationCode(OTPCode code);
}
