package com.telkom.dtpbe.interfaces;

import java.util.List;
import java.util.concurrent.Future;

import com.telkom.dtpbe.core.shared.Result;
import com.telkom.dtpbe.dtos.InstructionPayload;
import com.telkom.dtpbe.dtos.InstructionResponse;
import com.telkom.dtpbe.dtos.PayOrderPayload;
import com.telkom.dtpbe.dtos.PayOrderResponse;
import com.telkom.dtpbe.dtos.PaymentInformationPayload;
import com.telkom.dtpbe.dtos.PaymentInformationResponse;
import com.telkom.dtpbe.dtos.PaymentNotificationPayload;
import com.telkom.dtpbe.dtos.PaymentNotificationResponse;

public interface IPayment {
    Future<Result<PayOrderResponse, String>> payment(PayOrderPayload payload);
    Future<Result<PaymentNotificationResponse, String>> receivePaymentNotification(PaymentNotificationPayload payload);
    Future<Result<InstructionResponse, String>> getPaymentInstruction(String methodCode);
    Future<Result<String, String>> addPaymentInstruction(List<InstructionPayload> payload);
    Future<Result<PaymentInformationResponse, String>> getPaymentInformation(String ordersId, String userid);
}