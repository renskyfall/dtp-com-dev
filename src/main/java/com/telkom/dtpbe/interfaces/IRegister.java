package com.telkom.dtpbe.interfaces;

import java.util.Map;
import java.util.concurrent.Future;

import com.telkom.dtpbe.core.shared.Result;
import com.telkom.dtpbe.dtos.CreateAccount;
import com.telkom.dtpbe.dtos.OTPCode;
import com.telkom.dtpbe.dtos.Register;
import com.telkom.dtpbe.dtos.UsersDTO;

public interface IRegister {
	Future<Result<Map<String, String>, String>> register(Register register);
	
	Future<Result<Map<String, Object>, String>> createAccount(CreateAccount createAccount);

    Future<Result<Map<String, String>, String>> verificationCode(OTPCode code);
}
