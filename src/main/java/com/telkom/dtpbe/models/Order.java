package com.telkom.dtpbe.models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "orders", schema = "public")
@Data
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Order {
	
	@Id
	@Column(name = "orders_id", unique = true, nullable = false)
	private String ordersId;
	
	@Column(name = "orders_product_id")
	private String ordersProductId;
	
	@Column(name = "invoice_number")
	private String invoiceNumber;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private Users userId;
	
	@Column(name = "products")
	private String products;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "payment_method")
	private String paymentMethod;
	
	@Column(name = "payment_method_name")
	private String paymentMethodName;
	
	@Column(name = "va_number")
	private String vaNumber;
	
	@Column(name = "total_price")
	private Double totalPrice;
	
	@Column(name = "discount")
	private Double discount;
	
	@Column(name = "admin_fee")
	private Double adminFee;
	
	@Column(name = "grand_total")
	private Double grandTotal;
	
	@Column(name = "order_date", length = 13, updatable = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Bangkok")
	@CreationTimestamp
	@CreatedDate
	private Timestamp orderDate;
	
	@Column(name = "expired_date", length = 13)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Bangkok")
	private Timestamp expiredDate;
	
	@Column(name = "active_date", length = 13)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Bangkok")
	private Timestamp activeDate;
	
	@Column(name = "due_date", length = 13)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Bangkok")
	private Timestamp dueDate; 
	
	@Column(name = "cancel_reason")
	private String cancelReason;
	
	@Column(name = "is_deleted")
	private Boolean isDeleted;
	
	@Column(name = "created_by")
	@CreatedBy
	private Long createdBy;
	
	@Column(name = "created_on", length = 13, updatable = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Bangkok")
	@CreationTimestamp
	@CreatedDate
	private Timestamp createdOn;
	
	@Column(name = "last_modified_by")
	@LastModifiedBy
	private Long lastModifiedBy;
	
	@Column(name = "last_modified_on", length = 13)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Bangkok")
	@LastModifiedDate
	@UpdateTimestamp	
	private Timestamp lastModifiedOn;
}
