package com.telkom.dtpbe.models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "payment_method", schema = "public")
@Data
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class PaymentMethod {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_payment_method_id_seq")
	@SequenceGenerator(name = "generator_payment_method_id_seq", sequenceName = "payment_method_id_seq", schema = "public", allocationSize = 1)
	@Column(name = "payment_method_id", unique = true, nullable = false)
	private Long paymentMethodId;
	
	@Column(name = "method_type")
	private String methodType;
	
	@Column(name = "method_name")
	private String methodName;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "is_deleted")
	private Boolean isDeleted;
	
	@Column(name = "created_by")
	@CreatedBy
	private Long createdBy;
	
	@Column(name = "created_on", length = 13, updatable = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Bangkok")
	@CreationTimestamp
	@CreatedDate
	private Timestamp createdOn;
	
	@Column(name = "last_modified_by")
	@LastModifiedBy
	private Long lastModifiedBy;
	
	@Column(name = "last_modified_on", length = 13)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Bangkok")
	@LastModifiedDate
	@UpdateTimestamp
	private Timestamp lastModifiedOn;
}
