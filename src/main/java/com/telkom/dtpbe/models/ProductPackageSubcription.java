package com.telkom.dtpbe.models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "product_package_subcription", schema = "public")
@Data
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)

public class ProductPackageSubcription {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_product_package_subcription_id_seq")
	@SequenceGenerator(name = "generator_product_package_subcription_id_seq", sequenceName = "product_package_subcription_id_seq", schema = "public", allocationSize = 1)
	@Column(name = "product_package_subcription_id", unique = true, nullable = false)
	private Long productPackageSubcriptionId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_package_id")
	private ProductPackage productPackage;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_subcription_type_id")
	private ProductSubcriptionType productSubcriptionType;
	
	@Column(name = "product_price")
	private Double productPrice;
	
	@Column(name = "properties")
	private String properties;
	
	@Column(name = "quantity_customizable")
	private Boolean quantityCustomizable;
	
	@Column(name = "price_include")
	private Boolean priceInclude;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "is_deleted")
	private Boolean isDeleted;
	
	@Column(name = "created_by")
	@CreatedBy
	private Long createdBy;
	
	@Column(name = "created_on", length = 13, updatable = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Bangkok")
	@CreationTimestamp
	@CreatedDate
	private Timestamp createdOn;
	
	@Column(name = "last_modified_by")
	@LastModifiedBy
	private Long lastModifiedBy;
	
	@Column(name = "last_modified_on", length = 13)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Bangkok")
	@LastModifiedDate
	@UpdateTimestamp	
	private Timestamp lastModifiedOn;
}
