package com.telkom.dtpbe.modules.auth.delivery;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.Constant;
import com.telkom.dtpbe.MessageSetting;
import com.telkom.dtpbe.core.shared.EmptyJson;
import com.telkom.dtpbe.core.shared.Response;
import com.telkom.dtpbe.core.shared.Result;
import com.telkom.dtpbe.modules.auth.dtos.OTPCode;
import com.telkom.dtpbe.modules.auth.dtos.RequestLogin;
import com.telkom.dtpbe.modules.auth.dtos.RequestLoginOTP;
import com.telkom.dtpbe.modules.auth.dtos.RequestRefreshToken;
import com.telkom.dtpbe.modules.auth.dtos.ResponseLogin;
import com.telkom.dtpbe.modules.auth.dtos.TemporaryToken;
import com.telkom.dtpbe.modules.auth.usecase.IAuthUsecase;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@RestController
@RequestMapping(value = Constant.ROOT_PATH)
public class AuthHandler {
    private final static Logger LOGGER = LoggerFactory.getLogger(AuthHandler.class);

    @Autowired
    private IAuthUsecase authUsecase;

    @Autowired
    private MessageSetting messageSetting;


    @PostMapping("/auth/login")
    public Response login(@RequestParam(name = "lang", required = false) String lang,
                          @RequestBody @Valid RequestLogin reqLogin,
                          HttpServletResponse response) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException {
        Future<Result<ResponseLogin, String>> resultFuture = authUsecase.login(reqLogin);
        try {
            if (resultFuture.get().isError()) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
//                String message = messageSetting.getMessage("invalid.login.creds", lang);
                String message = resultFuture.get().getError();
                return new Response(HttpStatus.UNAUTHORIZED.value(),
                        false,
                        new EmptyJson(),
                        message);
            }

            ResponseLogin respLogin = resultFuture.get().getData();
            return new Response(HttpStatus.OK.value(),
                    true,
                    respLogin,
                    "success generate access token");
        } catch (InterruptedException | ExecutionException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return new Response(HttpStatus.BAD_REQUEST.value(),
                    false,
                    new EmptyJson(),
                    e.getMessage());
        }
    }


    @PostMapping("/auth/login/otp")
    public Response loginOTP(@RequestBody @Valid RequestLoginOTP reqLogin, HttpServletResponse response) {
        Future<Result<Map<String, String>, String>> resultFuture = authUsecase.loginOTP(reqLogin);
        try {
            if (resultFuture.get().isError()) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return new Response(HttpStatus.UNAUTHORIZED.value(),
                        false,
                        new EmptyJson(),
                        resultFuture.get().getError());
            }

            return new Response(HttpStatus.OK.value(),
                    true,
                    resultFuture.get().getData(),
                    "success");
        } catch (InterruptedException | ExecutionException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return new Response(HttpStatus.BAD_REQUEST.value(),
                    false,
                    new EmptyJson(),
                    e.getMessage());
        }
    }

    @PostMapping("/auth/otp-verification")
    public Response verificationCode(@RequestBody @Valid OTPCode otpCode, HttpServletResponse response) {

        try {
            Future<Result<ResponseLogin, String>> resultFuture = authUsecase.otpVerification(otpCode);

            if (resultFuture.get().isError()) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return new Response(HttpStatus.BAD_REQUEST.value(),
                        false, new EmptyJson(), resultFuture.get().getError());
            }

            return new Response(HttpStatus.OK.value(),
                    true,
                    resultFuture.get().getData(),
                    "success");
        } catch (InterruptedException | ExecutionException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return new Response(HttpStatus.BAD_REQUEST.value(),
                    false,
                    new EmptyJson(),
                    e.getMessage());
        }
    }

    @PostMapping("/auth/refreshtoken")
    public Response getRefreshToken(@RequestBody @Valid RequestRefreshToken req, HttpServletResponse response) {

        Future<Result<ResponseLogin, String>> resultFuture = authUsecase.refreshToken(req);
        try {
            if (resultFuture.get().isError()) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return new Response(HttpStatus.UNAUTHORIZED.value(),
                        false,
                        new EmptyJson(),
                        resultFuture.get().getError());
            }

            ResponseLogin resp = resultFuture.get().getData();
            LOGGER.info(resp.toString());
            return new Response(HttpStatus.OK.value(),
                    true,
                    resp,
                    "success refresh access token");
        } catch (InterruptedException | ExecutionException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return new Response(HttpStatus.BAD_REQUEST.value(),
                    false,
                    new EmptyJson(),
                    e.getMessage());
        }
    }

    @PostMapping("/auth/temporary-token")
    public Response temporaryToken(@RequestBody @Valid TemporaryToken temporaryToken, HttpServletResponse response) {

        try {
            Future<Result<ResponseLogin, String>>  resultFuture = authUsecase.getTemporaryToken(temporaryToken);

            if (resultFuture.get().isError()) {
                response.setStatus(HttpStatus.HTTP_VERSION_NOT_SUPPORTED.value());
                return new Response(HttpStatus.HTTP_VERSION_NOT_SUPPORTED.value(),
                        false, new EmptyJson(), resultFuture.get().getError());
            }

            return new Response(HttpStatus.OK.value(),
                    true,
                    resultFuture.get().getData(),
                    "success");
        } catch (InterruptedException | ExecutionException e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    false,
                    new EmptyJson(),
                    e.getMessage());
        }
    }
}
