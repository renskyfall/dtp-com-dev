package com.telkom.dtpbe.modules.auth.dtos;

public class OTPCode {

    private Integer otp;

    private String code;

    private String mobileNumber;

    public OTPCode() {}

    public OTPCode(Integer otp, String code, String mobileNumber) {
        this.otp = otp;
        this.code = code;
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Integer getOtp() {
        return otp;
    }

    public void setOtp(Integer otp) {
        this.otp = otp;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
