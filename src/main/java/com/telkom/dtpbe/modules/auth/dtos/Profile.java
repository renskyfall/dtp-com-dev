package com.telkom.dtpbe.modules.auth.dtos;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.Users;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;
import lombok.Data;

@Data
public class Profile extends Minio{
    private final static Logger LOGGER = LoggerFactory.getLogger(Profile.class);

    private Long userId;

    private String fullName;

    private String email;

    @JsonIgnore
    private String password;

    private String mobileNumber;

    private String role;

    private String avatar;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String accessToken;


    public Profile() {

    }

    public Profile(Long userId, String fullName, String email, String mobileNumber, String role, 
                        String avatar) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException {
        this.userId = userId;
        this.fullName = fullName;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.role = role;
        this.avatar = avatar;
    }

    public Profile from(Users users) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException {
        return new Profile(users.getUserId(), users.getFullName(), users.getEmail(),
        		users.getMobileNumber(), users.getRole().getRoleName(), users.getImagePath());
    }

    public Profile from(Users users, String accessToken) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException {
        String avatarFullUrl = "";
        if(users.getImagePath() == null) {
        	avatarFullUrl = "";
		} else if (!users.getImagePath().isEmpty()) {
			avatarFullUrl = minio().presignedGetObject(bucketName, users.getImagePath());
		} else {
			avatarFullUrl = "";
		}

        Profile Profile = new Profile(users.getUserId(), users.getFullName(), users.getEmail(),
        		users.getMobileNumber(), users.getRole().getRoleName(), avatarFullUrl);
        Profile.setAccessToken(accessToken);
        return Profile;
    }
}
