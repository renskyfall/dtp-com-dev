package com.telkom.dtpbe.modules.auth.dtos;

public class RequestLogin {
    private String username;
    private String password;
    private Integer accessTokenExpired;

    public RequestLogin() {

    }

    public RequestLogin(String username, String password, Integer accessTokenExpired) {
        this.username = username;
        this.password = password;
        this.accessTokenExpired = accessTokenExpired;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAccessTokenExpired() {
        return accessTokenExpired;
    }

    public void setAccessTokenExpired(Integer accessTokenExpired) {
        this.accessTokenExpired = accessTokenExpired;
    }

}
