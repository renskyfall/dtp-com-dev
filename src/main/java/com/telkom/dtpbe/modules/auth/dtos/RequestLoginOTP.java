package com.telkom.dtpbe.modules.auth.dtos;

public class RequestLoginOTP {

    private String mobileNumber;

    public RequestLoginOTP() {}

    public RequestLoginOTP(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
