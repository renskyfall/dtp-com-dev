package com.telkom.dtpbe.modules.auth.dtos;

public class RequestRefreshToken {
    private String refreshToken;
    private Integer accessTokenExpired;


    public RequestRefreshToken(String refreshToken, Integer accessTokenExpired) {
        this.refreshToken = refreshToken;
        this.accessTokenExpired = accessTokenExpired;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Integer getAccessTokenExpired() {
        return accessTokenExpired;
    }

    public void setAccessTokenExpired(Integer accessTokenExpired) {
        this.accessTokenExpired = accessTokenExpired;
    }
}
