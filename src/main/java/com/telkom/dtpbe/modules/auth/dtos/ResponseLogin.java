package com.telkom.dtpbe.modules.auth.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.telkom.dtpbe.core.jwt.CustomClaim;
import com.telkom.dtpbe.models.Users;

public class ResponseLogin {
    private String method;
    private String accessToken;
    private Long expiredIn;
    private Long issuedAt;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Profile profile;

    public ResponseLogin() {
    }

    public ResponseLogin(String accessToken) {
        this.method = "Bearer";
        this.accessToken = accessToken;
    }

    public ResponseLogin(String accessToken, CustomClaim claim) {
        this.method = "Bearer";
        this.accessToken = accessToken;
        this.expiredIn = claim.getExpiration().toInstant().getEpochSecond();
        this.issuedAt = claim.getIssuedAt().toInstant().getEpochSecond();
    }

    public ResponseLogin(String accessToken, CustomClaim claim, Profile profile) {
        this.method = "Bearer";
        this.accessToken = accessToken;
        this.expiredIn = claim.getExpiration().toInstant().getEpochSecond();
        this.issuedAt = claim.getIssuedAt().toInstant().getEpochSecond();
        this.profile = profile;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Long getExpiredIn() {
        return expiredIn;
    }

    public void setExpiredIn(Long expiredIn) {
        this.expiredIn = expiredIn;
    }

    public Long getIssuedAt() {
        return issuedAt;
    }

    public void setIssuedAt(Long issuedAt) {
        this.issuedAt = issuedAt;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
}
