package com.telkom.dtpbe.modules.auth.dtos;

public class TemporaryToken {
    private String refreshToken;
    private Integer accessTokenExpired;
    private String linkOrCodeRoom;
    private String platformLanguage;
    public TemporaryToken(String refreshToken, Integer accessTokenExpired) {
        this.refreshToken = refreshToken;
        this.accessTokenExpired = accessTokenExpired;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Integer getAccessTokenExpired() {
        return accessTokenExpired;
    }

    public void setAccessTokenExpired(Integer accessTokenExpired) {
        this.accessTokenExpired = accessTokenExpired;
    }

    public String getLinkOrCodeRoom() {
        return linkOrCodeRoom;
    }

    public void setLinkOrCodeRoom(String linkOrCodeRoom) {
        this.linkOrCodeRoom = linkOrCodeRoom;
    }

    public String getPlatformLanguage() {
        return platformLanguage;
    }

    public void setPlatformLanguage(String platformLanguage) {
        this.platformLanguage = platformLanguage;
    }
}
