package com.telkom.dtpbe.modules.auth.service;

import java.util.ArrayList;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.telkom.dtpbe.models.Users;
import com.telkom.dtpbe.repositories.UsersRepository;

@Service
public class AuthUserDetailService implements UserDetailsService {

    private final static Logger LOGGER = LoggerFactory.getLogger(AuthUserDetailService.class);

//    @Autowired
//    private ILdapClient ldapClient;
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        Result<UserPayload, String> result = ldapClient.findOne(username);
//        if (result.getError() != null) {
//            LOGGER.error("loadUserByUsername error {}", result.getError());
//            throw new UsernameNotFoundException("userPayload with username " + username + " not found");
//        } else {
//            return new org.springframework.security.core.userdetails.User(result.getData().getUsername(), result.getData().getPassword(), new ArrayList<>());
//        }
//    }


    @Autowired
    private UsersRepository userQuery;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Users> result = userQuery.findByEmailOrMobileNumber(username);
        if (!result.isPresent()) {
            // for Temporary Token
            LOGGER.error("loadUserByUsername or loadRoomByCode error");
            throw new UsernameNotFoundException("room or username with name " + username + " not found");
        } else {
            return new org.springframework.security.core.userdetails.User(username, result.get().getPassword(), new ArrayList<>());
        }
    }
}
