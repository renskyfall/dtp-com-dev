package com.telkom.dtpbe.modules.auth.usecase;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

import com.telkom.dtpbe.Constant;
import com.telkom.dtpbe.core.jwt.CustomClaim;
import com.telkom.dtpbe.core.jwt.IJwtService;
import com.telkom.dtpbe.core.shared.Result;
import com.telkom.dtpbe.core.sms.ISMSService;
import com.telkom.dtpbe.dtos.UsersDTO;
import com.telkom.dtpbe.models.Users;
import com.telkom.dtpbe.modules.auth.dtos.OTPCode;
import com.telkom.dtpbe.modules.auth.dtos.Profile;
import com.telkom.dtpbe.modules.auth.dtos.RequestLogin;
import com.telkom.dtpbe.modules.auth.dtos.RequestLoginOTP;
import com.telkom.dtpbe.modules.auth.dtos.RequestRefreshToken;
import com.telkom.dtpbe.modules.auth.dtos.ResponseLogin;
import com.telkom.dtpbe.modules.auth.dtos.TemporaryToken;
import com.telkom.dtpbe.password.Pbkdf2;
import com.telkom.dtpbe.repositories.UsersRepository;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@Service
public class AuthUsecase implements IAuthUsecase {

    private final static Logger LOGGER = LoggerFactory.getLogger(AuthUsecase.class);

    ModelMapper modelMapper = new ModelMapper();
    
    @Autowired
    private IJwtService jwtService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UsersRepository UsersQuery;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private ISMSService smsSender;
    
    Profile profile;
    
    public UsersDTO convertToDTO(Users users) {
		return modelMapper.map(users, UsersDTO.class);
	}
    
    @Async
    @Override
    public Future<Result<ResponseLogin, String>> login(RequestLogin reqLogin) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException {
    	
    	try {
		    Optional<Users> result = UsersQuery.findByEmailOrMobileNumber(reqLogin.getUsername());
		    if (!result.isPresent()) {
		    	if(isNumeric(reqLogin.getUsername())){
		    		 return CompletableFuture.completedFuture(Result.from(
		                     null,
		                     "Phone number isn’t registered yet"));
				} else {
					 return CompletableFuture.completedFuture(Result.from(
			                 null,
			                 "Email isn’t registered yet"));
				}
		        
		    } else if (result.isPresent() && !Pbkdf2.verifyPassword(reqLogin.getPassword(), result.get().getPassword())) {
		    	return CompletableFuture.completedFuture(Result.from(
		                 null,
		                 "Password is incorrect"));
		    }
		    
		//        if(users.isPresent() && Pbkdf2.verifyPassword(requestLogin.getPassword(), users.get().getPassword())) {
		//			message = "Invalid username or password";
		//			users.get().setStatus(1);
		//			usersRepository.save(users.get());
		//			message = "success";
		//			isSuccess = true;
		//			showHashMap.put("data", convertToDTO(users.get()));
		//		} else if (!users.isPresent()){
		//			if(isNumeric(requestLogin.getUsername())){
		//				message = "Phone number isn’t registered yet";
		//			} else {
		//				message = "Email isn’t registered yet";
		//			}
		//			showHashMap.put("data", null);
		//		} else {
		//			message = "Password is incorrect";
		//			showHashMap.put("data", null);
		//		} 
		
		    Users users = result.get();
		    LOGGER.info("UserPayload : {}", users.getEmail());
		
		//        if (Strings.isNullOrEmpty(Users.getPassword())) {
		//            return CompletableFuture.completedFuture(Result.from(
		//                    null,
		//                    "Users doesn't have password"));
		//        }
		
		//        if (!Users.getPassword().equals(reqLogin.getPassword())) {
		//            CompletableFuture<Result<ResponseLogin, String>> future = CompletableFuture.completedFuture(Result.from(
		//                    null,
		//                    "Invalid Usersname or password"));
		//            return future;
		//        }
		
		    Integer expirationInMinutes = 1;
		
		    if (reqLogin.getAccessTokenExpired() != null) {
		        expirationInMinutes = reqLogin.getAccessTokenExpired();
		    }
		
		    LOGGER.info("expiration in minute : {}", expirationInMinutes);
		
		    CustomClaim claim = new CustomClaim(
		            String.valueOf(users.getUserId()),
		            reqLogin.getUsername(),
		            "setkab.umeetme.id",
		            "setkab-userPayload",
		            expirationInMinutes);
		
		    claim.setName(users.getFullName());
		    claim.setContextEmail(users.getEmail());
		    
		    
		        Future<Result<String, String>> resultFuture = jwtService.generate(claim);
		        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(reqLogin.getUsername(),
		                reqLogin.getPassword()));
		        
		        if (resultFuture.get().isError()) {
		            return CompletableFuture.completedFuture(
		                    Result.from(null,
		                            resultFuture.get().getError()));
		        }
		        
		        profile = new Profile(users.getUserId(), users.getFullName(), users.getEmail(), users.getMobileNumber(), users.getRole().getRoleName(), users.getImagePath());
		        ResponseLogin responseLogin = new ResponseLogin(resultFuture.get().getData(), claim, profile.from(users));
		        
		        return CompletableFuture.completedFuture(
		                Result.from(responseLogin,
		                        null));
        } catch (InterruptedException | ExecutionException | AuthenticationException e) {
            e.printStackTrace();
            LOGGER.info("error auth : {}", e.getMessage());
            return CompletableFuture.completedFuture(
                    Result.from(null, "Wrong Username or Password"));
        }
    }


    @Async
    @Override
    public Future<Result<ResponseLogin, String>> refreshToken(RequestRefreshToken req) {

        try {
            Future<Result<CustomClaim, String>> resultFuture = jwtService.decode(req.getRefreshToken());

            String err = resultFuture.get().getError();
            CustomClaim claim = resultFuture.get().getData();
            if ((err != null) && (claim == null)) {
                return CompletableFuture.completedFuture(
                        Result.from(null, err));
            }

            Optional<Users> optionalUsers = UsersQuery.findByEmailOrMobileNumber(claim.getSubject());
            if (!optionalUsers.isPresent()) {
                return CompletableFuture.completedFuture(Result.from(
                        null,
                        "Users not found"));
            }
            Users users = optionalUsers.get();
            
            int refreshTokenLeeway = 2;
            long expired = claim.getExpiration().getTime();
            Date afterAddingLeeway = new Date(expired + (refreshTokenLeeway * Constant.ONE_DAY_IN_MILLIS));

            Date now = new Date();
            if (afterAddingLeeway.before(now)) {
                return CompletableFuture.completedFuture(
                        Result.from(null, "cannot refresh token"));
            }

            Integer expirationInMinutes = 1;
            if (req.getAccessTokenExpired() != null) {
                expirationInMinutes = req.getAccessTokenExpired();
            }

            claim.setIssuedAt(new Date());
            claim.setExpiration(Date.from(Instant.now().plus(expirationInMinutes.longValue(), ChronoUnit.MINUTES)));

            Future<Result<String, String>> resultToken = jwtService.generate(claim);
            if (resultToken.get().isError()) {
                return CompletableFuture.completedFuture(
                        Result.from(null,
                                resultFuture.get().getError()));
            }

            ResponseLogin responseLogin = new ResponseLogin(resultToken.get().getData(), claim);
            return CompletableFuture.completedFuture(
                    Result.from(responseLogin, null));
        } catch (Exception e) {
            return CompletableFuture.completedFuture(
                    Result.from(null,
                            e.getMessage()));
        }
    }
    
    private static String generateRandomChars(Integer length) {
        String chars = "123456789";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(chars.charAt(random.nextInt(chars.length())));
        }

        return sb.toString();
    }

	@Override
	public Future<Result<Map<String, String>, String>> loginOTP(RequestLoginOTP reqLogin) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Future<Result<ResponseLogin, String>> otpVerification(OTPCode code) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Future<Result<ResponseLogin, String>> getTemporaryToken(TemporaryToken temporaryToken) {
		// TODO Auto-generated method stub
		return null;
	}

	public static boolean isNumeric(String str) { 
		try {  
			Double.parseDouble(str);  
			return true;
		} catch(NumberFormatException e){  
		    return false;  
		}  
	}
}
