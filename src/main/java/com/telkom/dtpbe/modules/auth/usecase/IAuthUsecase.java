package com.telkom.dtpbe.modules.auth.usecase;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.concurrent.Future;

import com.telkom.dtpbe.core.shared.Result;
import com.telkom.dtpbe.modules.auth.dtos.OTPCode;
import com.telkom.dtpbe.modules.auth.dtos.RequestLogin;
import com.telkom.dtpbe.modules.auth.dtos.RequestLoginOTP;
import com.telkom.dtpbe.modules.auth.dtos.RequestRefreshToken;
import com.telkom.dtpbe.modules.auth.dtos.ResponseLogin;
import com.telkom.dtpbe.modules.auth.dtos.TemporaryToken;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

public interface IAuthUsecase {
    Future<Result<ResponseLogin, String>> login(RequestLogin reqLogin) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException;

    Future<Result<ResponseLogin, String>> refreshToken(RequestRefreshToken req);

    Future<Result<Map<String, String>, String>> loginOTP(RequestLoginOTP reqLogin);

    Future<Result<ResponseLogin, String>> otpVerification(OTPCode code);

    Future<Result<ResponseLogin, String>> getTemporaryToken(TemporaryToken temporaryToken);
}
