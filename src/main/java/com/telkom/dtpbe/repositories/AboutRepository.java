package com.telkom.dtpbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.About;

@Repository
public interface AboutRepository extends JpaRepository<About, Long> {

	
	
}
