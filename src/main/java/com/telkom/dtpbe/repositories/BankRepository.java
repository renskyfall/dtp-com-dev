package com.telkom.dtpbe.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.Bank;
import com.telkom.dtpbe.models.ProductAddOn;

@Repository
public interface BankRepository extends JpaRepository<Bank, Long> {
	
	//Get Bank By Payment Method Id
	@Query(value = "SELECT * FROM bank WHERE payment_method_id = :paymentMethodId ORDER BY bank_id ASC", nativeQuery = true)
	List<Bank> findBankByPaymentMethodId(@Param("paymentMethodId") Long paymentMethodId);
}