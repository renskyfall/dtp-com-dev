package com.telkom.dtpbe.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.Cart;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long>{
	
	@Query(value = "SELECT * FROM cart WHERE user_id = :userId", nativeQuery = true)
	public Optional<Cart> findCartByUserId(@Param("userId") Long userId);
	
}
