package com.telkom.dtpbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.CategoryCode;

@Repository
public interface CategoryCodeRepository extends JpaRepository<CategoryCode, Long>{

}
