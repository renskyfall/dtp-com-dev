package com.telkom.dtpbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.Code;

@Repository
public interface CodeRepository extends JpaRepository<Code, Long>{

}
