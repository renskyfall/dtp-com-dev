package com.telkom.dtpbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.ContentDashboardHome;

@Repository
public interface ContentDashboardHomeRepository extends JpaRepository<ContentDashboardHome, Long>{

}
