package com.telkom.dtpbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.ContentDashboardMenu;

@Repository
public interface ContentDashboardMenuRepository extends JpaRepository<ContentDashboardMenu, Long>{

}
