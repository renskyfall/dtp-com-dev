package com.telkom.dtpbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.ContentProduct;

@Repository
public interface ContentProductRepository extends JpaRepository<ContentProduct, Long>{

}
