package com.telkom.dtpbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.FAQCategory;

@Repository
public interface FAQCategoryRepository extends JpaRepository<FAQCategory, Long> {

	
	
}
