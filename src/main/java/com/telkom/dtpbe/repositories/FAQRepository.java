package com.telkom.dtpbe.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.FAQ;

@Repository
public interface FAQRepository extends JpaRepository<FAQ, Long> {
	
	//Get FAQ by FAQ Category Id
	@Query(value = "SELECT * FROM faq WHERE faq_category_id = :faqCategoryId", nativeQuery = true)
	List<FAQ> getFaqByCategoryId(@Param("faqCategoryId") Long faqCategoryId);
	
	//Get FAQ by words
	@Query(value = "SELECT * FROM faq WHERE title ~* :words OR description ~* :words", nativeQuery = true)
	List<FAQ> getFaqByWords(@Param("words") String words);
}
