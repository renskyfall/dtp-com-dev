package com.telkom.dtpbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.Guide;

@Repository
public interface GuideRepository extends JpaRepository<Guide, Long> {

	
	
}
