package com.telkom.dtpbe.repositories;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.History;

@Repository
public interface HistoryRepository extends JpaRepository<History, Long> {
	
	@Query(value = "SELECT h.* FROM history as h\r\n"
			+ "join(select orders_id,max(created_on) from history group by orders_id) as m on m.max = h.created_on\r\n"
			+ "join orders as o on h.orders_id = o.orders_id\r\n"
			+ "where o.user_id = :userId \r\n"
			+ "AND h.status ~* :status ORDER BY o.order_date desc", nativeQuery = true)
	ArrayList<History> getHistory(@Param("userId") Long userId, @Param("status") String status);
}
