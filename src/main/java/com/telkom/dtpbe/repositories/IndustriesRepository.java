package com.telkom.dtpbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.Industries;

@Repository
public interface IndustriesRepository extends JpaRepository<Industries, Long> {

}
