package com.telkom.dtpbe.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.Instruction;

@Repository
public interface InstructionRepository  extends JpaRepository<Instruction, Long> {
	
	//Get Instruction By Method Code
	@Query(value = "SELECT * FROM instruction WHERE payment_method = :paymentCode ORDER BY instruction_id ASC", nativeQuery = true)
	Optional<Instruction> findByMethodCode(@Param("paymentCode") String paymentCode);	
}
