package com.telkom.dtpbe.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, String> {

	//Get OrderProduct By OrderProduct Id and UserPayload Id
	@Query(value = "SELECT * FROM orders WHERE orders_id = :orderId AND user_id = :userId ORDER BY orders_id ASC", nativeQuery = true)
	Optional<Order> findByOrderIdAndUsersId(@Param("orderId") String orderId, @Param("userId") Long userId);	
	
	//Get OrderProduct By OrderProduct Id and invoice Id
	@Query(value = "SELECT * FROM orders WHERE orders_id = :orderId AND invoice_number = :invoiceId ORDER BY orders_id ASC", nativeQuery = true)
	Optional<Order> findByOrderIdAndInvoiceId(@Param("orderId") String orderId, @Param("invoiceId") String string);	
	
}
