package com.telkom.dtpbe.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.ProductAddOn;
import com.telkom.dtpbe.models.ProductPackage;

@Repository
public interface ProductAddOnRepository extends PagingAndSortingRepository<ProductAddOn, Long>{
	
	//Get Add on By Add On Type Id
	@Query(value = "SELECT * FROM product_add_on WHERE product_add_on_type_id = :productAddOnTypeId ORDER BY product_add_on_id ASC", nativeQuery = true)
	List<ProductAddOn> findProductAddOnByProductAddOnTypeId(@Param("productAddOnTypeId") Long productAddOnTypeId);
}
