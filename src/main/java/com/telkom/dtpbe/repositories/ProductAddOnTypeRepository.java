package com.telkom.dtpbe.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.ProductAddOn;
import com.telkom.dtpbe.models.ProductAddOnType;

@Repository
public interface ProductAddOnTypeRepository extends PagingAndSortingRepository<ProductAddOnType, Long>{

	//Get Product Add On by Product Package Id
	@Query(value = "SELECT * FROM product_add_on_type p, product_package_add_on_type pp WHERE p.product_add_on_type_id = pp.product_add_on_type_id AND pp.product_package_id = :productPackageId", nativeQuery = true)
	List<ProductAddOnType> findProductAddOnByProductPackageId(@Param("productPackageId") Long productPackageId);
}
