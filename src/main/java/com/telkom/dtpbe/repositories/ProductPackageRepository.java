package com.telkom.dtpbe.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.ProductPackage;

@Repository
public interface ProductPackageRepository extends JpaRepository<ProductPackage, Long>{
	
	//Get Package Product by Product ID
	@Query(value = "SELECT * FROM product_package WHERE product_id = :productId ORDER BY product_package_id ASC", nativeQuery = true)
	List<ProductPackage> findProductPackageByProductId(@Param("productId") Long productId);
}
