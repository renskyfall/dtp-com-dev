package com.telkom.dtpbe.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.ProductPackage;
import com.telkom.dtpbe.models.ProductPackageSubcription;

@Repository
public interface ProductPackageSubcriptionRepository extends PagingAndSortingRepository<ProductPackageSubcription, Long> {
	
	//Get Package Product Subcription by Product Package
	@Query(value = "SELECT * FROM product_package_subcription WHERE product_package_id = :productPackageId ORDER BY product_package_subcription_id ASC", nativeQuery = true)
	List<ProductPackageSubcription> findProductPackageSubcriptionByProductPackageId(@Param("productPackageId") Long productPackageId);
}
