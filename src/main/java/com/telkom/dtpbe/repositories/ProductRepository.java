package com.telkom.dtpbe.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.Product;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

	//Get Product by Product Type ID
	@Query(value = "SELECT * FROM product WHERE product_type_id = :productTypeId ORDER BY product_id ASC", nativeQuery = true)
	List<Product> findProductByProductTypeId(@Param("productTypeId") Long productTypeId);
	
	//Get Product by Industries
	@Query(value = "SELECT * FROM product p, product_industries pi WHERE pi.industries_id = :industries ORDER BY product_id ASC", nativeQuery = true)
	List<Product> findProductByIndustries(@Param("industries") Long industries);
	
	//Get Product By Solution
	@Query(value = "SELECT * FROM product Where product_type_id IN (:solution) ORDER BY product_id ASC", nativeQuery = true)
	List<Product> findProductBySolution(@Param("solution") ArrayList<Long> solution);
	
	//Get Product By Solution
	@Query(value = "SELECT * FROM product Where product_type_id IN (:solution) ORDER BY product_id ASC", nativeQuery = true)
	Slice<Product> getFilter(@Param("solution") ArrayList<Long> solution, Pageable pagable);
}
