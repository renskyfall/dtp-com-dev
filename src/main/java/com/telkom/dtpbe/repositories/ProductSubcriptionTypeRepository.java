package com.telkom.dtpbe.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.ProductPackage;
import com.telkom.dtpbe.models.ProductSubcriptionType;

@Repository
public interface ProductSubcriptionTypeRepository extends JpaRepository<ProductSubcriptionType, Long>{
	
	//Get Package Product Subcription by Product Package
	@Query(value = "SELECT * FROM product_package_subcription WHERE product_package_id = :productPackageId ORDER BY product_package_subcription_id ASC", nativeQuery = true)
	List<ProductPackage> findProductPackageSubcriptionByProductPackageId(@Param("productPackageId") Long productPackageId);
}
