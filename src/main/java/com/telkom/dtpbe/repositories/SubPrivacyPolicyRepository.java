package com.telkom.dtpbe.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.SubPrivacyPolicy;

@Repository
public interface SubPrivacyPolicyRepository extends JpaRepository<SubPrivacyPolicy, Long> {

	//Get SubTermsAndConditions by termsAndConditionsId
	//PP = Privacy Policy
	@Query(value = "SELECT * FROM sub_privacy_policy WHERE privacy_policy_id = :privacyPolicyId ORDER BY sub_privacy_policy_id", nativeQuery = true)
	List<SubPrivacyPolicy> getSubPPbyPPId(@Param("privacyPolicyId") Long privacyPolicyId);
	
}
