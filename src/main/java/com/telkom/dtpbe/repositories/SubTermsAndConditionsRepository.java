package com.telkom.dtpbe.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.SubTermsAndConditions;

@Repository
public interface SubTermsAndConditionsRepository extends JpaRepository<SubTermsAndConditions, Long> {

	//Get SubTermsAndConditions by termsAndConditionsId
	//TAC = Terms And Conditions
	@Query(value = "SELECT * FROM sub_terms_and_conditions WHERE terms_and_conditions_id = :termsAndConditionsId ORDER BY sub_terms_and_conditions_id", nativeQuery = true)
	List<SubTermsAndConditions> getSubTACByTACId(@Param("termsAndConditionsId") Long termsAndConditionsId);
	
}
