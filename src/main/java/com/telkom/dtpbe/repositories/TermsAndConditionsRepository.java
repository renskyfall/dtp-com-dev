package com.telkom.dtpbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.TermsAndConditions;

@Repository
public interface TermsAndConditionsRepository extends JpaRepository<TermsAndConditions, Long> {

	
	
}
