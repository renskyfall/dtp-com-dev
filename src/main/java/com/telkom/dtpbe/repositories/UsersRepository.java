package com.telkom.dtpbe.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, Long>{

	@Query(value = "SELECT * FROM users WHERE mobile_number = :mobileNumber", nativeQuery = true)
	public Optional<Users> findByMobileNumber(@Param("mobileNumber") String mobileNumber);
	
	@Query(value = "SELECT * FROM users WHERE email = :email", nativeQuery = true)
	public Optional<Users> findByEmail(@Param("email") String email);
	
	@Query(value = "SELECT * FROM users WHERE email = :username OR mobile_number = :username", nativeQuery = true)
	public Optional<Users> findByEmailOrMobileNumber(@Param("username") String username);
}
