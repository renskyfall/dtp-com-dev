package com.telkom.dtpbe.repositories.controllers;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ContentContactUsDTO;
import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.ContentContactUs;
import com.telkom.dtpbe.repositories.ContentContactUsRepository;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ContentContactUsRepositoryController extends Minio{
	
ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ContentContactUsRepository contentContactUsRepository;
	
	public ContentContactUsDTO convertToDTO(ContentContactUs contentContactUs) {
		return modelMapper.map(contentContactUs, ContentContactUsDTO.class);
	}

	public ContentContactUs convertToEntity(ContentContactUsDTO contentContactUsDTO) {
		return modelMapper.map(contentContactUsDTO, ContentContactUs.class);
	}
	
	// Get All Contact Us
	@SuppressWarnings("deprecation")
	@GetMapping("/contentContactUs/getAll")
	public HashMap<String, Object> getAllContentContactUs() throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException {
		HashMap<String, Object> mapResult = new HashMap<String, Object>();
		List<HashMap<String, Object>> listContentContactUs = new ArrayList<HashMap<String, Object>>();
		String imageUrl = null;
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		try {
			for (ContentContactUs c : contentContactUsRepository.findAll()) {
				ContentContactUsDTO contentContactUsDTO = convertToDTO(c);
				
				HashMap<String, Object> mapTemp = new HashMap<String, Object>();
				mapTemp.put("contactUsId",contentContactUsDTO.getContactUsId());
				mapTemp.put("title",contentContactUsDTO.getTitle());
				mapTemp.put("imagePath",contentContactUsDTO.getImagePath());
				mapTemp.put("description",contentContactUsDTO.getDescription());
				if(c.getImagePath() == null) {
					imageUrl = null;
				} else if (!c.getImagePath().isEmpty()) {
					imageUrl = minio().presignedGetObject(bucketName, c.getImagePath());
				} else {
					imageUrl = null;
				}
				mapTemp.put("imageUrl",imageUrl);
				
				listContentContactUs.add(mapTemp);
			}
			
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		} 
		
		mapResult.put("code", code);
		mapResult.put("success", isSuccess);
		mapResult.put("message", message);
		mapResult.put("total", listContentContactUs.size());
		mapResult.put("data", listContentContactUs);

		return mapResult;
	}
}
