package com.telkom.dtpbe.repositories.controllers;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.ContentDashboardHome;
import com.telkom.dtpbe.models.Product;
import com.telkom.dtpbe.repositories.ContentDashboardHomeRepository;
import com.telkom.dtpbe.repositories.ProductRepository;
import com.telkom.dtpbe.repositories.ProductTypeRepository;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class DashboardHomeRepositoryController extends Minio{
	

	@Autowired
	ProductTypeRepository productTypeRepository;
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	ContentDashboardHomeRepository contentDashboardHomeRepository;
	
	//Get Digitouch description
	@GetMapping("/dashboard/home/getDescription")
	public HashMap<String, Object> getDigitouchDescription()throws InvalidEndpointException, InvalidPortException {
		HashMap<String,Object> result = new HashMap<String,Object>();
		HashMap<String,Object> data = new HashMap<String,Object>();
		String message = null;
		boolean isSuccess = false, dataFound = false;
		Integer code = null;
		Optional<ContentDashboardHome> contentDashboardHome = contentDashboardHomeRepository.findById((long) 1);
		
		try {
			if(contentDashboardHome.isPresent()) {
				data.put("title", contentDashboardHome.get().getTitle());
				data.put("subtitle", contentDashboardHome.get().getDescription());
				dataFound = true;
			} 
			
			if(dataFound)
				message = "Read All Success!";
			else
				message = "No Data Found!";
			
			code = 200;
    		isSuccess = true;
			
    	} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
        
		result.put("code", code);
        result.put("success", isSuccess);
		result.put("message", message);
		result.put("data", data);
		
		return result;
	}
		
	//filterProduct
	@GetMapping("/dashboard/home/searchProduct")
	public HashMap<String, Object> getProductByFilter(
			@RequestParam(value = "filter") String filter ) throws InvalidEndpointException, InvalidPortException {
		HashMap<String,Object> result = new HashMap<String,Object>();
		ArrayList<HashMap<String, Object>> listData = new ArrayList<HashMap<String, Object>>();
		String message = null;
		boolean isSuccess = false, dataFound = false;
		Integer code = null;
		
		try {
			for(Product product:productRepository.findAll()) {
				if(filter.isEmpty()) {
					listData.add(showData(product));
					dataFound = true;
				} else if(product.getProductName().toLowerCase().contains(filter.toLowerCase())) {
					listData.add(showData(product));
					dataFound = true;
				} else {
					listData.isEmpty();
				}
			}
			
			if(dataFound)
				message = "Read All Success!";
			else
				message = "No Data Found!";
			
    		code = 200;
    		isSuccess = true;
    	} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
        
		result.put("code", code);
        result.put("success", isSuccess);
		result.put("message", message);
		result.put("total", listData.size());
		result.put("data", listData);
		return result;
	}
	
	
	//Show Data
	@SuppressWarnings("deprecation")
	public HashMap<String,Object>  showData(Product product) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException{
		HashMap<String,Object> dataHashMap = new HashMap<String,Object>();
		String image = null;
	
		if(product.getImagePath() != null) {
			image = minio().presignedGetObject(bucketName, product.getImagePath());
			
		}
		dataHashMap.put("productId", product.getProductId());
		dataHashMap.put("productName", product.getProductName());
		dataHashMap.put("productType", product.getProductType().getProductTypeName());
		dataHashMap.put("brandName", product.getBrand().getBrandName());
		dataHashMap.put("linkUrl", product.getLinkUrl());
//		dataHashMap.put("tagline", product.getTagline());
//		dataHashMap.put("description", product.getDescription());
		dataHashMap.put("imageUrl", image);
		
		return dataHashMap;
	}
}
