package com.telkom.dtpbe.repositories.controllers;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.interfaces.Minio;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class DownloadRepositoryController extends Minio{

	@SuppressWarnings("deprecation")
	@GetMapping("/downloadFile/")
	public String termsAndConditionsURL(@RequestParam (value="filePathInput") String filePathInput) throws InvalidEndpointException, InvalidPortException, InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidResponseException, NoSuchAlgorithmException, XmlParserException, IOException, InvalidExpiresRangeException, ServerException {			
		
		String filePath = filePathInput;
		
		return minio().presignedGetObject(bucketName, filePath);
	}
	
	
}