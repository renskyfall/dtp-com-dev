package com.telkom.dtpbe.repositories.controllers;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.FAQ;
import com.telkom.dtpbe.models.FAQCategory;
import com.telkom.dtpbe.repositories.FAQCategoryRepository;
import com.telkom.dtpbe.repositories.FAQRepository;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class FAQRepositoryController extends Minio{

	@Autowired
	FAQRepository faqRepository;
	
	@Autowired
	FAQCategoryRepository faqCategoryRepository;
	

	//show all Data
	@GetMapping("/faq/showAll")
	public HashMap<String, Object> showFAQ() throws InvalidEndpointException, InvalidPortException, InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, XmlParserException, IOException, ServerException{
		HashMap<String,Object> result = new HashMap<String,Object>();
		ArrayList<HashMap<String, Object>> listData = new ArrayList<HashMap<String, Object>>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for(FAQCategory data : faqCategoryRepository.findAll()) {
				listData.add(showDataFAQCategory(data));
			}
    		message = "Read All Success!";
    		code = 200;
    		isSuccess = true;
    	} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
        
		result.put("code", code);
        result.put("success", isSuccess);
		result.put("message", message);
		result.put("total", listData.size());
		result.put("data", listData);
		return result;
	}
	
	//show all Data
	@PostMapping("/faq/search")
	public HashMap<String, Object> searchFAQ(@Param("words") String words) throws InvalidEndpointException, InvalidPortException, InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, XmlParserException, IOException, ServerException{
		HashMap<String,Object> result = new HashMap<String,Object>();
		ArrayList<HashMap<String, Object>> listData = new ArrayList<HashMap<String, Object>>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for(FAQ data : faqRepository.getFaqByWords(words)) {
				listData.add(showDataFAQ(data));
			}
    		message = "Read All Success!";
    		code = 200;
    		isSuccess = true;
    	} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
        
		result.put("code", code);
        result.put("success", isSuccess);
		result.put("message", message);
		result.put("total", listData.size());
		result.put("data", listData);
		return result;
	}
	
	//Show Data FAQCategory
	public HashMap<String,Object>  showDataFAQCategory(FAQCategory data) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException{
		HashMap<String,Object> dataHashMap = new HashMap<String,Object>();
		
		Long faqCategoryId = data.getFaqCategoryId();
		String faqCategoryDescription = data.getDescription();
		
		dataHashMap.put("faqCategoryId", faqCategoryId);
		dataHashMap.put("faqCategoryDescription", faqCategoryDescription);
		dataHashMap.put("faqData", showDataFAQByCategoryId(faqCategoryId));
		
		return dataHashMap;
	}
	
	//Show Data FAQ By Category Id
	public ArrayList<HashMap<String,Object>> showDataFAQByCategoryId(Long faqCategoryId) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException{
		ArrayList<HashMap<String,Object>> listDataHashMap = new ArrayList<HashMap<String,Object>>();
		
		
		List<FAQ> faqData = faqRepository.getFaqByCategoryId(faqCategoryId);
		for(FAQ tempData : faqData) {
			HashMap<String,Object> dataHashMap = new HashMap<String,Object>();
			dataHashMap = showDataFAQ(tempData);
			listDataHashMap.add(dataHashMap);
		}
		
		
		return listDataHashMap;
	}
	
	//Show Data FAQ
	public HashMap<String,Object> showDataFAQ(FAQ data) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException{
		HashMap<String,Object> dataHashMap = new HashMap<String,Object>();
		
		dataHashMap.put("faqId", data.getFaqId());
		dataHashMap.put("faqTitle", data.getTitle());
		dataHashMap.put("faqCategoryDescription", data.getDescription());

		return dataHashMap;
	}
}