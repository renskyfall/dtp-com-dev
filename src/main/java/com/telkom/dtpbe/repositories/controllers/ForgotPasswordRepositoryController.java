package com.telkom.dtpbe.repositories.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.Constant;
import com.telkom.dtpbe.core.shared.EmptyJson;
import com.telkom.dtpbe.core.shared.Response;
import com.telkom.dtpbe.core.shared.Result;
import com.telkom.dtpbe.dtos.ForgotPassword;
import com.telkom.dtpbe.dtos.OTPCode;
import com.telkom.dtpbe.dtos.ResetPassword;
import com.telkom.dtpbe.dtos.UsersDTO;
import com.telkom.dtpbe.interfaces.IForgotPassword;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping(value = Constant.ROOT_PATH)
public class ForgotPasswordRepositoryController {

	 private final static Logger LOGGER = LoggerFactory.getLogger(ForgotPasswordRepositoryController.class);
	 
	 @Autowired
	 private IForgotPassword iForgotPassword;
	 
	 @PostMapping("/users/forgotPassword")
     public Response forgotPassword(@RequestBody @Valid ForgotPassword forgotPassword, BindingResult result, HttpServletResponse response) {

        try {
            if (forgotPassword.getEmail() != null && forgotPassword.getMobileNumber() != null) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return new Response(HttpStatus.BAD_REQUEST.value(),
                        false, new EmptyJson(), "Email & mobile number, both of them may not be filled");
            }

            Future<Result<Map<String, String>, String>> resultFuture = iForgotPassword.forgotPassword(forgotPassword);

            if (resultFuture.get().isError()) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return new Response(HttpStatus.BAD_REQUEST.value(),
                        false, new EmptyJson(), resultFuture.get().getError());
            }

            return new Response(HttpStatus.OK.value(),
                    true,
                    resultFuture.get().getData(),
                    "success");
        } catch (InterruptedException | ExecutionException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return new Response(HttpStatus.BAD_REQUEST.value(),
                    false,
                    new EmptyJson(),
                    e.getMessage());
        }
    }

    @PostMapping("/users/resetPassword")
    public Response resetPassword(@RequestBody @Valid ResetPassword resetPassword, BindingResult result, HttpServletResponse response) {

        try {
            Future<Result<UsersDTO, String>> resultFuture = iForgotPassword.resetPassword(resetPassword);

            if (resultFuture.get().isError()) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return new Response(HttpStatus.BAD_REQUEST.value(),
                        false, new EmptyJson(), resultFuture.get().getError());
            }

            return new Response(HttpStatus.OK.value(),
                    true,
                    resultFuture.get().getData(),
                    "success");
        } catch (InterruptedException | ExecutionException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return new Response(HttpStatus.BAD_REQUEST.value(),
                    false,
                    new EmptyJson(),
                    e.getMessage());
        }
    }

    @PostMapping("/users/verificationCode")
    public Response verificationCode(@RequestBody @Valid OTPCode otpCode, HttpServletResponse response) {

        try {
            Future<Result<Map<String, String>, String>> resultFuture = iForgotPassword.verificationCode(otpCode);

            if (resultFuture.get().isError()) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return new Response(HttpStatus.BAD_REQUEST.value(),
                        false, new EmptyJson(), resultFuture.get().getError());
            }

            return new Response(HttpStatus.OK.value(),
                    true,
                    resultFuture.get().getData(),
                    "success");
        } catch (InterruptedException | ExecutionException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return new Response(HttpStatus.BAD_REQUEST.value(),
                    false,
                    new EmptyJson(),
                    e.getMessage());
        }
    }
    
    private Map<String, String> generateError(BindingResult bindingResult) {
        Map<String, String> errors = new HashMap<>();
        bindingResult.getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
