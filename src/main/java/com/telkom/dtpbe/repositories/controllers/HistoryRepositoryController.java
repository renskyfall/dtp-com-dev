package com.telkom.dtpbe.repositories.controllers;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Optional;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telkom.dtpbe.dtos.HistoryDTO;
import com.telkom.dtpbe.dtos.HistoryLoad;
import com.telkom.dtpbe.dtos.ProductCheckout;
import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.History;
import com.telkom.dtpbe.models.Order;
import com.telkom.dtpbe.models.Product;
import com.telkom.dtpbe.models.ProductPackage;
import com.telkom.dtpbe.models.ProductPackageSubcription;
import com.telkom.dtpbe.models.Users;
import com.telkom.dtpbe.repositories.HistoryRepository;
import com.telkom.dtpbe.repositories.OrderRepository;
import com.telkom.dtpbe.repositories.ProductPackageSubcriptionRepository;
import com.telkom.dtpbe.repositories.UsersRepository;
import com.telkom.dtpbe.service.PaymentService;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class HistoryRepositoryController extends Minio{
	
	private final static Logger LOGGER = LoggerFactory.getLogger(HistoryRepositoryController.class);
	
	ModelMapper modelMapper = new ModelMapper();
	ObjectMapper objectMapper = new ObjectMapper();
	
	@Autowired
	ProductPackageSubcriptionRepository productPackageSubcriptionRepository;
	
	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	HistoryRepository historyRepository;
	
	@Autowired
	UsersRepository usersRepository;
	
	public HistoryDTO convertToDTO(History object) {
        return modelMapper.map(object, HistoryDTO.class);
	}
	
	public History convertToEntity(HistoryDTO object) {
	    return modelMapper.map(object, History.class);
	}

	
	//show all ProductCheckout
  	@PostMapping("/history/show")
  	public HashMap<String, Object> showHistory(@AuthenticationPrincipal UserDetails userDetails, @RequestBody HistoryLoad historyLoad, HttpServletResponse response) throws InvalidEndpointException, InvalidPortException, InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, XmlParserException, IOException, ServerException{
  		HashMap<String,Object> result = new HashMap<String,Object>();
  		ArrayList<HashMap<String, Object>> listData = new ArrayList<HashMap<String, Object>>();
  		String message;
  		boolean isSuccess = false;
  		Integer code = null;
  		Optional<Users> userData = usersRepository.findByEmailOrMobileNumber(userDetails.getUsername());
  		Long userId = userData.get().getUserId();
		try {
			ArrayList<History> listHistoryArrayList = historyRepository.getHistory(userId, historyLoad.getStatus());
			LOGGER.info("List History Empty = "+ listHistoryArrayList.isEmpty());
			LOGGER.info("data = "+ listHistoryArrayList);
			for(History tempHistory : listHistoryArrayList) {
	  				HashMap<String,Object> data = showHistoryData(tempHistory);
	  				listData.add(data);

	  		}
      		message = "Read All Success!";
      		code = 200;
      		isSuccess = true;

    		result.put("total", listData.size());
    		result.put("data", listData);
      	} catch (Exception e) {
  			message = e.getMessage();
  			code = 400;
  		}
  		
  		
  		response.setStatus(code);
  		result.put("code", code);
        result.put("success", isSuccess);
  		result.put("message", message);
  		return result;
  	}
  	
  	//Show Data Product
  	@SuppressWarnings("deprecation")
  	public HashMap<String,Object>  showDataProductCheckout(ProductCheckout productCheckout) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException{
  		HashMap<String,Object> dataHashMap = new HashMap<String,Object>();
  		Long productPackageSubcriptionId = productCheckout.getProductPackageSubcriptionId();
  		
  		ProductPackageSubcription productPackageSubcription = productPackageSubcriptionRepository.findById(productPackageSubcriptionId).get();
  		Product product = productPackageSubcription.getProductPackage().getProduct();
  		ProductPackage productPackage = productPackageSubcription.getProductPackage();
  		
  		Integer qty = productCheckout.getQty();
  		String packageName = productPackage.getPackageName();
  		String productName = product.getProductName();
  		String productType = product.getProductType().getProductTypeName();
  		String productSubcriptionTypeName = productPackageSubcription.getProductSubcriptionType().getProductSubcriptionTypeName();
  		Double productPrice = productPackageSubcription.getProductPrice();
  		
  		String image = null;
  		if(product.getImagePath() != null) {
  			image = minio().presignedGetObject(bucketName, product.getImagePath());
  			
  		}
  		productCheckout.setPackageName(packageName);
  		productCheckout.setPrice(productPrice);
  		productCheckout.setProductName(productName);
  		productCheckout.setProductType(productType);
  		productCheckout.setTermOfUse(productSubcriptionTypeName);
  		productCheckout.setSubTotal(qty*productPrice);
  		dataHashMap.put("productData", productCheckout);
  		dataHashMap.put("imageUrl", image);
  		
  		return dataHashMap;
  	}
  	
  	//Show Data Product
  	@SuppressWarnings("deprecation")
  	public HashMap<String,Object>  showHistoryData(History history) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException{
  		HashMap<String,Object> dataHashMap = new HashMap<String,Object>();
  		ArrayList<ProductCheckout> productCheckoutList = objectMapper.readValue(history.getOrder().getProducts() , new TypeReference<ArrayList<ProductCheckout>>(){});
  		ProductCheckout productCheckout = productCheckoutList.get(0);
  		DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-LLLL-yyyy");
  		DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy, HH:mm");
  		Timestamp expDate = history.getOrder().getExpiredDate();
  		Timestamp activeDate = history.getOrder().getActiveDate();
  		Timestamp orderDate = history.getOrder().getActiveDate();
  		LOGGER.info("expDate = "+ expDate);
  		LOGGER.info("activeDate = "+ activeDate);
  		LOGGER.info("orderDate = "+ orderDate);
  		
  		String formattedExpDate = null;
  		String formattedActiveDate = null;
  		String validUntil = null;
  		
  		if(expDate != null) {
  			formattedExpDate = dateTimeFormat.format(expDate.toLocalDateTime());
  		}
  		if(!history.getStatus().equals("WAITING_PAYMENT")) {
  			formattedExpDate = null;
  		}
  		if(activeDate != null) {
  			Calendar calendar = Calendar.getInstance();
  			calendar.setTimeInMillis(activeDate.getTime());
  			Integer tempQty = productCheckout.getQty();
  			Integer days = 30;
  			Integer totalDays = tempQty * days;

  			calendar.add(Calendar.DATE, totalDays);
  			Timestamp validUntilTimestamp = new Timestamp(calendar.getTimeInMillis());
  			formattedActiveDate = dateFormat.format(activeDate.toLocalDateTime());
  			validUntil = dateFormat.format(validUntilTimestamp.toLocalDateTime());
  			LOGGER.info("formattedActiveDate = "+ formattedActiveDate);
  			LOGGER.info("validUntil = "+ validUntil);
  			
  		}
  		
  		String formattedOrderDate = dateFormat.format(orderDate.toLocalDateTime());  		
  		
  		Long productPackageSubcriptionId = productCheckout.getProductPackageSubcriptionId();
  		
  		ProductPackageSubcription productPackageSubcription = productPackageSubcriptionRepository.findById(productPackageSubcriptionId).get();
  		Product product = productPackageSubcription.getProductPackage().getProduct();
  		ProductPackage productPackage = productPackageSubcription.getProductPackage();
  		
  		Integer qty = productCheckout.getQty();
  		String packageName = productPackage.getPackageName();
  		String productSubcriptionTypeName = productPackageSubcription.getProductSubcriptionType().getProductSubcriptionTypeName();

  		
  		String image = null;
  		if(product.getImagePath() != null) {
  			image = minio().presignedGetObject(bucketName, product.getImagePath());
  			
  		}
  		dataHashMap.put("productName", product.getProductName());
  		dataHashMap.put("packageName", packageName);
  		dataHashMap.put("qty", qty);
  		dataHashMap.put("expiredDate", formattedExpDate);
  		dataHashMap.put("termOfUse", productSubcriptionTypeName);
  		dataHashMap.put("activeDate", formattedActiveDate);
  		dataHashMap.put("validUntil", validUntil);
  		dataHashMap.put("paymentMethod", history.getOrder().getPaymentMethod());
  		dataHashMap.put("status", history.getStatus());
  		dataHashMap.put("totalPayment", history.getOrder().getGrandTotal());
  		dataHashMap.put("orderDate", formattedOrderDate);
  		dataHashMap.put("imageUrl", image);
  		dataHashMap.put("ordersId", history.getOrder().getOrdersId());
  		return dataHashMap;
  	}
	
  	private String generateOrderId() {
        String SALTCHARS = "1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 16) { 
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
  	
}