package com.telkom.dtpbe.repositories.controllers;

import java.util.Base64;
import java.util.HashMap;
import java.util.Optional;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.password.Pbkdf2;
import com.telkom.dtpbe.dtos.RequestLogin;
import com.telkom.dtpbe.dtos.UsersDTO;
import com.telkom.dtpbe.models.Users;
import com.telkom.dtpbe.repositories.UsersRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class LoginRepositoriesController {
	
	ModelMapper modelMapper = new ModelMapper();
	@Autowired
	UsersRepository usersRepository;
	
	public UsersDTO convertToDTO(Users users) {
		return modelMapper.map(users, UsersDTO.class);
	}
	// Create a new ContentAdvantageDtp
	@PostMapping("/auth/loginNoJwt")
	public HashMap<String, Object> login(@Valid @RequestBody RequestLogin requestLogin) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	String message;
		boolean isSuccess = false;
		Integer code = null;
		try {
			Optional<Users> users = usersRepository.findByEmailOrMobileNumber(requestLogin.getUsername());
			if(users.isPresent() && Pbkdf2.verifyPassword(requestLogin.getPassword(), users.get().getPassword())) {
				message = "Invalid username or password";
				users.get().setStatus(1);
				usersRepository.save(users.get());
				message = "success";
				isSuccess = true;
				showHashMap.put("data", convertToDTO(users.get()));
			} else if (!users.isPresent()){
				if(isNumeric(requestLogin.getUsername())){
					message = "Phone number isn’t registered yet";
				} else {
					message = "Email isn’t registered yet";
				}
				showHashMap.put("data", null);
			} else {
				message = "Password is incorrect";
				showHashMap.put("data", null);
			} 
			code = 200;
			
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			showHashMap.put("data", null);
		}
		
		showHashMap.put("code", code);
		showHashMap.put("success", isSuccess);
		showHashMap.put("message", message);
    	
    	return showHashMap;
    }
	
	public static boolean isNumeric(String str) { 
		try {  
			Double.parseDouble(str);  
			return true;
		} catch(NumberFormatException e){  
		    return false;  
		}  
	}
}
