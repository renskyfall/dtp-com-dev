package com.telkom.dtpbe.repositories.controllers;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Optional;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telkom.dtpbe.dtos.CartDTO;
import com.telkom.dtpbe.dtos.HistoryLoad;
import com.telkom.dtpbe.dtos.OrderDTO;
import com.telkom.dtpbe.dtos.ProductCheckout;
import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.Cart;
import com.telkom.dtpbe.models.History;
import com.telkom.dtpbe.models.Order;
import com.telkom.dtpbe.models.Product;
import com.telkom.dtpbe.models.ProductPackage;
import com.telkom.dtpbe.models.ProductPackageSubcription;
import com.telkom.dtpbe.models.Users;
import com.telkom.dtpbe.repositories.CartRepository;
import com.telkom.dtpbe.repositories.OrderRepository;
import com.telkom.dtpbe.repositories.ProductPackageSubcriptionRepository;
import com.telkom.dtpbe.repositories.UsersRepository;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class OrderProcessController extends Minio{

	ModelMapper modelMapper = new ModelMapper();
	ObjectMapper objectMapper = new ObjectMapper();
	
	@Autowired
	ProductPackageSubcriptionRepository productPackageSubcriptionRepository;
	
	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	UsersRepository usersRepository;
	
	@Autowired
	CartRepository cartRepository;
	
	public OrderDTO convertToDTO(Order object) {
        return modelMapper.map(object, OrderDTO.class);
	}
	
	public Order convertToEntity(OrderDTO object) {
	    return modelMapper.map(object, Order.class);
	}

	//Create OrderProduct
	@PostMapping("/order/create")
	public HashMap<String, Object> createOrder(@AuthenticationPrincipal UserDetails userDetails, @RequestBody Cart cartData, HttpServletResponse response) throws InvalidEndpointException, InvalidPortException, InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, XmlParserException, IOException, ServerException{
		HashMap<String,Object> result = new HashMap<String,Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		cartData = cartRepository.findById(cartData.getCartId()).orElse(null);
		Optional<Users> userData = usersRepository.findByEmailOrMobileNumber(userDetails.getUsername());
		if(cartData == null) {
			message = "No cart found.";
    		code = 400;
    		result.put("data", null);
		}else {
			try {
				Order orderData = new Order();
				String orderId = "";
				do{
					orderId = generateOrderId();
					orderData = orderRepository.findById(orderId).orElse(null);
				}while(orderData != null);
				
				orderData = new Order();
				orderData.setOrdersId(orderId);
				orderData.setGrandTotal(cartData.getGrandTotal());
				orderData.setDiscount(cartData.getDiscount());


				orderData.setAdminFee(0.0);
				
				orderData.setProducts(cartData.getProducts());
				orderData.setTotalPrice(orderData.getGrandTotal() + orderData.getAdminFee());
				orderData.setIsDeleted(false);
				orderData.setUserId(userData.get());
				orderData.setCreatedBy(userData.get().getUserId());
				orderData.setLastModifiedBy(userData.get().getUserId());
				
				orderData = orderRepository.save(orderData);
				
				result.put("data", convertToDTO(orderData));
	    		message = "Create OrderProduct Success!";
	    		code = 201;
	    		isSuccess = true;
	    	} catch (Exception e) {
				message = e.getMessage();
				result.put("data", null);
				code = 400;
			}
		}
		
		response.setStatus(code);
		result.put("code", code);
        result.put("success", isSuccess);
		result.put("message", message);
		return result;
	}

	
	//show all ProductCheckout
  	@GetMapping("/order/cart")
  	public HashMap<String, Object> showProductCart(@AuthenticationPrincipal UserDetails userDetails, HttpServletResponse response) throws InvalidEndpointException, InvalidPortException, InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, XmlParserException, IOException, ServerException{
  		HashMap<String,Object> result = new HashMap<String,Object>();
  		ArrayList<HashMap<String, Object>> listData = new ArrayList<HashMap<String, Object>>();
  		Double totalPrice = 0.0;
  		String message;
  		boolean isSuccess = false;
  		Integer code = null;
  		Optional<Users> userData = usersRepository.findByEmailOrMobileNumber(userDetails.getUsername());
  		Long userId = userData.get().getUserId();
	  	
  		Optional<Cart> cartData = cartRepository.findCartByUserId(userId);
  		if(cartData.isEmpty()) {
  			message = "No Data Cart Found";
      		code = 400;
  		}else {
  			try {

  				ArrayList<ProductCheckout> productCheckout = objectMapper.readValue(cartData.get().getProducts(), new TypeReference<ArrayList<ProductCheckout>>(){});
  	  			
  				for(ProductCheckout product : productCheckout) {
  	  				HashMap<String,Object> data = showDataProductCheckout(product);
  	  				listData.add(data);
  	  				ProductCheckout tempProduct = (ProductCheckout) data.get("productData");
  	  				Double tempPrice = (Double) tempProduct.getSubTotal();
  	  				totalPrice += tempPrice;
  	  			}
  	  			result.put("totalPrice", totalPrice);
  	      		message = "Read All Success!";
  	      		code = 200;
  	      		isSuccess = true;

  	    		result.put("total", listData.size());
  	    		result.put("cartData", modelMapper.map(cartData.get(), CartDTO.class));
  	    		result.put("data", listData);
  	      	} catch (Exception e) {
  	  			message = e.getMessage();
  	  			code = 400;
  	  		}
  		}
  		
  		response.setStatus(code);
  		result.put("code", code);
        result.put("success", isSuccess);
  		result.put("message", message);
  		return result;
  	}
  	
  	//Add Products to cart
  	@PostMapping("/order/cart")
  	public HashMap<String, Object> addProductCart(@RequestBody ArrayList<ProductCheckout> productCheckout, @AuthenticationPrincipal UserDetails userDetails) throws InvalidEndpointException, InvalidPortException, InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, XmlParserException, IOException, ServerException{
  		HashMap<String,Object> result = new HashMap<String,Object>();
  		ArrayList<HashMap<String, Object>> listData = new ArrayList<HashMap<String, Object>>();
  		Double totalPrice = 0.0;
  		String message;
  		boolean isSuccess = false;
  		Integer code = null;
  		Optional<Users> userData = usersRepository.findByEmailOrMobileNumber(userDetails.getUsername());
  		Long userId = userData.get().getUserId();
  		String listProducts = objectMapper.writeValueAsString(productCheckout);
  		Optional<Cart> cartData = cartRepository.findCartByUserId(userId);
  		
  		//this process will create cart if there's not exist
  		if(cartData.isEmpty()) {
  			Cart cartDataNew = new Cart();
  			cartDataNew.setCreatedBy(userId);
  			cartDataNew.setUserId(userData.get());
  			cartDataNew.setProducts(listProducts);
  			//Create cart 
  			cartRepository.save(cartDataNew);
  			
  			cartData = cartRepository.findCartByUserId(userId);
  		}
  		
		try {
			Cart tempCart = cartData.get();
			ArrayList<ProductCheckout> listProductCheckout = new ArrayList<ProductCheckout>();
			
			for(ProductCheckout product : productCheckout) {
  				HashMap<String,Object> data = showDataProductCheckout(product);
  				listData.add(data);
  				ProductCheckout tempProduct = (ProductCheckout) data.get("productData");
  				listProductCheckout.add(tempProduct);
  				Double tempPrice = (Double) tempProduct.getSubTotal();
  				totalPrice += tempPrice;
  			}
			
			listProducts = objectMapper.writeValueAsString(listProductCheckout);
			
//SET DISCOUNT HARDCODE
			tempCart.setDiscount(0.0);
			
			tempCart.setProducts(listProducts);
			tempCart.setIsDeleted(false);
			tempCart.setGrandTotal(totalPrice);
			tempCart = cartRepository.save(tempCart);
			
  			result.put("totalPrice", totalPrice);
      		message = "Read All Success!";
      		code = 200;
      		isSuccess = true;

    		result.put("total", listData.size());
    		result.put("cartData", modelMapper.map(tempCart, CartDTO.class));
    		result.put("data", listData);
      	} catch (Exception e) {
  			message = e.getMessage();
  			code = 400;
  		}
  		
          
  		result.put("code", code);
        result.put("success", isSuccess);
  		result.put("message", message);
  		result.put("total", listData.size());
  		result.put("data", listData);
  		return result;
  	}
  	
  	@PostMapping("/order/detail/{id}")
  	public HashMap<String, Object> getOrderDetail(@AuthenticationPrincipal UserDetails userDetails, @PathVariable(value = "id") String id, HttpServletResponse response) throws InvalidEndpointException, InvalidPortException, InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, XmlParserException, IOException, ServerException{
  		HashMap<String,Object> result = new HashMap<String,Object>();
  		HashMap<String,Object> orderDetail = new HashMap<String,Object>();
  		ArrayList<HashMap<String, Object>> listData = new ArrayList<HashMap<String, Object>>();
  		String message;
  		boolean isSuccess = false;
  		Integer code = null;
  		Optional<Order> orderData = orderRepository.findById(id);
  		Optional<Users> userData = usersRepository.findByEmailOrMobileNumber(userDetails.getUsername());
  		
  		if(orderData.get().getUserId() != userData.get()) {
  			message = "This user is not the owner of the order";
      		code = 400;
  		}else {
  			try {
  				Order order = orderData.get();
  				
  				DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("dd-LLLL-yyyy, HH:mm");
  				Timestamp expDate = order.getExpiredDate();
  		  		Timestamp activeDate = order.getActiveDate();
  		  		Timestamp orderDate = order.getActiveDate();
  		  		String formattedActiveDate = null;
  		  		String formattedExpDate = null;
  		  		String validUntil = null;
  		  		String formattedOrderDate = dateTimeFormat.format(orderDate.toLocalDateTime());
  				
  				ArrayList<ProductCheckout> productCheckoutList = objectMapper.readValue(order.getProducts(), new TypeReference<ArrayList<ProductCheckout>>(){});
  	  			
  				ProductCheckout productCheckout = productCheckoutList.get(0);
  				
  				for(ProductCheckout product : productCheckoutList) {
  	  				HashMap<String,Object> data = showDataProductCheckout(product);
  	  				listData.add(data);
  	  			}
  				
  				if(expDate != null) {
		  			formattedExpDate = dateTimeFormat.format(expDate.toLocalDateTime());
		  		}
		  		if(!order.getStatus().equals("WAITING_PAYMENT")) {
		  			formattedExpDate = null;
		  		}
		  		if(activeDate != null) {
		  			Calendar calendar = Calendar.getInstance();
		  			calendar.setTimeInMillis(activeDate.getTime());
		  			Integer tempQty = productCheckout.getQty();
		  			Integer days = 30;
		  			Integer totalDays = tempQty * days;
		
		  			calendar.add(Calendar.DATE, totalDays);
		  			Timestamp validUntilTimestamp = new Timestamp(calendar.getTimeInMillis());
		  			formattedActiveDate = dateTimeFormat.format(activeDate.toLocalDateTime());
		  			validUntil = dateTimeFormat.format(validUntilTimestamp.toLocalDateTime());
		  			
		  		}
  				
		  		orderDetail.put("ordersId", order.getOrdersId());
  				orderDetail.put("products", listData);
  				orderDetail.put("invoiceNumber", order.getInvoiceNumber());
  				orderDetail.put("status", order.getStatus());
  				orderDetail.put("totalPrice", order.getTotalPrice());
  				orderDetail.put("grandTotal", order.getGrandTotal());
  				orderDetail.put("adminFee", order.getAdminFee());
  				orderDetail.put("paymentMethod", order.getPaymentMethod());
  				orderDetail.put("expiredDate", formattedExpDate);
  				orderDetail.put("validUntil", validUntil);
  				orderDetail.put("activeDate", formattedActiveDate);
  				orderDetail.put("orderDate", formattedOrderDate);
  				
  				
  	      		message = "Read Data Success!";
  	      		code = 200;
  	      		isSuccess = true;

  	    		result.put("data", orderDetail);
  	      	} catch (Exception e) {
  	  			message = e.getMessage();
  	  			code = 400;
  	  		}
  		}
		
  		
  		
  		response.setStatus(code);
  		result.put("code", code);
        result.put("success", isSuccess);
  		result.put("message", message);
  		return result;
  	}
  	
  	//Show Data Product
  	@SuppressWarnings("deprecation")
  	public HashMap<String,Object>  showDataProductCheckout(ProductCheckout productCheckout) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException{
  		HashMap<String,Object> dataHashMap = new HashMap<String,Object>();
  		Long productPackageSubcriptionId = productCheckout.getProductPackageSubcriptionId();
  		
  		ProductPackageSubcription productPackageSubcription = productPackageSubcriptionRepository.findById(productPackageSubcriptionId).get();
  		Product product = productPackageSubcription.getProductPackage().getProduct();
  		ProductPackage productPackage = productPackageSubcription.getProductPackage();
  		
  		Integer qty = productCheckout.getQty();
  		String packageName = productPackage.getPackageName();
  		String productName = product.getProductName();
  		String productType = product.getProductType().getProductTypeName();
  		String productSubcriptionTypeName = productPackageSubcription.getProductSubcriptionType().getProductSubcriptionTypeName();
  		Double productPrice = productPackageSubcription.getProductPrice();
  		String productCode = productPackage.getProductCode();
  		String image = null;
  		if(product.getImagePath() != null) {
  			image = minio().presignedGetObject(bucketName, product.getImagePath());
  			
  		}
  		productCheckout.setPackageName(packageName);
  		productCheckout.setPrice(productPrice);
  		productCheckout.setProductName(productName);
  		productCheckout.setProductType(productType);
  		productCheckout.setTermOfUse(productSubcriptionTypeName);
  		productCheckout.setSubTotal(qty*productPrice);
  		productCheckout.setProductCode(productCode);
  		dataHashMap.put("productData", productCheckout);
  		dataHashMap.put("imageUrl", image);
  		
  		return dataHashMap;
  	}
	
  	private String generateOrderId() {
        String SALTCHARS = "1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 16) { 
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
  	
}