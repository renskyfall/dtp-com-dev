package com.telkom.dtpbe.repositories.controllers;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ContentAdvantageDtpDTO;
import com.telkom.dtpbe.models.Bank;
import com.telkom.dtpbe.models.ContentAdvantageDtp;
import com.telkom.dtpbe.models.PaymentMethod;
import com.telkom.dtpbe.repositories.BankRepository;
import com.telkom.dtpbe.repositories.PaymentMethodRepository;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class PaymentMethodListController {
ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	PaymentMethodRepository paymentMethodRepository;
	
	@Autowired
	BankRepository bankRepository;
	
	public ContentAdvantageDtpDTO convertToDTO(ContentAdvantageDtp contentAdvantageDtp) {
		return modelMapper.map(contentAdvantageDtp, ContentAdvantageDtpDTO.class);
	}

	public ContentAdvantageDtp convertToEntity(ContentAdvantageDtpDTO contentAdvantageDtpDTO) {
		return modelMapper.map(contentAdvantageDtpDTO, ContentAdvantageDtp.class);
	}
	
	// Get All paymentMethod
	@GetMapping("/paymentMethod/getAll")
	public HashMap<String, Object> getAllPaymentMethod() throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException {
		HashMap<String, Object> mapResult = new HashMap<String, Object>();
		List<HashMap<String, Object>> paymentMethodList = new ArrayList<HashMap<String, Object>>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		try {
			for (PaymentMethod paymentMethod : paymentMethodRepository.findAll()) {
				List<HashMap<String, Object>> bankList = new ArrayList<HashMap<String, Object>>();
				HashMap<String, Object> paymentMethodMap = new HashMap<String, Object>();
				paymentMethodMap.put("methodType", paymentMethod.getMethodType());
				paymentMethodMap.put("methodName", paymentMethod.getMethodName());
				paymentMethodMap.put("description", paymentMethod.getDescription());
				paymentMethodMap.put("banks", bankList);
				
				for (Bank bank : bankRepository.findBankByPaymentMethodId(paymentMethod.getPaymentMethodId())) {
					HashMap<String, Object> bankMap = new HashMap<String, Object>();
					bankMap.put("bankCode", bank.getBankCode());
					bankMap.put("bankName", bank.getBankName());
					bankMap.put("bankImage", bank.getBankImage());
					bankMap.put("paymentCode", bank.getPaymentCode());
					bankMap.put("gateway", bank.getGateway());
					bankList.add(bankMap);
				}
				paymentMethodList.add(paymentMethodMap);
			}
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		} 
		
		mapResult.put("code", code);
		mapResult.put("success", isSuccess);
		mapResult.put("message", message);
		mapResult.put("paymentMethodList", paymentMethodList);

		return mapResult;
	}
}
