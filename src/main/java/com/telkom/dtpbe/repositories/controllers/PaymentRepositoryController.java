package com.telkom.dtpbe.repositories.controllers;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.Constant;
import com.telkom.dtpbe.core.shared.EmptyJson;
import com.telkom.dtpbe.core.shared.Response;
import com.telkom.dtpbe.core.shared.Result;
import com.telkom.dtpbe.dtos.InstructionPayload;
import com.telkom.dtpbe.dtos.InstructionResponse;
import com.telkom.dtpbe.dtos.PayOrderPayload;
import com.telkom.dtpbe.dtos.PayOrderResponse;
import com.telkom.dtpbe.dtos.PaymentInformationResponse;
import com.telkom.dtpbe.dtos.PaymentNotificationPayload;
import com.telkom.dtpbe.dtos.PaymentNotificationResponse;
import com.telkom.dtpbe.interfaces.IPayment;


@RestController
@RequestMapping(value = Constant.ROOT_PATH)
public class PaymentRepositoryController {
    private final static Logger LOGGER = LoggerFactory.getLogger(PaymentRepositoryController.class);

    @Autowired
    private IPayment payment;

    @PostMapping("/order/payment")
    public Response payment(@RequestBody @Valid PayOrderPayload payload,
                             @AuthenticationPrincipal UserDetails userDetails,
                             BindingResult result, HttpServletResponse response) {
        try {

            payload.setUserId(userDetails.getUsername());
            Future<Result<PayOrderResponse, String>> resultFuture = payment.payment(payload);
            if (resultFuture.get().isError()) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return new Response(HttpStatus.BAD_REQUEST.value(),
                        false, new EmptyJson(), resultFuture.get().getError());
            }

            return new Response(HttpStatus.OK.value(),
                    true, resultFuture.get().getData(),
                    "success pay");
        } catch (Exception e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    false,
                    new EmptyJson(),
                    e.getMessage());
        }
    }
    
    @PostMapping("/payment/notification")
    public PaymentNotificationResponse handleNotification(@RequestBody @Valid PaymentNotificationPayload reqPayload,
                          HttpServletResponse response) {
        try {
            Future<Result<PaymentNotificationResponse, String>> resultFuture = payment.receivePaymentNotification(reqPayload);

            return resultFuture.get().getData();
        } catch (InterruptedException | ExecutionException e) {
            return new PaymentNotificationResponse("Failed", "", "");
        }
    }

    @GetMapping("/payment/information")
    public Response getPaymentInformation(@Param(value = "ordersId") String ordersId,
                             @AuthenticationPrincipal UserDetails userDetails,
                             HttpServletResponse response) {
        try {
        	String userId = userDetails.getUsername();
            Future<Result<PaymentInformationResponse, String>> resultFuture = payment.getPaymentInformation(ordersId, userId);
            if (resultFuture.get().isError()) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return new Response(HttpStatus.BAD_REQUEST.value(),
                        false, new EmptyJson(), resultFuture.get().getError());
            }

            return new Response(HttpStatus.OK.value(),
                    true, resultFuture.get().getData(),
                    "success get Payment Information");
        } catch (Exception e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    false,
                    new EmptyJson(),
                    e.getMessage());
        }
    }
    
    @GetMapping("/payment/instruction/{code}")
    public Response getDetailProduct(@PathVariable String code, HttpServletResponse response) {
        try {

            Future<Result<InstructionResponse, String>> resultFuture = payment.getPaymentInstruction(code);
            if (resultFuture.get().isError()) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return new Response(HttpStatus.BAD_REQUEST.value(),
                        false, new EmptyJson(), resultFuture.get().getError());
            }

            return new Response(HttpStatus.OK.value(),
                    true, resultFuture.get().getData(),
                    "success get instruction");
        } catch (Exception e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    false,
                    new EmptyJson(),
                    e.getMessage());
        }
    }

    @PostMapping("/payment/instruction")
    public Response addInstruction(@RequestBody @Valid List<InstructionPayload> reqPayload,
                                                          HttpServletResponse response) {
        try {
            Future<Result<String, String>> resultFuture = payment.addPaymentInstruction(reqPayload);
            if (resultFuture.get().isError()) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return new Response(HttpStatus.BAD_REQUEST.value(),
                        false, new EmptyJson(), resultFuture.get().getError());
            }

            return new Response(HttpStatus.OK.value(),
                    true, resultFuture.get().getData(),
                    "success save instruction");
        } catch (InterruptedException | ExecutionException e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    false,
                    new EmptyJson(),
                    e.getMessage());
        }
    }
}
