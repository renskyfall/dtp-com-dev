package com.telkom.dtpbe.repositories.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ProductPackageDTO;
import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.Product;
import com.telkom.dtpbe.models.ProductPackage;
import com.telkom.dtpbe.models.ProductPackageSubcription;
import com.telkom.dtpbe.repositories.ProductPackageRepository;
import com.telkom.dtpbe.repositories.ProductPackageSubcriptionRepository;
import com.telkom.dtpbe.repositories.ProductRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ProductPackageRepositoryController extends Minio{
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ProductPackageRepository productPackageRepository;
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	ProductPackageSubcriptionRepository productPackageSubcriptionRepository;
	
	public ProductPackageDTO convertToDTO(ProductPackage productPackage) {
		return modelMapper.map(productPackage, ProductPackageDTO.class);
	}

	public ProductPackage convertToEntity(ProductPackageDTO productPackageDTO) {
		return modelMapper.map(productPackageDTO, ProductPackage.class);
	}
	
	// Get ProductPackage By Product ID
		@GetMapping("/product/package/getAll")
		public HashMap<String, Object> getAllProductPackagePrice() {
			HashMap<String, Object> mapResult = new HashMap<String, Object>();
			List<HashMap<String, Object>> listProductPackagePrices = new ArrayList<HashMap<String, Object>>();
			String message = null;
			boolean isSuccess = false;
			Integer code = null;
			
			try {
				for (ProductPackage p : productPackageRepository.findAll()) {
					ProductPackageDTO productPackageDTO = convertToDTO(p);
					List<HashMap<String, Object>> listProductPackageSubcriptionResult = new ArrayList<HashMap<String, Object>>();
					HashMap<String, Object> mapTemp = new HashMap<String, Object>();
					mapTemp.put("id",productPackageDTO.getProductPackageId());
					mapTemp.put("code",productPackageDTO.getProductCode());
					mapTemp.put("name",productPackageDTO.getPackageName());
					mapTemp.put("description",productPackageDTO.getDescription());
					mapTemp.put("additionalInfo", productPackageDTO.getAdditionalInfo());
					mapTemp.put("price",listProductPackageSubcriptionResult);
					//get Product Package Subcription List
					for(ProductPackageSubcription productPackageSubcription :  productPackageSubcriptionRepository.findProductPackageSubcriptionByProductPackageId(productPackageDTO.getProductPackageId())) {
						HashMap<String, Object> mapProductPackageSubcription = new HashMap<String, Object>();
						mapProductPackageSubcription.put("productSubcriptionType", productPackageSubcription.getProductSubcriptionType().getProductSubcriptionTypeName());
						mapProductPackageSubcription.put("productPackagePrice", productPackageSubcription.getProductPrice());
						mapProductPackageSubcription.put("properties", productPackageSubcription.getProperties());
						mapProductPackageSubcription.put("productPackagePrice", productPackageSubcription.getProductPrice());
						listProductPackageSubcriptionResult.add(mapProductPackageSubcription);
					}
					listProductPackagePrices.add(mapTemp);
				}
				
				message = "success";
				code = 200;
				isSuccess = true;
			} catch (Exception e) {
				message = e.getMessage();
				code = 400;
			}
			
			mapResult.put("code", code);
			mapResult.put("success", isSuccess);
			mapResult.put("message", message);
			mapResult.put("total", listProductPackagePrices.size());
			mapResult.put("data", listProductPackagePrices);

			return mapResult;
		}
	
	// Get ProductPackage By Product ID
	@GetMapping("/product/package/detailPackage")
	public HashMap<String, Object> getProductPackageByProductId(@RequestParam(value = "productId") Long productId) {
		HashMap<String, Object> mapResult = new HashMap<String, Object>();
		List<HashMap<String, Object>> listProductPackagePrices = new ArrayList<HashMap<String, Object>>();
		String message = null;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for (ProductPackage p : productPackageRepository.findProductPackageByProductId(productId)) {
				ProductPackageDTO productPackageDTO = convertToDTO(p);
				List<HashMap<String, Object>> listProductPackageSubcriptionResult = new ArrayList<HashMap<String, Object>>();
				HashMap<String, Object> mapTemp = new HashMap<String, Object>();
				mapTemp.put("id",productPackageDTO.getProductPackageId());
				mapTemp.put("code",productPackageDTO.getProductCode());
				mapTemp.put("name",productPackageDTO.getPackageName());
				mapTemp.put("description",productPackageDTO.getDescription());
				mapTemp.put("additionalInfo", productPackageDTO.getAdditionalInfo());
				mapTemp.put("price",listProductPackageSubcriptionResult);
				//get Product Package Subcription List
				for(ProductPackageSubcription productPackageSubcription :  productPackageSubcriptionRepository.findProductPackageSubcriptionByProductPackageId(productPackageDTO.getProductPackageId())) {
					HashMap<String, Object> mapProductPackageSubcription = new HashMap<String, Object>();
					mapProductPackageSubcription.put("productSubcriptionType", productPackageSubcription.getProductSubcriptionType().getProductSubcriptionTypeName());
					mapProductPackageSubcription.put("productPackagePrice", productPackageSubcription.getProductPrice());
					mapProductPackageSubcription.put("properties", productPackageSubcription.getProperties());
					mapProductPackageSubcription.put("productPackagePrice", productPackageSubcription.getProductPrice());
					listProductPackageSubcriptionResult.add(mapProductPackageSubcription);
				}
				listProductPackagePrices.add(mapTemp);
			}
			
			message = "success";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
		
		mapResult.put("code", code);
		mapResult.put("success", isSuccess);
		mapResult.put("message", message);
		mapResult.put("total", listProductPackagePrices.size());
		mapResult.put("data", listProductPackagePrices);

		return mapResult;
	}
}
