package com.telkom.dtpbe.repositories.controllers;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.Industries;
import com.telkom.dtpbe.models.Product;
import com.telkom.dtpbe.models.ProductType;
import com.telkom.dtpbe.repositories.ProductRepository;
import com.telkom.dtpbe.repositories.ProductTypeRepository;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ProductRepositoryController extends Minio{

	@Autowired
	ProductTypeRepository productTypeRepository;
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	private ProductFilterService service;

	//show all Data
	@GetMapping("/product/showAll")
	public HashMap<String, Object> showAbout() throws InvalidEndpointException, InvalidPortException, InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, XmlParserException, IOException, ServerException{
		HashMap<String,Object> result = new HashMap<String,Object>();
		ArrayList<HashMap<String, Object>> listData = new ArrayList<HashMap<String, Object>>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for(Product product : productRepository.findAll()) {
				listData.add(showDataProduct(product));
			}
    		message = "Read All Success!";
    		code = 200;
    		isSuccess = true;
    	} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
        
		result.put("code", code);
        result.put("success", isSuccess);
		result.put("message", message);
		result.put("total", listData.size());
		result.put("data", listData);
		return result;
	}
	
	//Show Data By ID
	@GetMapping("/product/show/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String,Object> result = new HashMap<String,Object>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Product product = productRepository.findById(id).orElse(null);
			result.put("data", showDataProduct(product));
			message = "Read All Success!";
			code = 200;
			isSuccess = true;
		} catch (Exception e) {
			message = e.getMessage();
			code = 400;
			result.put("data", "");
		}
		
		result.put("code", code);
		result.put("success", isSuccess);
		result.put("message", message);
		return result;
	}
	
	//filterProduct
	@GetMapping("/product/filterProduct")
	public HashMap<String, Object> getProductByFilter(
			@RequestParam(value = "pageNo") Integer pageNo,
			@RequestParam(value = "pageSize") Integer pageSize,
			@RequestParam(value = "solution") String solution,
			@RequestParam(value = "industries") String industries ) throws InvalidEndpointException, InvalidPortException {
		HashMap<String,Object> result = new HashMap<String,Object>();
		ArrayList<HashMap<String, Object>> listData = new ArrayList<HashMap<String, Object>>();
		String message = null;
		boolean isSuccess = false, dataFound = false;
		Integer code = null;
		
		try {
			for(Product data : service.getProductByFilter(pageNo, pageSize, solutionFilter(solution))) {
				if(industries.isEmpty()) {
					listData.add(showDataProduct(data));
					dataFound = true;
				} else {
					for(Industries i : data.getIndustrieses()) {
						 if(i.getIndustriesName().toLowerCase().contains(industries.toLowerCase())) {
							 listData.add(showDataProduct(data));
							 dataFound = true;
						} else {
							listData.isEmpty();
						}
					}
				}
			}
			
			if(dataFound)
				message = "Read All Success!";
			else
				message = "No Data Found!";
			
    		code = 200;
    		isSuccess = true;
    	} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
        
		result.put("code", code);
        result.put("success", isSuccess);
		result.put("message", message);
		result.put("total", listData.size());
		result.put("data", listData);
		return result;
	}
	
	@Service
	public class ProductFilterService {

		public List<Product> getProductByFilter(Integer pageNo, Integer pageSize, ArrayList<Long> solutionId) {
			Pageable paging = PageRequest.of(pageNo, pageSize);

			Slice<Product> pagedResult = productRepository.getFilter(solutionId, paging);

			return pagedResult.getContent();						
		}
	}
	
	//Show Data Product
	@SuppressWarnings("deprecation")
	public HashMap<String,Object>  showDataProduct(Product product) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException{
		HashMap<String,Object> dataHashMap = new HashMap<String,Object>();
		
		String image = null;
		if(product.getImagePath() != null) {
			image = minio().presignedGetObject(bucketName, product.getImagePath());
			
		}
		dataHashMap.put("productId", product.getProductId());
		dataHashMap.put("productName", product.getProductName());
		dataHashMap.put("productType", product.getProductType().getProductTypeName());
		dataHashMap.put("brandName", product.getBrand().getBrandName());
		dataHashMap.put("linkUrl", product.getLinkUrl());
		dataHashMap.put("tagline", product.getTagline());
		dataHashMap.put("description", product.getDescription());
		dataHashMap.put("imageUrl", image);
		
		return dataHashMap;
	}
	
	//SolutionFilter
	public ArrayList<Long> solutionFilter(String solution) {
		ArrayList<Long> solutionId = new ArrayList<Long>();
		
		if(solution.isEmpty()) {
			for(ProductType productType : productTypeRepository.findAll()) {
				solutionId.add(productType.getProductTypeId());
			}
		} else {
			String[] solutionFilter = solution.split(",");
			for (int i = 0; i < solutionFilter.length; i++) {
				for(ProductType productType : productTypeRepository.findAll()) {
					if(productType.getProductTypeName().equalsIgnoreCase(solutionFilter[i])) {
						solutionId.add(productType.getProductTypeId());
					} 
				}
			}
		}
		
		return solutionId;
	}
}