package com.telkom.dtpbe.repositories.controllers;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.ContentLandingPage;
import com.telkom.dtpbe.models.Guide;
import com.telkom.dtpbe.repositories.ContentLandingPageRepository;
import com.telkom.dtpbe.repositories.GuideRepository;

import io.minio.MinioClient;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ShowAndSetHowToUse extends Minio{

	@Autowired
	GuideRepository guideRepository;

	// Show All LandingPage
	@GetMapping("/howToUse/product/{id}")
	public HashMap<String, Object> showCarousel(@PathVariable(value="id") Long id) throws InvalidEndpointException, InvalidPortException, InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, XmlParserException, IOException, ServerException{
		HashMap<String,Object> result = new HashMap<String,Object>();
		HashMap<String,Object> dataHashMap = new HashMap<String,Object>();
		ArrayList<HashMap<String, Object>> listData = new ArrayList<HashMap<String, Object>>();
		MinioClient minioClient = minio();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			Guide dataGuide = guideRepository.findById(id).get();
			ObjectMapper mapper = new ObjectMapper();
			
			GuideContent[] guideContents = mapper.readValue(dataGuide.getGuideContent(), GuideContent[].class);
			ArrayList<GuideContent> newGuideContents = new ArrayList<GuideContent>();
			

			for(GuideContent data : guideContents) {
				String image = null;

				if(data.getFile() != null) {
					image = minioClient.presignedGetObject(bucketName, data.getFile());
				}

				data.file = image;
				newGuideContents.add(data);
				dataHashMap = new HashMap<String,Object>();
			}
			dataHashMap.put("productName", dataGuide.getProductId().getProductName());
			dataHashMap.put("productType", dataGuide.getProductId().getProductType().getProductTypeName());
			dataHashMap.put("brandName", dataGuide.getProductId().getBrand().getBrandName());
			dataHashMap.put("guideContent", newGuideContents);
			dataHashMap.put("guideDescription", dataGuide.getDescription());
			listData.add(dataHashMap);
    		message = "Read All Success!";
    		code = 200;
    		isSuccess = true;
    	} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
        
		result.put("code", code);
        result.put("success", isSuccess);
		result.put("message", message);
		result.put("total", listData.size());
		result.put("data", listData);
		return result;
	}
	
	@Data
	@NoArgsConstructor
	private static class GuideContent {
		private String contentId;
		private String file;
		private String title;
		private String description;
	}
	
}
