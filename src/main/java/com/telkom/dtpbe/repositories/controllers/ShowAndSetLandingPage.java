package com.telkom.dtpbe.repositories.controllers;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.ContentLandingPage;
import com.telkom.dtpbe.repositories.ContentLandingPageRepository;

import io.minio.MinioClient;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ShowAndSetLandingPage extends Minio{

	@Autowired
	ContentLandingPageRepository contentLandingPageRepository;

	// Show All LandingPage
	@GetMapping("/landingPage/carousel")
	public HashMap<String, Object> showCarousel() throws InvalidEndpointException, InvalidPortException, InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, XmlParserException, IOException, ServerException{
		HashMap<String,Object> result = new HashMap<String,Object>();
		HashMap<String,Object> dataHashMap = new HashMap<String,Object>();
		ArrayList<HashMap<String, Object>> listData = new ArrayList<HashMap<String, Object>>();
		MinioClient minioClient = minio();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for(ContentLandingPage data : contentLandingPageRepository.findAll()) {
				String image = null;
				if(data.getImagePath() != null) {
					image = minioClient.presignedGetObject(bucketName, data.getImagePath());
					
				}
				dataHashMap.put("title", data.getTitle());
				dataHashMap.put("description", data.getDescription());
				dataHashMap.put("imageUrl", image);
				
				listData.add(dataHashMap);
				dataHashMap = new HashMap<String,Object>();
			}
    		message = "Read All Success!";
    		code = 200;
    		isSuccess = true;
    	} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
        
		result.put("code", code);
        result.put("success", isSuccess);
		result.put("message", message);
		result.put("total", listData.size());
		result.put("data", listData);
		return result;
	}
	
	
}
