package com.telkom.dtpbe.repositories.controllers;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ShowEnvValue {

	@Value("${spring.datasource.redis.host}")
	private String redisHost;

	@Value("${spring.datasource.redis.port}")
	private String redisPort;

	@Value("${spring.datasource.redis.password}")
	private String redisPassword;
	
	@Value("${spring.mail.username}")
	private String emailUsername;

	@Value("${spring.mail.password}")
	private String emailPassword;
	
	@GetMapping("/showEnv")
	public HashMap<String, Object> showEnv() {
		HashMap<String,Object> dataHashMap = new HashMap<String,Object>();
		dataHashMap.put("datasourceRedisHost", redisHost);
		dataHashMap.put("datasourceRedisPort", redisPort);
		dataHashMap.put("datasourceRedisPassword", redisPassword);
		dataHashMap.put("emailUsername", emailUsername);
		dataHashMap.put("emailPassword", emailPassword);
		return dataHashMap;
	}
}
