package com.telkom.dtpbe.repositories.controllers;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ProductTypeDTO;
import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.Product;
import com.telkom.dtpbe.models.ProductAddOn;
import com.telkom.dtpbe.models.ProductAddOnType;
import com.telkom.dtpbe.models.ProductPackage;
import com.telkom.dtpbe.models.ProductPackageSubcription;
import com.telkom.dtpbe.models.ProductType;
import com.telkom.dtpbe.repositories.ProductAddOnRepository;
import com.telkom.dtpbe.repositories.ProductAddOnTypeRepository;
import com.telkom.dtpbe.repositories.ProductPackageRepository;
import com.telkom.dtpbe.repositories.ProductPackageSubcriptionRepository;
import com.telkom.dtpbe.repositories.ProductRepository;
import com.telkom.dtpbe.repositories.ProductTypeRepository;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class SimulationProductRepositoryController extends Minio{
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ProductTypeRepository productTypeRepository;
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	ProductPackageRepository productPackageRepository;
	
	@Autowired
	ProductAddOnTypeRepository productAddOnTypeRepository;
	
	@Autowired
	ProductAddOnRepository productAddOnRepository;
	
	@Autowired
	ProductPackageSubcriptionRepository productPackageSubcriptionRepository;
	
	public ProductTypeDTO convertToDTO(ProductType productType) {
		return modelMapper.map(productType, ProductTypeDTO.class);
	}

	public ProductType convertToEntity(ProductTypeDTO productTypeDTO) {
		return modelMapper.map(productTypeDTO, ProductType.class);
	}

	// Get All ProductType
	@GetMapping("/product/simulation/getData")
	public HashMap<String, Object> getData(
			@RequestParam(value = "solution") String solution,
			@RequestParam(value = "budget") Integer budget ) 
					throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException {
		HashMap<String, Object> result = new HashMap<String, Object>();
		List<HashMap<String, Object>> listProductTypesResult = new ArrayList<HashMap<String, Object>>();
		List<Product> listProduct = new ArrayList<Product>();
		List<ProductType> listProductType = new ArrayList<ProductType>();
		List<ProductAddOn> listProductAddOn = new ArrayList<ProductAddOn>();
		List<ProductAddOnType> listProductAddOnType = new ArrayList<ProductAddOnType>();
		List<ProductPackage> listProductPackage = new ArrayList<ProductPackage>();
		List<ProductPackageSubcription> listProductPackageSubcription = new ArrayList<ProductPackageSubcription>();
		String message = null;
		boolean isSuccess = false, isValid = false;
		Integer code = null, free = 0;

		try {
			for(ProductPackageSubcription productPackageSubcription : productPackageSubcriptionRepository.findAll()) {
				if(productPackageSubcription.getProductPrice() > free && productPackageSubcription.getProductPrice() <= budget) {
					isValid = true;
					listProductType.add(productPackageSubcription.getProductPackage().getProduct().getProductType());
					listProduct.add(productPackageSubcription.getProductPackage().getProduct());
					listProductPackage.add(productPackageSubcription.getProductPackage());
					listProductPackageSubcription.add(productPackageSubcription);
				} 
				for(ProductAddOnType productAddOnType : productPackageSubcription.getProductPackage().getProductAddOnType()) {
					for(ProductAddOn productAddOn : productAddOnRepository.findProductAddOnByProductAddOnTypeId(productAddOnType.getProductAddOnTypeId())) {
						if(productAddOn.getPrice() <= budget) {
							listProductAddOn.add(productAddOn);
							listProductAddOnType.add(productAddOnType);
						}
					}
				}
			}
			
			if(isValid) {
				for (ProductType productType : solutionFilter(solution)) {
					HashMap<String, Object> mapProductType = new HashMap<String, Object>();
					List<HashMap<String, Object>> listProductResult = new ArrayList<HashMap<String, Object>>();
					
					if(listProductType.contains(productType)){
						mapProductType.put("productTypeId",productType.getProductTypeId());
						mapProductType.put("productTypeName",productType.getProductTypeName());
						mapProductType.put("product", listProductResult);
						mapProductType.put("productTypeName",productType.getProductTypeName());
						mapProductType.put("isActivated",productType.getIsActivated());
						listProductTypesResult.add(mapProductType);
						
						//get Product
						for(Product product :  productRepository.findProductByProductTypeId(productType.getProductTypeId())) {
							HashMap<String, Object> mapProduct = new HashMap<String, Object>();
							List<HashMap<String, Object>> listProductPackageResult = new ArrayList<HashMap<String, Object>>();
							
							if(listProduct.contains(product)){
								String image = null;
								if(product.getImagePath() != null) {
									image = minio().presignedGetObject(bucketName, product.getImagePath());
									
								}
								mapProduct.put("productId", product.getProductId());
								mapProduct.put("productName", product.getProductName());
								mapProduct.put("linkUrl", product.getLinkUrl());
								mapProduct.put("productPackage", listProductPackageResult);
								mapProduct.put("imageUrl", image);
								listProductResult.add(mapProduct);
								
								//get Product Package
								for(ProductPackage productPackage :  productPackageRepository.findProductPackageByProductId(product.getProductId())) {
									HashMap<String, Object> mapProductPackage = new HashMap<String, Object>();
									List<HashMap<String, Object>> listProductPackageSubcriptionResult = new ArrayList<HashMap<String, Object>>();
									List<HashMap<String, Object>> listProductAddOnTypeResult = new ArrayList<HashMap<String, Object>>();
									
									if(listProductPackage.contains(productPackage)) {
										mapProductPackage.put("productPackageId", productPackage.getProductPackageId());
										mapProductPackage.put("productPackageName", productPackage.getPackageName());
										mapProductPackage.put("quantityCustomizable", productPackage.getQuantityCustomizable());
										mapProductPackage.put("productPackageSubcriptionList", listProductPackageSubcriptionResult);
										mapProductPackage.put("productAddOnType", listProductAddOnTypeResult);
										listProductPackageResult.add(mapProductPackage);
										
										//get Product Package Subcription List
										for(ProductPackageSubcription productPackageSubcription :  productPackageSubcriptionRepository.findProductPackageSubcriptionByProductPackageId(productPackage.getProductPackageId())) {
											HashMap<String, Object> mapProductPackageSubcription = new HashMap<String, Object>();
											if(listProductPackageSubcription.contains(productPackageSubcription)){
												mapProductPackageSubcription.put("productSubcriptionType", productPackageSubcription.getProductSubcriptionType().getProductSubcriptionTypeName());
												mapProductPackageSubcription.put("priceInclude", productPackageSubcription.getPriceInclude());
//												mapProductPackageSubcription.put("quantityCustomizable", productPackageSubcription.getQuantityCustomizable());
												mapProductPackageSubcription.put("productPackagePrice", productPackageSubcription.getProductPrice());
												listProductPackageSubcriptionResult.add(mapProductPackageSubcription);
											}
										}
										
										//get Product Add On Type
										for(ProductAddOnType productAddOnType :  productAddOnTypeRepository.findProductAddOnByProductPackageId(productPackage.getProductPackageId())) {
											HashMap<String, Object> mapProductADdOnType = new HashMap<String, Object>();
											List<HashMap<String, Object>> listProductAddOnResult = new ArrayList<HashMap<String, Object>>();
											if(listProductAddOnType.contains(productAddOnType)){
												mapProductADdOnType.put("productAddOnTypeId", productAddOnType.getProductAddOnTypeId());
												mapProductADdOnType.put("name", productAddOnType.getName());
												mapProductADdOnType.put("productAddOn", listProductAddOnResult);
												listProductAddOnTypeResult.add(mapProductADdOnType);
												
												//Get Produt Add On List
												for(ProductAddOn productAddOn :  productAddOnRepository.findProductAddOnByProductAddOnTypeId(productAddOnType.getProductAddOnTypeId())) {
													HashMap<String, Object> mapProductADdOn = new HashMap<String, Object>();
													if(listProductAddOn.contains(productAddOn)){
														mapProductADdOn.put("productAddOnId", productAddOn.getProductAddOnId());
														mapProductADdOn.put("name", productAddOn.getName());
														mapProductADdOn.put("price", productAddOn.getPrice());
														mapProductADdOn.put("quantityCustomizable", productAddOn.getQuantityCustomizable());
														mapProductADdOn.put("additionalPrice", productAddOn.getAdditionalPrice());
														listProductAddOnResult.add(mapProductADdOn);
													}
												}
											}
										}
									}  
								}
							}
						}
					}
				}
				message = "Read All Success!";
			} else {
				message = "No Data Found!";
			}
			
    		code = 200;
    		isSuccess = true;
    	} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
        
		result.put("code", code);
        result.put("success", isSuccess);
		result.put("message", message);
		result.put("total", listProductTypesResult.size());
		result.put("data", listProductTypesResult);
		return result;
	}
	
	//SolutionFilter
	public ArrayList<ProductType> solutionFilter(String solution) {
		ArrayList<ProductType> solutionType = new ArrayList<ProductType>();
		
		if(solution.equalsIgnoreCase("All Solution") || solution.equalsIgnoreCase("Semua Solusi")) {
			for(ProductType productType : productTypeRepository.findAll()) {
				solutionType.add(productType);
			}
		} else {
			for(ProductType productType : productTypeRepository.findAll()) {
				if(productType.getProductTypeName().equalsIgnoreCase(solution)) {
					solutionType.add(productType);
				} 
			}
		}
		
		return solutionType;
	}
}
