package com.telkom.dtpbe.repositories.controllers;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.SubTermsAndConditions;
import com.telkom.dtpbe.models.TermsAndConditions;
import com.telkom.dtpbe.repositories.SubTermsAndConditionsRepository;
import com.telkom.dtpbe.repositories.TermsAndConditionsRepository;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class TermsAndConditionsRepositoryController extends Minio{

	@Autowired
	TermsAndConditionsRepository termsAndConditionsRepository;
	
	@Autowired
	SubTermsAndConditionsRepository subTermsAndConditionsRepository;
	

	//show all Data
	@GetMapping("/termsAndConditions/showAllTitle")
	public HashMap<String, Object> showFAQ() throws InvalidEndpointException, InvalidPortException, InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, XmlParserException, IOException, ServerException{
		HashMap<String,Object> result = new HashMap<String,Object>();
		ArrayList<HashMap<String, Object>> listData = new ArrayList<HashMap<String, Object>>();
		String message;
		boolean isSuccess = false;
		Integer code = null;
		
		try {
			for(TermsAndConditions data : termsAndConditionsRepository.findAll(Sort.by(Sort.Direction.ASC, "termsAndConditionsId"))) {
				
				listData.add(showPerMainTitle(data));
			}
    		message = "Read All Success!";
    		code = 200;
    		isSuccess = true;
    	} catch (Exception e) {
			message = e.getMessage();
			code = 400;
		}
        
		result.put("code", code);
        result.put("success", isSuccess);
		result.put("message", message);
		result.put("total", listData.size());
		result.put("data", listData);
		return result;
	}
	
	//Show PerMainTitle
	public HashMap<String,Object>  showPerMainTitle(TermsAndConditions data) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException{
		HashMap<String,Object> dataHashMap = new HashMap<String,Object>();
		ArrayList<HashMap<String,Object>> listDataHashMap = new ArrayList<HashMap<String,Object>>();
		Long termsAndConditionId = data.getTermsAndConditionsId();
		String termsAndConditionTitle = data.getTitle();
		
		List<SubTermsAndConditions> listSubData = subTermsAndConditionsRepository.getSubTACByTACId(termsAndConditionId);
		
		if(listSubData.isEmpty()) {
			listSubData = null;
		}else {
			for(SubTermsAndConditions tempData : listSubData) {
				listDataHashMap.add(showPerSubTitle(tempData));
			}
		}
		dataHashMap.put("termsAndConditionId", termsAndConditionId);
		dataHashMap.put("termsAndConditionDescription", termsAndConditionTitle);
		dataHashMap.put("subTitle", listDataHashMap);
		
		return dataHashMap;
	}
	
	//Show PerSubTitle
	public HashMap<String,Object>  showPerSubTitle(SubTermsAndConditions data) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException{
		HashMap<String,Object> dataHashMap = new HashMap<String,Object>();
		
		Long termsAndConditionId = data.getSubTermsAndConditionsId();
		String termsAndConditionTitle = data.getTitle();
		
		
		dataHashMap.put("subTermsAndConditionId", termsAndConditionId);
		dataHashMap.put("subTermsAndConditionDescription", termsAndConditionTitle);
		
		return dataHashMap;
	}
	
}