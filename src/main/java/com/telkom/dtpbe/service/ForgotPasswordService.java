package com.telkom.dtpbe.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.mail.MailException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.common.base.Strings;
import com.telkom.dtpbe.Constant;
import com.telkom.dtpbe.core.email.EmailMessage;
import com.telkom.dtpbe.core.email.IEmailService;
import com.telkom.dtpbe.core.shared.Result;
import com.telkom.dtpbe.core.sms.ISMSService;
import com.telkom.dtpbe.core.sms.SMSPayload;
import com.telkom.dtpbe.dtos.ForgotPassword;
import com.telkom.dtpbe.dtos.OTPCode;
import com.telkom.dtpbe.dtos.ResetPassword;
import com.telkom.dtpbe.dtos.UsersDTO;
import com.telkom.dtpbe.interfaces.IForgotPassword;
import com.telkom.dtpbe.models.Users;
import com.telkom.dtpbe.password.Pbkdf2;
import com.telkom.dtpbe.repositories.UsersRepository;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class ForgotPasswordService implements IForgotPassword {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(ForgotPassword.class);
	
	@Value("${twilio.trial-number}")
	private PhoneNumber twilioNumber;
	
	@Value("${twilio.account-sid}")
	private String accountSid;
	
	@Value("${twilio.auth-token}")
	private String authToken;
	
	@Value("${spring.mail.username}")
	private String userName;
	
	@Value("${spring.sms.otp.sender.id}")
	private String smsSenderId;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	private UsersRepository usersRepository;
	
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private ISMSService smsSender;
    
    @Autowired
    private IEmailService emailSender;
	
    public UsersDTO convertToDTO(Users users) {
		return modelMapper.map(users, UsersDTO.class);
	}

	public Users convertToEntity(UsersDTO usersDTO) {
		return modelMapper.map(usersDTO, Users.class);
	}
	
	@Async
    @Override
    public Future<Result<Map<String, String>, String>> forgotPassword(ForgotPassword forgotPassword) {
        try {
            LOGGER.info(forgotPassword.getEmail());

            Optional<Users> userResult = Optional.empty();

            String identifier = "EMAIL";
            if (!Strings.isNullOrEmpty(forgotPassword.getEmail())) {
                userResult = usersRepository.findByEmail(forgotPassword.getEmail());
            } else if (!Strings.isNullOrEmpty(forgotPassword.getMobileNumber())) {
                userResult = usersRepository.findByMobileNumber(forgotPassword.getMobileNumber());
                identifier = "PHONE";
            } else {
                return CompletableFuture.completedFuture(Result.from(null, "email or mobile number is empty"));
            }

            if (!userResult.isPresent()) {
                LOGGER.error(userResult.toString());
                return CompletableFuture.completedFuture(Result.from(null, "userPayload not found"));
            }

            String activationCode = null;

            String username = userResult.get().getEmail();
            if (identifier.equals("EMAIL")) {
                activationCode = UUID.randomUUID().toString().replace("-", "");
            } else {
                activationCode = generateRandomChars(6);
                username = userResult.get().getMobileNumber();
            }
            
            try {
            	String key = "forgot-password:" + activationCode;
                redisTemplate.opsForValue().set(key, username);
                redisTemplate.expire(key, 30, TimeUnit.MINUTES);
            } catch (Exception e) {
            	LOGGER.error(e.getMessage());
			}
             

            LOGGER.info(activationCode);
            
            Map<String, String> result = new HashMap<>();
            if (identifier.equals("EMAIL")) {
                String messageBody = "Silahkan klik link berikut untuk reset password anda: " +
                        forgotPassword.getRedirectURL() + "?activationCode=" + activationCode;
                LOGGER.info(forgotPassword.getEmail());
                List<String> to = new ArrayList<>(Arrays.asList(forgotPassword.getEmail()));
                EmailMessage emailMessage = new EmailMessage.Builder()
                        .setSubject("Forgot Password")
                        .setFrom(userName)
                        .setTo(to)
                        .setMessage(messageBody)
                        .build();
                CompletableFuture.runAsync(() -> {
                    Future<Result<String, String>> futureEmail = emailSender.send(emailMessage);
                    LOGGER.info("response: -- {} --");
                    try {
                        LOGGER.info("response: -- {} --", futureEmail.get().getData());
                        result.put("data", futureEmail.get().getData());
                    } catch (InterruptedException e) {
                        LOGGER.error("sending email error : {}", e.getLocalizedMessage());
                    } catch (ExecutionException e) {
                        LOGGER.error("sending email error : {}", e.getLocalizedMessage());
                    } catch (Exception e) {
                    	LOGGER.error("sending email error : {}", e.getLocalizedMessage());
                    }
                });
                result.put("data", "Please check your email");

            } else {
                String messageBody = "Kode OTP anda adalah: " + activationCode;
                SMSPayload smsPayload = new SMSPayload(
                		smsSenderId,
                        forgotPassword.getMobileNumber(),
                        messageBody
                );
                
                Twilio.init(accountSid, authToken);
        		
                PhoneNumber phoneNumber = new PhoneNumber(forgotPassword.getMobileNumber());
        		Message message = Message
        	            .creator( phoneNumber, twilioNumber, messageBody)
        	            .create();
        		
        		LOGGER.info("response: -- {} --", message);
        		
                CompletableFuture.runAsync(() -> {
                	try {
                		Future<Result<String, String>> futureSms = smsSender.send(smsPayload);
                        LOGGER.info("response: -- {} --", futureSms.get().getData());
                        result.put("data", futureSms.get().getData());
                    } catch (InterruptedException e) {
                        LOGGER.error("sending sms error : {}", e.getLocalizedMessage());
                    } catch (ExecutionException e) {
                        LOGGER.error("sending sms error : {}", e.getLocalizedMessage());
                    } catch (Exception e) {
                    	LOGGER.error("sending sms error : {}", e.getLocalizedMessage());
                    }
                });

                result.put("data", "sms sent");

            }

            return CompletableFuture.completedFuture(
                    Result.from(result, null)
            );
        } catch (MailException e) {
        	LOGGER.info("error message = " +  e.getMessage());
            return CompletableFuture.completedFuture(
                    Result.from(null,
                            e.getMessage()));
        }
    }
 
	@Async
    @Override
    public Future<Result<UsersDTO, String>> resetPassword(ResetPassword resetPassword) {
        try {
        	
            String key = "forgot-password:" + resetPassword.getActivationCode();
            String value = redisTemplate.opsForValue().get(key);
            if (value == null) {
                LOGGER.info("key not found");
                return CompletableFuture.completedFuture(Result.from(null, "Invalid activation code"));
            }

            String username = null;
            if (!Strings.isNullOrEmpty(resetPassword.getEmail())) {
                username = resetPassword.getEmail();
            } else if (!Strings.isNullOrEmpty(resetPassword.getMobileNumber())) {
                username = resetPassword.getMobileNumber();
            }

            if (username == null) {
                return CompletableFuture.completedFuture(Result.from(null, "Email or mobile number is empty"));
            }

            if (!username.equals(value)) {
                return CompletableFuture.completedFuture(Result.from(null, "Invalid activation code"));
            }

            Optional<Users> userResult = usersRepository.findByEmailOrMobileNumber(username);
            if (!userResult.isPresent()) {
                return CompletableFuture.completedFuture(Result.from(null, "userPayload not found"));
            }

            Users user = userResult.get();
            try {
                user.setPassword(Pbkdf2.hashPassword(
                        resetPassword.getNewPassword(),
                        Constant.PASSWORD_SALT_LEN,
                        Constant.PASSWORD_ITERATIONS,
                        Constant.PASSWORD_KEY_LEN));
            } catch (Exception e) {
                CompletableFuture<Result<UsersDTO, String>> future = CompletableFuture.completedFuture(
                        Result.from(null, e.getMessage()));
                return future;
            }
            
            LocalDateTime localNow = LocalDateTime.now();
    		Timestamp dateNow = Timestamp.valueOf(localNow);
    		
            user.setLastModifiedOn(dateNow);
            Users saveResult = usersRepository.save(user);

            redisTemplate.delete(key);

            CompletableFuture<Result<UsersDTO, String>> future = CompletableFuture.completedFuture(
                    Result.from(convertToDTO(saveResult), null)
            );
            return future;
        } catch (Exception e) {
            e.printStackTrace();
            CompletableFuture<Result<UsersDTO, String>> future = CompletableFuture.completedFuture(
                    Result.from(null, "Invalid activation code"));
            return future;
        }
    }

    @Async
    @Override
    public Future<Result<Map<String, String>, String>> verificationCode(OTPCode code) {
        try {

            String key = "forgot-password:" + code.getCode();
            String value = redisTemplate.opsForValue().get(key);
            if (value == null) {
                LOGGER.info("email/mobile not found");
                return CompletableFuture.completedFuture(Result.from(null, "Invalid activation code"));
            }

            Map<String, String> result = new HashMap<>();
            result.put("key", value);
            return CompletableFuture.completedFuture(
                    Result.from(result, null)
            );
        } catch (MailException e) {
            return CompletableFuture.completedFuture(
                    Result.from(null,
                            e.getMessage()));
        }
    }
    
    private static String generateRandomChars(Integer length) {
        String chars = "123456789";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(chars.charAt(random.nextInt(chars.length())));
        }

        return sb.toString();
    }
}