package com.telkom.dtpbe.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.telkom.dtpbe.Constant;
import com.telkom.dtpbe.Helper;
import com.telkom.dtpbe.core.mps.IMPSService;
import com.telkom.dtpbe.core.mps.PayloadRequestInvoice;
import com.telkom.dtpbe.core.mps.PayloadRequestVA;
import com.telkom.dtpbe.core.mps.PayloadResponseVA;
import com.telkom.dtpbe.core.shared.Result;
import com.telkom.dtpbe.core.umeetme.IOrderUmeetme;
import com.telkom.dtpbe.core.umeetme.OrderProduct;
import com.telkom.dtpbe.core.umeetme.OrderUmeetmePayload;
import com.telkom.dtpbe.core.umeetme.OrderUmeetmeResponse;
import com.telkom.dtpbe.core.umeetme.OrderUmeetmeService;
import com.telkom.dtpbe.core.umeetme.PayloadLogin;
import com.telkom.dtpbe.core.umeetme.PayloadPaymentNotification;
import com.telkom.dtpbe.core.umeetme.ProductPayload;
import com.telkom.dtpbe.core.umeetme.ResponseLogin;
import com.telkom.dtpbe.core.umeetme.ResponsePaymentNotification;
import com.telkom.dtpbe.core.umeetme.UserPayload;
import com.telkom.dtpbe.core.umeetme.UserVerificationPayload;
import com.telkom.dtpbe.core.umeetme.UserVerificationResponse;
import com.telkom.dtpbe.dtos.InstructionPayload;
import com.telkom.dtpbe.dtos.InstructionPayload.Method;
import com.telkom.dtpbe.dtos.InstructionResponse;
import com.telkom.dtpbe.dtos.PayOrderPayload;
import com.telkom.dtpbe.dtos.PayOrderResponse;
import com.telkom.dtpbe.dtos.PaymentInformationResponse;
import com.telkom.dtpbe.dtos.PaymentNotificationPayload;
import com.telkom.dtpbe.dtos.PaymentNotificationResponse;
import com.telkom.dtpbe.dtos.ProductCheckout;
import com.telkom.dtpbe.interfaces.IPayment;
import com.telkom.dtpbe.models.Cart;
import com.telkom.dtpbe.models.History;
import com.telkom.dtpbe.models.Instruction;
import com.telkom.dtpbe.models.Order;
import com.telkom.dtpbe.models.Users;
import com.telkom.dtpbe.repositories.CartRepository;
import com.telkom.dtpbe.repositories.HistoryRepository;
import com.telkom.dtpbe.repositories.InstructionRepository;
import com.telkom.dtpbe.repositories.OrderRepository;
import com.telkom.dtpbe.repositories.UsersRepository;

@Service
public class PaymentService implements IPayment {

    private final static Logger LOGGER = LoggerFactory.getLogger(PaymentService.class);
    
    ObjectMapper objectMapper = new ObjectMapper();
    
    @Autowired
    private UsersRepository usersRepository;
    
    @Autowired
    private InstructionRepository instructionRepository;
    
    @Autowired
    private OrderRepository orderRepository;
    
    @Autowired
    private HistoryRepository historyRepository;
    
	@Autowired
	CartRepository cartRepository;

    @Autowired
    private IMPSService mpsService;
    
    @Autowired
    private IOrderUmeetme orderUmeetmeService;

    @Autowired
    private Environment env;
  
    @Async
    @Override
    public Future<Result<PayOrderResponse, String>> payment(PayOrderPayload payload) {
    try {
        Optional<Users> usersOptional = usersRepository.findByEmailOrMobileNumber(payload.getUserId());
        if (!usersOptional.isPresent()) {
            return CompletableFuture.completedFuture(
                    Result.from(null, "Users not found"));
        }
        Users users = usersOptional.get();

        Optional<Order> optionalOrder = orderRepository.findByOrderIdAndUsersId(payload.getOrdersId(), users.getUserId());
        if (!optionalOrder.isPresent()) {
            return CompletableFuture.completedFuture(
                    Result.from(null, "Order not found"));
        }
        
        Order order = optionalOrder.get();
        if (!order.getUserId().getUserId().equals(users.getUserId())) {
            return CompletableFuture.completedFuture(
                    Result.from(null, "Order not found"));
        }

        if (!Strings.isNullOrEmpty(order.getInvoiceNumber())) {
            return CompletableFuture.completedFuture(
                    Result.from(null, "Invoice sudah digenerate"));
        }

        if (!Strings.isNullOrEmpty(order.getVaNumber())) {
            return CompletableFuture.completedFuture(
                    Result.from(null, "VA number has been generated"));
        }
        
        //DELETE CART
        Optional<Cart> cartData = cartRepository.findCartByUserId(users.getUserId());
        cartRepository.delete(cartData.get());
        
        PayloadRequestInvoice payloadRequestInvoice = new PayloadRequestInvoice();
        payloadRequestInvoice.setOrderId(String.valueOf(order.getOrdersId()));

        Double grandTotal = order.getGrandTotal() - order.getDiscount();
        payloadRequestInvoice.setAmount(grandTotal.toString());

        PayloadRequestInvoice.OrderInformation orderInformation = new PayloadRequestInvoice.OrderInformation();
        orderInformation.setAmount(payloadRequestInvoice.getAmount());
        orderInformation.setEmail(users.getEmail());
        orderInformation.setNama(users.getFullName());
        String mobileNumber = users.getMobileNumber();
        String temp = mobileNumber.substring(3, mobileNumber.length());
        mobileNumber = temp;
        orderInformation.setNomorHp(mobileNumber);
        orderInformation.setProductName("Umeetme");
        orderInformation.setQuantity("1");
        payloadRequestInvoice.setOrderInformation(orderInformation);

        PayloadRequestInvoice.AdditionalInformation additionalInformation = new PayloadRequestInvoice.AdditionalInformation();
        additionalInformation.setEmailNotif(users.getEmail());
        payloadRequestInvoice.setAdditionalInformation(additionalInformation);

        Future<Result<String, String>> resultInvoice = mpsService.generateInvoice(payloadRequestInvoice);

        order.setInvoiceNumber(resultInvoice.get().getData());
        order.setStatus(Constant.masterOrderStatus.WAITING_PAYMENT.name());
        orderRepository.save(order);
        
        PayOrderResponse responsePayOrder = new PayOrderResponse();

        if (payload.getPaymentMethodCode().equals(env.getProperty("LINKAJA_METHOD_CODE"))) {
            responsePayOrder.setIsRedirect(true);
            responsePayOrder.setTotalAmount(order.getGrandTotal());
            responsePayOrder.setRedirectUrl(String.format("%s?invoiceId=%s&productCode=%s&merchantId=%s&gateway=%s",
                    env.getProperty("LINKAJA_REDIRECT_URL"),
                    order.getInvoiceNumber(),
                    payload.getPaymentMethodCode(),
                    env.getProperty("MPS_MERCHANT_ID"),
                    payload.getPaymentGatewayId()
            ));

        } else {
            PayloadRequestVA payloadRequestVA = new PayloadRequestVA();
            payloadRequestVA.setInvoiceId(resultInvoice.get().getData());
            payloadRequestVA.setProductCode(payload.getPaymentMethodCode());
            payloadRequestVA.setGateway(payload.getPaymentGatewayId());
            Future<Result<PayloadResponseVA, String>> resultReqVA = mpsService.requestVA(payloadRequestVA);
            if (resultReqVA.get().getError() != null) {
                LOGGER.error(resultReqVA.get().getError());
                return CompletableFuture.completedFuture(
                        Result.from(null, "Error when generate VA from MPS"));
            }
            PayloadResponseVA payloadResponseVA = resultReqVA.get().getData();

            SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            isoFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
            Date expiredDate = isoFormat.parse(payloadResponseVA.getExpired());
            Timestamp timestamp = new Timestamp(expiredDate.getTime());
            LOGGER.info(String.valueOf(timestamp));
            LOGGER.info(String.valueOf(expiredDate));
            order.setVaNumber(payloadResponseVA.getVaNumber());
            order.setExpiredDate(timestamp);
            order.setAdminFee(payloadResponseVA.getFee());

            responsePayOrder.setIsRedirect(false);
            responsePayOrder.setAmount(payloadResponseVA.getAmount());
            responsePayOrder.setDescription(payloadResponseVA.getDescription());
            responsePayOrder.setPaymentExpired(Helper.convertToAsiaJakarta(expiredDate));
            responsePayOrder.setVaNumber(payloadResponseVA.getVaNumber());
            responsePayOrder.setTotalAmount(payloadResponseVA.getTotalAmount());
            responsePayOrder.setFee(payloadResponseVA.getFee());
            order.setGrandTotal(payloadResponseVA.getTotalAmount());
        }

        order.setPaymentMethod(payload.getPaymentMethodCode());
        order.setPaymentMethodName(payload.getPaymentMethodName());
        
        
        //Login Umeetme
        PayloadLogin login = new PayloadLogin();
        login.setUsername("+628123123123");
        login.setPassword("dtpdtp");
        login.setRoom("room3");
        login.setAccessTokenExpired(10000);
        login.setDomainChannel("setkab");
        login.setAppType("WEB");
        
        Future<Result<ResponseLogin, String>> resultLogin = orderUmeetmeService.login(login);
        ResponseLogin responseLogin = resultLogin.get().getData();
       
        //OrderProduct Umeetme
        ArrayList<ProductCheckout> productCheckoutList = objectMapper.readValue(order.getProducts(), new TypeReference<ArrayList<ProductCheckout>>(){});
        ArrayList<ProductPayload> productPayloadList = new ArrayList<ProductPayload>();
        for(ProductCheckout productCheckout : productCheckoutList) {
        	ProductPayload productPayload = new ProductPayload();
        	if(productCheckout.getTermOfUse().toUpperCase().contains("DAILY")){
        		productPayload.setType("DAILY");
        	} else if (productCheckout.getTermOfUse().toUpperCase().contains("WEEK")) {
        		productPayload.setType("WEEKLY");
        	} else if (productCheckout.getTermOfUse().toUpperCase().contains("MONTH")) {
        		productPayload.setType("MONTHLY");
        	}
        	
            productPayload.setCode(productCheckout.getProductCode());
            productPayload.setPrice(productCheckout.getPrice());
            productPayload.setQty(productCheckout.getQty());
            productPayload.setTotalPrice(productCheckout.getSubTotal());
            productPayloadList.add(productPayload);
        }
        //Body Order
        OrderProduct orderProduct = new OrderProduct();
        orderProduct.setInvoiceNumber(order.getInvoiceNumber());
        orderProduct.setStatus(order.getStatus());
        orderProduct.setPaymentMethod(order.getPaymentMethod());
        orderProduct.setVaNumber(order.getVaNumber());
        orderProduct.setTotal(order.getTotalPrice());
        orderProduct.setDiscount(order.getDiscount());
        orderProduct.setAdminFee(order.getAdminFee());
        orderProduct.setGrandTotal(order.getGrandTotal());
        orderProduct.setOrderDate(order.getOrderDate());
        orderProduct.setExpiredDate(order.getExpiredDate());
        orderProduct.setActiveDate(order.getActiveDate());
        orderProduct.setDueDate(order.getDueDate());
        orderProduct.setProducts(productPayloadList);
        //Body UserPayload
        UserPayload userPayload = new UserPayload();
        userPayload.setEmail(users.getEmail());
        userPayload.setMobileNumber(users.getMobileNumber());
        //Body
        OrderUmeetmePayload orderUmeetmePayload = new OrderUmeetmePayload();
        orderUmeetmePayload.setAccessToken(responseLogin.getData().getAccessToken());
        orderUmeetmePayload.setOrder(orderProduct);	
        orderUmeetmePayload.setUser(userPayload);
        
        //Response
        Future<Result<OrderUmeetmeResponse, String>> resultOrder = orderUmeetmeService.order(orderUmeetmePayload);
        if (resultOrder.get().getError() != null) {
            LOGGER.error(resultOrder.get().getError());
            return CompletableFuture.completedFuture(
                    Result.from(null, "Error when Create Order Umeetme"));
        }
        
        OrderUmeetmeResponse orderUmeetmeResponse = resultOrder.get().getData();
        order.setOrdersProductId(orderUmeetmeResponse.getData().getOrderId());
        order = orderRepository.save(order);
        
        try {
        	if(orderUmeetmeResponse.getData().getVerifyUserToken() != null) {
        		UserVerificationPayload userVerificationPayload = new UserVerificationPayload();
            	userVerificationPayload.setFullname(users.getFullName());
            	userVerificationPayload.setPassword(users.getPassword());
            	userVerificationPayload.setVerificationUserToken(orderUmeetmeResponse.getData().getVerifyUserToken());
            	Future<Result<UserVerificationResponse, String>> resultVerificationUser = orderUmeetmeService.userVerification(userVerificationPayload);
        	}
        } catch (Exception e) {
        	LOGGER.error(e.getMessage());
		}
        
        responsePayOrder.setAccessToken(responseLogin.getData().getAccessToken());
        responsePayOrder.setVerifyUserToken(orderUmeetmeResponse.getData().getVerifyUserToken());
        responsePayOrder.setOrdersId(order.getOrdersId());
        responsePayOrder.setOrderDate(Helper.convertToAsiaJakarta(order.getOrderDate()));
        return CompletableFuture.completedFuture(
                Result.from(responsePayOrder, null));

    } catch (Exception e) {
        e.printStackTrace();
        return CompletableFuture.completedFuture(
                Result.from(null, "Cannot pay order"));
    }
}
    
    @Async
    @Override
    public Future<Result<PaymentNotificationResponse, String>> receivePaymentNotification(PaymentNotificationPayload payload) {
        try {

            Optional<Order> optionalOrder = orderRepository.findByOrderIdAndInvoiceId(payload.getOrderId(), payload.getInvoiceId());
            if (!optionalOrder.isPresent()) {
                return CompletableFuture.completedFuture(Result.from(
                        new PaymentNotificationResponse("Failed", "", "Order not found"),
                        null
                ));
            }

            String status = null;
            if (payload.getStatus().equals("PAID")) {
                status = Constant.masterOrderStatus.PAID.name();
            } else if (payload.getStatus().equals("EXPIRED")) {
                status = Constant.masterOrderStatus.CANCEL.name();
            } else if (payload.getStatus().equals("UNPAID")) {
                status = Constant.masterOrderStatus.WAITING_PAYMENT.name();
            } else {
                return CompletableFuture.completedFuture(Result.from(
                        new PaymentNotificationResponse("Failed", "", "Invalid status"),
                        null
                ));
            }

            Order order = optionalOrder.get();
            LocalDateTime localNow = LocalDateTime.now();
        	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        	Timestamp dateNow = Timestamp.valueOf(formatter.format(localNow));
            order.setStatus(status);
            order.setActiveDate(dateNow);
            order.setLastModifiedOn(dateNow);
            orderRepository.save(order);
            
            //SAVING TO HISTORY
            History history = new History();
            history.setOrder(order);
            history.setStatus(order.getStatus());
            history.setCreatedBy(order.getUserId().getUserId());
            history.setLastModifiedBy(order.getUserId().getUserId());
            history.setIsDeleted(false);
            historyRepository.save(history);
            
            //Login Umeetme
            PayloadLogin login = new PayloadLogin();
            login.setUsername("+628123123123");
            login.setPassword("dtpdtp");
            login.setRoom("room3");
            login.setAccessTokenExpired(10000);
            login.setDomainChannel("setkab");
            login.setAppType("WEB");
            
            Future<Result<ResponseLogin, String>> resultLogin = orderUmeetmeService.login(login);
            ResponseLogin responseLogin = resultLogin.get().getData();
            
            //Payment Notification Umeetme
            PayloadPaymentNotification payloadPaymentNotification = new PayloadPaymentNotification();
            
            payloadPaymentNotification.setAccessToken(responseLogin.getData().getAccessToken());
            payloadPaymentNotification.setAmount(payload.getAmount());
            payloadPaymentNotification.setExpiredDate(payload.getExpiredDate());
            payloadPaymentNotification.setInvoiceId(payload.getInvoiceId());
            boolean isVaStatic = false;
            if (order.getPaymentMethod().equals(env.getProperty("LINKAJA_METHOD_CODE"))) {
            	isVaStatic = true;
            }
            payloadPaymentNotification.setIsVaStatic(String.valueOf(isVaStatic));
            payloadPaymentNotification.setMerchantId(payload.getMerchantId());
            payloadPaymentNotification.setOrderId(order.getOrdersProductId());
            payloadPaymentNotification.setStatus(payload.getStatus());
            Future<Result<ResponsePaymentNotification, String>> resultPaymentNotification = orderUmeetmeService.paymentNotification(payloadPaymentNotification);
            
            return CompletableFuture.completedFuture(Result.from(
                    new PaymentNotificationResponse("Success", order.getInvoiceNumber(), "Success"),
                    null
            ));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            return CompletableFuture.completedFuture(Result.from(
                    new PaymentNotificationResponse("Failed", "", e.getMessage()),
                    null
            ));
        }
    }
    
    @Async
    @Override
    public Future<Result<PaymentInformationResponse, String>> getPaymentInformation(String OrdersId, String userId) {
    	
        try {
            Optional<Users> usersOptional = usersRepository.findByEmailOrMobileNumber(userId);
            if (!usersOptional.isPresent()) {
                return CompletableFuture.completedFuture(
                        Result.from(null, "Users not found"));
            }
            Users users = usersOptional.get();

            Optional<Order> optionalOrder = orderRepository.findByOrderIdAndUsersId(OrdersId, users.getUserId());
            if (!optionalOrder.isPresent()) {
                return CompletableFuture.completedFuture(
                        Result.from(null, "Order not found"));
            }
            
            Order order = optionalOrder.get();
            if (!order.getUserId().getUserId().equals(users.getUserId())) {
                return CompletableFuture.completedFuture(
                        Result.from(null, "Order not found"));
            }

            if (Strings.isNullOrEmpty(order.getInvoiceNumber())) {
                return CompletableFuture.completedFuture(
                        Result.from(null, "Invoice has not been generated"));
            }

            if (Strings.isNullOrEmpty(order.getVaNumber())) {
                return CompletableFuture.completedFuture(
                        Result.from(null, "VA number has not been generated"));
            }
            
            if(order.getStatus().isEmpty() || order.getStatus().isBlank()) {
           	 return CompletableFuture.completedFuture(
                        Result.from(null, "Status is null"));
            }
            
            if(order.getStatus().equals(Constant.masterOrderStatus.PAID.name())) {
            	 return CompletableFuture.completedFuture(
                         Result.from(null, "Order has been paid"));
            }
            
            if(order.getStatus().equals(Constant.masterOrderStatus.CANCEL.name())) {
           	 return CompletableFuture.completedFuture(
                        Result.from(null, "Order has been canceled"));
            }
            
            PaymentInformationResponse paymentInformationResponse = new PaymentInformationResponse();
            paymentInformationResponse.setOrderId(optionalOrder.get().getOrdersId());
            paymentInformationResponse.setVaNumber(optionalOrder.get().getVaNumber());
            paymentInformationResponse.setPaymentExpired(optionalOrder.get().getExpiredDate());
            paymentInformationResponse.setTotalAmount(optionalOrder.get().getGrandTotal());
            
            Optional<Instruction> optionalInstruction = instructionRepository.findByMethodCode(optionalOrder.get().getPaymentMethod());
            if (!optionalInstruction.isPresent()) {
                return CompletableFuture.completedFuture(Result.from(null,"Data not found"));
            }
            InstructionResponse response = new InstructionResponse();
            ArrayList<Method> methods = objectMapper.readValue(optionalInstruction.get().getMethods(), new TypeReference<ArrayList<Method>>(){});
            response.setBankCode(optionalInstruction.get().getBankCode());
            response.setBankName(optionalInstruction.get().getBankName());
            response.setBankImageUrl(optionalInstruction.get().getBankImageUrl());
            response.setPaymentMethod(optionalInstruction.get().getPaymentMethod());
            response.setMethods(methods);
            paymentInformationResponse.setInstruction(response);

            return CompletableFuture.completedFuture(
                    Result.from(paymentInformationResponse, null));

        } catch (Exception e) {
            e.printStackTrace();
            return CompletableFuture.completedFuture(
                    Result.from(null, "Error get data"));
        }
    }
    
    @Async
    @Override
    public Future<Result<InstructionResponse, String>> getPaymentInstruction(String paymentCode) {
        try {
            Optional<Instruction> optionalInstruction = instructionRepository.findByMethodCode(paymentCode);
            if (!optionalInstruction.isPresent()) {
                return CompletableFuture.completedFuture(Result.from(null,"Data not found"));
            }
            InstructionResponse response = new InstructionResponse();
            ArrayList<Method> methods = objectMapper.readValue(optionalInstruction.get().getMethods(), new TypeReference<ArrayList<Method>>(){});
            response.setBankCode(optionalInstruction.get().getBankCode());
            response.setBankName(optionalInstruction.get().getBankName());
            response.setBankImageUrl(optionalInstruction.get().getBankImageUrl());
            response.setPaymentMethod(optionalInstruction.get().getPaymentMethod());
            response.setMethods(methods);
            return CompletableFuture.completedFuture(Result.from(response,null));

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            return CompletableFuture.completedFuture(Result.from(null,"Error get data"));
        }
    }

    @Override
    public Future<Result<String, String>> addPaymentInstruction(List<InstructionPayload> payload) {
        try {	
        	LocalDateTime localNow = LocalDateTime.now();
    		Timestamp dateNow = Timestamp.valueOf(localNow);
            for (InstructionPayload instructionPayload: payload) {
                Optional<Instruction> optionalInstruction = instructionRepository.findByMethodCode(instructionPayload.getPaymentMethod());
                if (optionalInstruction.isPresent()) {
                	optionalInstruction.get().setCreatedOn(optionalInstruction.get().getCreatedOn());
                	instructionRepository.save(optionalInstruction.get());
                } else {
                	String listMethods = objectMapper.writeValueAsString(instructionPayload.getMethods());
                	Instruction instruction = new Instruction();
                	instruction.setBankCode(instructionPayload.getBankCode());
                	instruction.setBankName(instructionPayload.getBankName());
                	instruction.setPaymentMethod(instructionPayload.getPaymentMethod());
                	instruction.setBankImageUrl(instructionPayload.getBankImageUrl());
                	instruction.setMethods(listMethods);
                    instruction.setCreatedOn(dateNow);
                    instructionRepository.save(instruction);
                }
            }

            return CompletableFuture.completedFuture(Result.from("success",null));

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            return CompletableFuture.completedFuture(Result.from(null,"Error save data"));
        }
    }
    
    private String randomString() {
        String SALTCHARS = "1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 15) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();

    }
}
