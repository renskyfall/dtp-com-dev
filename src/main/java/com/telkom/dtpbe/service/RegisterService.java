package com.telkom.dtpbe.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.mail.MailException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.common.base.Strings;
import com.telkom.dtpbe.Constant;
import com.telkom.dtpbe.core.shared.Result;
import com.telkom.dtpbe.core.sms.ISMSService;
import com.telkom.dtpbe.core.sms.SMSPayload;
import com.telkom.dtpbe.dtos.CreateAccount;
import com.telkom.dtpbe.dtos.OTPCode;
import com.telkom.dtpbe.dtos.Register;
import com.telkom.dtpbe.dtos.UsersDTO;
import com.telkom.dtpbe.interfaces.IRegister;
import com.telkom.dtpbe.models.Users;
import com.telkom.dtpbe.modules.auth.dtos.RequestLogin;
import com.telkom.dtpbe.modules.auth.dtos.ResponseLogin;
import com.telkom.dtpbe.modules.auth.usecase.IAuthUsecase;
import com.telkom.dtpbe.password.Pbkdf2;
import com.telkom.dtpbe.repositories.UsersRepository;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class RegisterService implements IRegister {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(Register.class);
	
	@Value("${twilio.trial-number}")
	private PhoneNumber twilioNumber;	
	
	@Value("${twilio.account-sid}")
	private String accountSid;
	
	@Value("${twilio.auth-token}")
	private String authToken;
	
	@Value("${spring.sms.otp.sender.id}")
	private String smsSenderId;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	private UsersRepository usersRepository;
	
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private ISMSService smsSender;
    
    @Autowired
    private IAuthUsecase authUsecase;
    
//    @Autowired
//    private IEmailService emailSender;
	
    public UsersDTO convertToDTO(Users users) {
		return modelMapper.map(users, UsersDTO.class);
	}

	public Users convertToEntity(UsersDTO usersDTO) {
		return modelMapper.map(usersDTO, Users.class);
	}
	
	@Async
    @Override
    public Future<Result<Map<String, String>, String>> register(Register register) {
        try {

            LOGGER.info(register.getMobileNumber());

            Optional<Users> userResult = Optional.empty();

            
            if (!Strings.isNullOrEmpty(register.getMobileNumber())) {
                userResult = usersRepository.findByMobileNumber(register.getMobileNumber());
            } else {
                return CompletableFuture.completedFuture(Result.from(null, "mobile number is empty"));
            }

            if (userResult.isPresent()) {
                LOGGER.error(userResult.toString());
                return CompletableFuture.completedFuture(Result.from(null, "userPayload is already registered"));
            }
            
            String activationCode = null;

            String username = register.getMobileNumber();
            
            activationCode = generateRandomChars(6);
            username = register.getMobileNumber();
            

            String key = "register:" + activationCode;
            redisTemplate.opsForValue().set(key, username);
            redisTemplate.expire(key, 30, TimeUnit.MINUTES);

            LOGGER.info(activationCode);
            
            Map<String, String> result = new HashMap<>();
          
                String messageBody = "Kode OTP anda adalah: " + activationCode;
                SMSPayload smsPayload = new SMSPayload(
                		smsSenderId,
                        register.getMobileNumber(),
                        messageBody
                );
                
                Twilio.init(accountSid, authToken);
        		
                PhoneNumber phoneNumber = new PhoneNumber(register.getMobileNumber());
        		Message message = Message
        	            .creator( phoneNumber, twilioNumber, messageBody)
        	            .create();
        		
        		LOGGER.info("response: -- {} --", message);
        		
                CompletableFuture.runAsync(() -> {
                	try {
                		Future<Result<String, String>> futureSms = smsSender.send(smsPayload);
                        LOGGER.info("response: -- {} --", futureSms.get().getData());
                        result.put("data", futureSms.get().getData());
                    } catch (InterruptedException e) {
                        LOGGER.error("sending sms error : {}", e.getLocalizedMessage());
                    } catch (ExecutionException e) {
                        LOGGER.error("sending sms error : {}", e.getLocalizedMessage());
                    } catch (Exception e) {
                    	LOGGER.error("sending sms error : {}", e.getLocalizedMessage());
                    }
                });

                result.put("data", "sms sent");

            

            return CompletableFuture.completedFuture(
                    Result.from(result, null)
            );
        } catch (MailException e) {
            return CompletableFuture.completedFuture(
                    Result.from(null,
                    		e.getMessage()));
        }
    }
 
	@Async
    @Override
    public Future<Result<Map<String, Object>, String>> createAccount(CreateAccount createAccount) {
        try {
        	
            String key = "register:" + createAccount.getActivationCode();
            String value = redisTemplate.opsForValue().get(key);
            if (value == null) {
                LOGGER.info("key not found");
                return CompletableFuture.completedFuture(Result.from(null, "Invalid activation code"));
            }
            
            String username = null;
            if (!Strings.isNullOrEmpty(createAccount.getMobileNumber())) {
                username = createAccount.getMobileNumber();
            }

            if (username == null) {
                return CompletableFuture.completedFuture(Result.from(null, "Mobile number is empty"));
            }
            
            if (Strings.isNullOrEmpty(createAccount.getMobileNumber())) {
            	return CompletableFuture.completedFuture(Result.from(null, "Email is empty"));
            }

            if (!username.equals(value)) {
                return CompletableFuture.completedFuture(Result.from(null, "Invalid activation code"));
            }

            Optional<Users> userResult = usersRepository.findByMobileNumber(username);
            if (userResult.isPresent()) {
                return CompletableFuture.completedFuture(Result.from(null, "userPayload is already registered"));
            }

            Users user = createAccount.getUser(); 
            
            LocalDateTime localNow = LocalDateTime.now();
    		Timestamp dateNow = Timestamp.valueOf(localNow);
    		
    		String temporaryUsername = createAccount.getMobileNumber();
    		String temporaryPassword = user.getPassword();
    		
    		user.setEmail(createAccount.getEmail());
    		user.setMobileNumber(createAccount.getMobileNumber());
    		user.setCreatedOn(dateNow);
            user.setLastModifiedOn(dateNow);
            user.setPassword(Pbkdf2.hashPassword(
                    user.getPassword(),
                    Constant.PASSWORD_SALT_LEN,
                    Constant.PASSWORD_ITERATIONS,
                    Constant.PASSWORD_KEY_LEN));
            Users saveResult = usersRepository.save(user);

            redisTemplate.delete(key);
            Map<String, Object> result = new HashMap<>();
            result.put("data", saveResult);
            
            RequestLogin reqLogin = new RequestLogin();
            
            reqLogin.setUsername(temporaryUsername);
            reqLogin.setPassword(temporaryPassword);
            
            Future<Result<ResponseLogin, String>> resultFuture = authUsecase.login(reqLogin);
            ResponseLogin respLogin = resultFuture.get().getData();
            result.put("dataLogin", respLogin);
            CompletableFuture<Result<Map<String, Object>, String>> future = CompletableFuture.completedFuture(
                    Result.from(result, null)
            );
            return future;
        } catch (Exception e) {
        	CompletableFuture<Result<Map<String, Object>, String>> future = CompletableFuture.completedFuture(
                    Result.from(null, e.getMessage()));
            return future;
        }
    }

    @Async
    @Override
    public Future<Result<Map<String, String>, String>> verificationCode(OTPCode code) {
        try {

            String key = "register:" + code.getCode();
            String value = redisTemplate.opsForValue().get(key);
            if (value == null) {
                LOGGER.info("email/mobile not found");
                return CompletableFuture.completedFuture(Result.from(null, "Invalid activation code"));
            }

            Map<String, String> result = new HashMap<>();
            result.put("key", value);
            return CompletableFuture.completedFuture(
                    Result.from(result, null)
            );
        } catch (MailException e) {
            return CompletableFuture.completedFuture(
                    Result.from(null,
                            e.getMessage()));
        }
    }
    
    private static String generateRandomChars(Integer length) {
        String chars = "123456789";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(chars.charAt(random.nextInt(chars.length())));
        }

        return sb.toString();
    }
}
