package com.telkom.dtpbe.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import java.lang.annotation.*;

@Pattern(regexp = "^$|.+@.+\\..+", message = "{EmailValidator.email}")
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@Documented
public @interface EmailValidator {
    String message() default "{EmailValidator.email}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
