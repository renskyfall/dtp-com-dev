package com.telkom.dtpbe.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import java.lang.annotation.*;

@Pattern(regexp = "^((\\+)?(62)(8))(\\d{9,11})+$", message = "{MobileNumberValidator.mobile}")
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@Documented
public @interface MobileNumberValidator {
    String message() default "{MobileNumberValidator.mobile}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

// ^((\+|0)?(62|0)(8))(\d{10})+$
// https://en.wikipedia.org/wiki/Telephone_numbers_in_Indonesia